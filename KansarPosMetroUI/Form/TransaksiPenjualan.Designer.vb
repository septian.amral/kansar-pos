<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TransaksiPenjualan
    Inherits DevComponents.DotNetBar.Metro.MetroAppForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TransaksiPenjualan))
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.MetroStatusBar1 = New DevComponents.DotNetBar.Metro.MetroStatusBar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX14 = New DevComponents.DotNetBar.LabelX()
        Me.DateTimeInput1 = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.ComboBoxEx1 = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.ButtonX2 = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonX4 = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX25 = New DevComponents.DotNetBar.LabelX()
        Me.TextBoxX4 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX16 = New DevComponents.DotNetBar.LabelX()
        Me.cbxCustomer = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.txtNamaCustomer = New System.Windows.Forms.TextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteRecordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.txtDisc2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtDisc1 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX27 = New DevComponents.DotNetBar.LabelX()
        Me.txtDiscNilai1 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtDiscNilai2 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.lblSubTotalHidden = New DevComponents.DotNetBar.LabelX()
        Me.LabelX28 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX29 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX30 = New DevComponents.DotNetBar.LabelX()
        Me.txtDiscBulat = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtOngkir = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtDP = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ddlWarehouse = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ddlSalesOrder = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.btnNext = New DevComponents.DotNetBar.ButtonX()
        Me.btnPrev = New DevComponents.DotNetBar.ButtonX()
        Me.btnLast = New DevComponents.DotNetBar.ButtonX()
        Me.btnBegin = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.lblIdTransaksiHidden = New DevComponents.DotNetBar.LabelX()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtReffNo = New System.Windows.Forms.TextBox()
        Me.LabelX31 = New DevComponents.DotNetBar.LabelX()
        Me.btnEdit = New DevComponents.DotNetBar.ButtonX()
        Me.btnNew = New DevComponents.DotNetBar.ButtonX()
        Me.lblSubTotal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX32 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX33 = New DevComponents.DotNetBar.LabelX()
        Me.txtRemark = New System.Windows.Forms.TextBox()
        Me.DataGridViewX1 = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.dgvTransaction = New System.Windows.Forms.DataGridView()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem5 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem7 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer1 = New DevComponents.DotNetBar.GalleryContainer()
        Me.labelItem8 = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem8 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem9 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem10 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem11 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem12 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer2 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.GalleryContainer3 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem14 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem15 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem16 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer4 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.lblPPN = New DevComponents.DotNetBar.LabelX()
        Me.txtPPN = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.lblPrintCount = New System.Windows.Forms.Label()
        Me.ButtonItem17 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem18 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem19 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer5 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.GalleryContainer6 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem6 = New DevComponents.DotNetBar.LabelItem()
        Me.ApplicationButton2 = New DevComponents.DotNetBar.ApplicationButton()
        Me.ItemContainer5 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer6 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer7 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem20 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem21 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem22 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem23 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem24 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer7 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem7 = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem25 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem26 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem27 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem28 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer8 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem9 = New DevComponents.DotNetBar.LabelItem()
        Me.GalleryContainer9 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem10 = New DevComponents.DotNetBar.LabelItem()
        Me.ApplicationButton1 = New DevComponents.DotNetBar.ApplicationButton()
        Me.ItemContainer1 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer4 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem13 = New DevComponents.DotNetBar.ButtonItem()
        Me.ItemContainer2 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer3 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem6 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonControl1 = New DevComponents.DotNetBar.RibbonControl()
        Me.GalleryContainer10 = New DevComponents.DotNetBar.GalleryContainer()
        Me.LabelItem11 = New DevComponents.DotNetBar.LabelItem()
        CType(Me.DateTimeInput1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridViewX1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTransaction, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerColorTint = System.Drawing.Color.Crimson
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro
        Me.StyleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.Crimson)
        '
        'MetroStatusBar1
        '
        Me.MetroStatusBar1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.MetroStatusBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MetroStatusBar1.ContainerControlProcessDialogKey = True
        Me.MetroStatusBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MetroStatusBar1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroStatusBar1.ForeColor = System.Drawing.Color.Black
        Me.MetroStatusBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1})
        Me.MetroStatusBar1.Location = New System.Drawing.Point(0, 613)
        Me.MetroStatusBar1.Name = "MetroStatusBar1"
        Me.MetroStatusBar1.Size = New System.Drawing.Size(969, 22)
        Me.MetroStatusBar1.TabIndex = 1
        Me.MetroStatusBar1.Text = "MetroStatusBar1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "READY"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.BackColor = System.Drawing.Color.Crimson
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(0, 60)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(969, 2)
        Me.LabelX1.TabIndex = 4
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX9.ForeColor = System.Drawing.Color.Black
        Me.LabelX9.Location = New System.Drawing.Point(337, 81)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(23, 23)
        Me.LabelX9.TabIndex = 49
        Me.LabelX9.Text = ":"
        Me.LabelX9.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.ForeColor = System.Drawing.Color.Black
        Me.LabelX10.Location = New System.Drawing.Point(294, 81)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(46, 23)
        Me.LabelX10.TabIndex = 48
        Me.LabelX10.Text = "&Tanggal"
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(641, 83)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(23, 23)
        Me.LabelX6.TabIndex = 40
        Me.LabelX6.Text = ":"
        Me.LabelX6.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(553, 83)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(93, 23)
        Me.LabelX11.TabIndex = 36
        Me.LabelX11.Text = "&Nama Customer"
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(83, 79)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(28, 23)
        Me.LabelX12.TabIndex = 52
        Me.LabelX12.Text = ":"
        Me.LabelX12.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.ForeColor = System.Drawing.Color.Black
        Me.LabelX13.Location = New System.Drawing.Point(18, 79)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(64, 23)
        Me.LabelX13.TabIndex = 51
        Me.LabelX13.Text = "&Id Transaksi"
        '
        'LabelX14
        '
        Me.LabelX14.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX14.BackgroundStyle.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.LabelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX14.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelX14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX14.ForeColor = System.Drawing.Color.Black
        Me.LabelX14.Location = New System.Drawing.Point(117, 80)
        Me.LabelX14.Name = "LabelX14"
        Me.LabelX14.Size = New System.Drawing.Size(149, 23)
        Me.LabelX14.TabIndex = 53
        '
        'DateTimeInput1
        '
        Me.DateTimeInput1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.DateTimeInput1.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateTimeInput1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimeInput1.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateTimeInput1.ButtonDropDown.Visible = True
        Me.DateTimeInput1.ForeColor = System.Drawing.Color.Black
        Me.DateTimeInput1.Format = DevComponents.Editors.eDateTimePickerFormat.[Long]
        Me.DateTimeInput1.IsPopupCalendarOpen = False
        Me.DateTimeInput1.Location = New System.Drawing.Point(369, 83)
        '
        '
        '
        Me.DateTimeInput1.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateTimeInput1.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimeInput1.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateTimeInput1.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateTimeInput1.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateTimeInput1.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimeInput1.MonthCalendar.DisplayMonth = New Date(2017, 10, 1, 0, 0, 0, 0)
        Me.DateTimeInput1.MonthCalendar.FirstDayOfWeek = System.DayOfWeek.Monday
        Me.DateTimeInput1.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateTimeInput1.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateTimeInput1.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateTimeInput1.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateTimeInput1.MonthCalendar.TodayButtonVisible = True
        Me.DateTimeInput1.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateTimeInput1.Name = "DateTimeInput1"
        Me.DateTimeInput1.Size = New System.Drawing.Size(156, 26)
        Me.DateTimeInput1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateTimeInput1.TabIndex = 54
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(132, 125)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(34, 23)
        Me.LabelX2.TabIndex = 56
        Me.LabelX2.Text = ":"
        Me.LabelX2.TextAlignment = System.Drawing.StringAlignment.Center
        Me.LabelX2.Visible = False
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(18, 125)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(98, 23)
        Me.LabelX3.TabIndex = 55
        Me.LabelX3.Text = "&Kode Barang"
        Me.LabelX3.Visible = False
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX4.BackgroundStyle.BackColor = System.Drawing.Color.Crimson
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(-3, 120)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(969, 2)
        Me.LabelX4.TabIndex = 57
        '
        'ComboBoxEx1
        '
        Me.ComboBoxEx1.DisplayMember = "Text"
        Me.ComboBoxEx1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboBoxEx1.ForeColor = System.Drawing.Color.Black
        Me.ComboBoxEx1.FormattingEnabled = True
        Me.ComboBoxEx1.ItemHeight = 20
        Me.ComboBoxEx1.Location = New System.Drawing.Point(172, 126)
        Me.ComboBoxEx1.Name = "ComboBoxEx1"
        Me.ComboBoxEx1.Size = New System.Drawing.Size(149, 26)
        Me.ComboBoxEx1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboBoxEx1.TabIndex = 58
        Me.ComboBoxEx1.Visible = False
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(18, 157)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(46, 20)
        Me.LabelX7.TabIndex = 60
        Me.LabelX7.Text = "&Remark"
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelX8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(172, 151)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(329, 23)
        Me.LabelX8.TabIndex = 63
        Me.LabelX8.Visible = False
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelX15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(385, 133)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(46, 23)
        Me.LabelX15.TabIndex = 66
        Me.LabelX15.TextAlignment = System.Drawing.StringAlignment.Center
        Me.LabelX15.Visible = False
        '
        'ButtonX2
        '
        Me.ButtonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX2.FocusCuesEnabled = False
        Me.ButtonX2.Location = New System.Drawing.Point(265, 560)
        Me.ButtonX2.Name = "ButtonX2"
        Me.ButtonX2.Size = New System.Drawing.Size(75, 23)
        Me.ButtonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX2.TabIndex = 71
        Me.ButtonX2.Text = "Close"
        '
        'ButtonX4
        '
        Me.ButtonX4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX4.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX4.FocusCuesEnabled = False
        Me.ButtonX4.Location = New System.Drawing.Point(180, 560)
        Me.ButtonX4.Name = "ButtonX4"
        Me.ButtonX4.Size = New System.Drawing.Size(75, 23)
        Me.ButtonX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX4.TabIndex = 73
        Me.ButtonX4.Text = "&Save"
        '
        'LabelX21
        '
        Me.LabelX21.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX21.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelX21.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX21.ForeColor = System.Drawing.Color.Black
        Me.LabelX21.Location = New System.Drawing.Point(438, 133)
        Me.LabelX21.Name = "LabelX21"
        Me.LabelX21.Size = New System.Drawing.Size(63, 23)
        Me.LabelX21.TabIndex = 75
        Me.LabelX21.TextAlignment = System.Drawing.StringAlignment.Center
        Me.LabelX21.Visible = False
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelX22.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX22.ForeColor = System.Drawing.Color.Black
        Me.LabelX22.Location = New System.Drawing.Point(414, 178)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(87, 23)
        Me.LabelX22.TabIndex = 76
        Me.LabelX22.TextAlignment = System.Drawing.StringAlignment.Center
        Me.LabelX22.Visible = False
        '
        'LabelX23
        '
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelX23.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.Black
        Me.LabelX23.Location = New System.Drawing.Point(155, 411)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(64, 23)
        Me.LabelX23.TabIndex = 79
        Me.LabelX23.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'LabelX24
        '
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX24.ForeColor = System.Drawing.Color.Black
        Me.LabelX24.Location = New System.Drawing.Point(132, 411)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(17, 23)
        Me.LabelX24.TabIndex = 78
        Me.LabelX24.Text = ":"
        Me.LabelX24.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'LabelX25
        '
        Me.LabelX25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX25.ForeColor = System.Drawing.Color.Black
        Me.LabelX25.Location = New System.Drawing.Point(18, 411)
        Me.LabelX25.Name = "LabelX25"
        Me.LabelX25.Size = New System.Drawing.Size(98, 23)
        Me.LabelX25.TabIndex = 77
        Me.LabelX25.Text = "&Jumlah Barang"
        '
        'TextBoxX4
        '
        Me.TextBoxX4.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TextBoxX4.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.TextBoxX4.Border.Class = "TextBoxBorder"
        Me.TextBoxX4.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxX4.Font = New System.Drawing.Font("DSEG7 Modern", 40.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxX4.ForeColor = System.Drawing.Color.Black
        Me.TextBoxX4.Location = New System.Drawing.Point(369, 127)
        Me.TextBoxX4.Name = "TextBoxX4"
        Me.TextBoxX4.ReadOnly = True
        Me.TextBoxX4.Size = New System.Drawing.Size(583, 81)
        Me.TextBoxX4.TabIndex = 83
        Me.TextBoxX4.Text = "0"
        Me.TextBoxX4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelX16
        '
        Me.LabelX16.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX16.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LabelX16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX16.ForeColor = System.Drawing.Color.Black
        Me.LabelX16.Location = New System.Drawing.Point(862, 437)
        Me.LabelX16.Name = "LabelX16"
        Me.LabelX16.Size = New System.Drawing.Size(90, 23)
        Me.LabelX16.TabIndex = 84
        Me.LabelX16.Visible = False
        '
        'cbxCustomer
        '
        Me.cbxCustomer.DisplayMember = "Text"
        Me.cbxCustomer.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbxCustomer.ForeColor = System.Drawing.Color.Black
        Me.cbxCustomer.FormattingEnabled = True
        Me.cbxCustomer.ItemHeight = 20
        Me.cbxCustomer.Location = New System.Drawing.Point(795, 84)
        Me.cbxCustomer.Name = "cbxCustomer"
        Me.cbxCustomer.Size = New System.Drawing.Size(157, 26)
        Me.cbxCustomer.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cbxCustomer.TabIndex = 85
        Me.cbxCustomer.Visible = False
        '
        'txtNamaCustomer
        '
        Me.txtNamaCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtNamaCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtNamaCustomer.BackColor = System.Drawing.Color.White
        Me.txtNamaCustomer.ForeColor = System.Drawing.Color.Black
        Me.txtNamaCustomer.Location = New System.Drawing.Point(669, 86)
        Me.txtNamaCustomer.Name = "txtNamaCustomer"
        Me.txtNamaCustomer.Size = New System.Drawing.Size(283, 26)
        Me.txtNamaCustomer.TabIndex = 86
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteRecordToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(168, 28)
        '
        'DeleteRecordToolStripMenuItem
        '
        Me.DeleteRecordToolStripMenuItem.Name = "DeleteRecordToolStripMenuItem"
        Me.DeleteRecordToolStripMenuItem.Size = New System.Drawing.Size(167, 24)
        Me.DeleteRecordToolStripMenuItem.Text = "delete record"
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(701, 413)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(98, 23)
        Me.LabelX17.TabIndex = 87
        Me.LabelX17.Text = "&Sub Total :"
        '
        'txtDisc2
        '
        Me.txtDisc2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDisc2.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtDisc2.Border.Class = "TextBoxBorder"
        Me.txtDisc2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDisc2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDisc2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDisc2.ForeColor = System.Drawing.Color.Black
        Me.txtDisc2.Location = New System.Drawing.Point(701, 464)
        Me.txtDisc2.MaxLength = 5
        Me.txtDisc2.Multiline = True
        Me.txtDisc2.Name = "txtDisc2"
        Me.txtDisc2.Size = New System.Drawing.Size(68, 25)
        Me.txtDisc2.TabIndex = 89
        Me.txtDisc2.Text = "0"
        Me.txtDisc2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDisc1
        '
        Me.txtDisc1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDisc1.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtDisc1.Border.Class = "TextBoxBorder"
        Me.txtDisc1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDisc1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDisc1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDisc1.ForeColor = System.Drawing.Color.Black
        Me.txtDisc1.Location = New System.Drawing.Point(701, 437)
        Me.txtDisc1.MaxLength = 5
        Me.txtDisc1.Multiline = True
        Me.txtDisc1.Name = "txtDisc1"
        Me.txtDisc1.Size = New System.Drawing.Size(68, 25)
        Me.txtDisc1.TabIndex = 90
        Me.txtDisc1.Text = "0"
        Me.txtDisc1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(775, 437)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(37, 23)
        Me.LabelX20.TabIndex = 91
        Me.LabelX20.Text = "% = "
        Me.LabelX20.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'LabelX27
        '
        Me.LabelX27.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX27.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX27.ForeColor = System.Drawing.Color.Black
        Me.LabelX27.Location = New System.Drawing.Point(775, 466)
        Me.LabelX27.Name = "LabelX27"
        Me.LabelX27.Size = New System.Drawing.Size(37, 23)
        Me.LabelX27.TabIndex = 92
        Me.LabelX27.Text = "% = "
        Me.LabelX27.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'txtDiscNilai1
        '
        Me.txtDiscNilai1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDiscNilai1.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtDiscNilai1.Border.Class = "TextBoxBorder"
        Me.txtDiscNilai1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDiscNilai1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiscNilai1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiscNilai1.ForeColor = System.Drawing.Color.Black
        Me.txtDiscNilai1.Location = New System.Drawing.Point(818, 437)
        Me.txtDiscNilai1.Multiline = True
        Me.txtDiscNilai1.Name = "txtDiscNilai1"
        Me.txtDiscNilai1.Size = New System.Drawing.Size(134, 25)
        Me.txtDiscNilai1.TabIndex = 93
        Me.txtDiscNilai1.Text = "0"
        Me.txtDiscNilai1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDiscNilai2
        '
        Me.txtDiscNilai2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDiscNilai2.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtDiscNilai2.Border.Class = "TextBoxBorder"
        Me.txtDiscNilai2.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDiscNilai2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiscNilai2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiscNilai2.ForeColor = System.Drawing.Color.Black
        Me.txtDiscNilai2.Location = New System.Drawing.Point(818, 464)
        Me.txtDiscNilai2.Multiline = True
        Me.txtDiscNilai2.Name = "txtDiscNilai2"
        Me.txtDiscNilai2.Size = New System.Drawing.Size(134, 25)
        Me.txtDiscNilai2.TabIndex = 94
        Me.txtDiscNilai2.Text = "0"
        Me.txtDiscNilai2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSubTotalHidden
        '
        Me.lblSubTotalHidden.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblSubTotalHidden.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblSubTotalHidden.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubTotalHidden.ForeColor = System.Drawing.Color.Black
        Me.lblSubTotalHidden.Location = New System.Drawing.Point(561, 414)
        Me.lblSubTotalHidden.Name = "lblSubTotalHidden"
        Me.lblSubTotalHidden.Size = New System.Drawing.Size(134, 23)
        Me.lblSubTotalHidden.TabIndex = 95
        Me.lblSubTotalHidden.Text = "0"
        Me.lblSubTotalHidden.TextAlignment = System.Drawing.StringAlignment.Far
        Me.lblSubTotalHidden.Visible = False
        '
        'LabelX28
        '
        Me.LabelX28.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX28.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX28.ForeColor = System.Drawing.Color.Black
        Me.LabelX28.Location = New System.Drawing.Point(647, 500)
        Me.LabelX28.Name = "LabelX28"
        Me.LabelX28.Size = New System.Drawing.Size(137, 23)
        Me.LabelX28.TabIndex = 96
        Me.LabelX28.Text = "&Diskon Pembulatan :"
        '
        'LabelX29
        '
        Me.LabelX29.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX29.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX29.ForeColor = System.Drawing.Color.Black
        Me.LabelX29.Location = New System.Drawing.Point(647, 554)
        Me.LabelX29.Name = "LabelX29"
        Me.LabelX29.Size = New System.Drawing.Size(137, 23)
        Me.LabelX29.TabIndex = 97
        Me.LabelX29.Text = "&Ongkos Kirim           :"
        '
        'LabelX30
        '
        Me.LabelX30.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX30.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX30.ForeColor = System.Drawing.Color.Black
        Me.LabelX30.Location = New System.Drawing.Point(648, 581)
        Me.LabelX30.Name = "LabelX30"
        Me.LabelX30.Size = New System.Drawing.Size(137, 23)
        Me.LabelX30.TabIndex = 98
        Me.LabelX30.Text = "&DP                             :"
        '
        'txtDiscBulat
        '
        Me.txtDiscBulat.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDiscBulat.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtDiscBulat.Border.Class = "TextBoxBorder"
        Me.txtDiscBulat.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDiscBulat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiscBulat.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiscBulat.ForeColor = System.Drawing.Color.Black
        Me.txtDiscBulat.Location = New System.Drawing.Point(818, 500)
        Me.txtDiscBulat.Multiline = True
        Me.txtDiscBulat.Name = "txtDiscBulat"
        Me.txtDiscBulat.Size = New System.Drawing.Size(134, 25)
        Me.txtDiscBulat.TabIndex = 99
        Me.txtDiscBulat.Text = "0"
        Me.txtDiscBulat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOngkir
        '
        Me.txtOngkir.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtOngkir.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtOngkir.Border.Class = "TextBoxBorder"
        Me.txtOngkir.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOngkir.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOngkir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOngkir.ForeColor = System.Drawing.Color.Black
        Me.txtOngkir.Location = New System.Drawing.Point(818, 554)
        Me.txtOngkir.Multiline = True
        Me.txtOngkir.Name = "txtOngkir"
        Me.txtOngkir.Size = New System.Drawing.Size(134, 25)
        Me.txtOngkir.TabIndex = 100
        Me.txtOngkir.Text = "0"
        Me.txtOngkir.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDP
        '
        Me.txtDP.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDP.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtDP.Border.Class = "TextBoxBorder"
        Me.txtDP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDP.ForeColor = System.Drawing.Color.Black
        Me.txtDP.Location = New System.Drawing.Point(818, 581)
        Me.txtDP.Multiline = True
        Me.txtDP.Name = "txtDP"
        Me.txtDP.Size = New System.Drawing.Size(134, 25)
        Me.txtDP.TabIndex = 101
        Me.txtDP.Text = "0"
        Me.txtDP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ddlWarehouse
        '
        Me.ddlWarehouse.DisplayMember = "Text"
        Me.ddlWarehouse.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ddlWarehouse.ForeColor = System.Drawing.Color.Black
        Me.ddlWarehouse.FormattingEnabled = True
        Me.ddlWarehouse.ItemHeight = 20
        Me.ddlWarehouse.Location = New System.Drawing.Point(110, 30)
        Me.ddlWarehouse.Name = "ddlWarehouse"
        Me.ddlWarehouse.Size = New System.Drawing.Size(211, 26)
        Me.ddlWarehouse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ddlWarehouse.TabIndex = 102
        '
        'ddlSalesOrder
        '
        Me.ddlSalesOrder.DisplayMember = "Text"
        Me.ddlSalesOrder.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ddlSalesOrder.ForeColor = System.Drawing.Color.Black
        Me.ddlSalesOrder.FormattingEnabled = True
        Me.ddlSalesOrder.ItemHeight = 20
        Me.ddlSalesOrder.Location = New System.Drawing.Point(71, 125)
        Me.ddlSalesOrder.Name = "ddlSalesOrder"
        Me.ddlSalesOrder.Size = New System.Drawing.Size(250, 26)
        Me.ddlSalesOrder.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ddlSalesOrder.TabIndex = 103
        '
        'btnNext
        '
        Me.btnNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnNext.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnNext.FocusCuesEnabled = False
        Me.btnNext.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNext.Location = New System.Drawing.Point(101, 51)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(29, 23)
        Me.btnNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnNext.TabIndex = 104
        Me.btnNext.Text = ">"
        '
        'btnPrev
        '
        Me.btnPrev.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnPrev.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnPrev.FocusCuesEnabled = False
        Me.btnPrev.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrev.Location = New System.Drawing.Point(73, 51)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(29, 23)
        Me.btnPrev.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnPrev.TabIndex = 105
        Me.btnPrev.Text = "<"
        '
        'btnLast
        '
        Me.btnLast.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnLast.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnLast.FocusCuesEnabled = False
        Me.btnLast.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLast.Location = New System.Drawing.Point(129, 51)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(29, 23)
        Me.btnLast.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnLast.TabIndex = 106
        Me.btnLast.Text = ">|"
        '
        'btnBegin
        '
        Me.btnBegin.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBegin.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnBegin.FocusCuesEnabled = False
        Me.btnBegin.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBegin.Location = New System.Drawing.Point(45, 51)
        Me.btnBegin.Name = "btnBegin"
        Me.btnBegin.Size = New System.Drawing.Size(29, 23)
        Me.btnBegin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnBegin.TabIndex = 107
        Me.btnBegin.Text = "|<"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.txtSearch)
        Me.GroupBox1.Controls.Add(Me.btnNext)
        Me.GroupBox1.Controls.Add(Me.btnBegin)
        Me.GroupBox1.Controls.Add(Me.btnLast)
        Me.GroupBox1.Controls.Add(Me.btnPrev)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(371, 428)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(210, 82)
        Me.GroupBox1.TabIndex = 109
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Search"
        '
        'txtSearch
        '
        Me.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtSearch.BackColor = System.Drawing.Color.White
        Me.txtSearch.Font = New System.Drawing.Font("DSEG7 Modern", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.ForeColor = System.Drawing.Color.Black
        Me.txtSearch.Location = New System.Drawing.Point(6, 19)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(198, 26)
        Me.txtSearch.TabIndex = 110
        '
        'lblIdTransaksiHidden
        '
        Me.lblIdTransaksiHidden.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblIdTransaksiHidden.BackgroundStyle.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.lblIdTransaksiHidden.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblIdTransaksiHidden.BackgroundStyle.TextColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblIdTransaksiHidden.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdTransaksiHidden.ForeColor = System.Drawing.Color.Black
        Me.lblIdTransaksiHidden.Location = New System.Drawing.Point(589, 30)
        Me.lblIdTransaksiHidden.Name = "lblIdTransaksiHidden"
        Me.lblIdTransaksiHidden.Size = New System.Drawing.Size(149, 23)
        Me.lblIdTransaksiHidden.TabIndex = 110
        Me.lblIdTransaksiHidden.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(438, 560)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 111
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'txtReffNo
        '
        Me.txtReffNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtReffNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtReffNo.BackColor = System.Drawing.Color.White
        Me.txtReffNo.ForeColor = System.Drawing.Color.Black
        Me.txtReffNo.Location = New System.Drawing.Point(390, 30)
        Me.txtReffNo.Name = "txtReffNo"
        Me.txtReffNo.Size = New System.Drawing.Size(158, 26)
        Me.txtReffNo.TabIndex = 111
        '
        'LabelX31
        '
        Me.LabelX31.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX31.ForeColor = System.Drawing.Color.Black
        Me.LabelX31.Location = New System.Drawing.Point(333, 30)
        Me.LabelX31.Name = "LabelX31"
        Me.LabelX31.Size = New System.Drawing.Size(53, 23)
        Me.LabelX31.TabIndex = 112
        Me.LabelX31.Text = "Reff. No."
        '
        'btnEdit
        '
        Me.btnEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnEdit.FocusCuesEnabled = False
        Me.btnEdit.Location = New System.Drawing.Point(99, 560)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 23)
        Me.btnEdit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnEdit.TabIndex = 113
        Me.btnEdit.Text = "Edit"
        '
        'btnNew
        '
        Me.btnNew.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnNew.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnNew.FocusCuesEnabled = False
        Me.btnNew.Location = New System.Drawing.Point(18, 560)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(75, 23)
        Me.btnNew.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnNew.TabIndex = 114
        Me.btnNew.Text = "New"
        '
        'lblSubTotal
        '
        Me.lblSubTotal.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lblSubTotal.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblSubTotal.Border.Class = "TextBoxBorder"
        Me.lblSubTotal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblSubTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.lblSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubTotal.ForeColor = System.Drawing.Color.Black
        Me.lblSubTotal.Location = New System.Drawing.Point(818, 411)
        Me.lblSubTotal.Multiline = True
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Size = New System.Drawing.Size(134, 25)
        Me.lblSubTotal.TabIndex = 115
        Me.lblSubTotal.Text = "0"
        Me.lblSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelX32
        '
        Me.LabelX32.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX32.ForeColor = System.Drawing.Color.Black
        Me.LabelX32.Location = New System.Drawing.Point(18, 507)
        Me.LabelX32.Name = "LabelX32"
        Me.LabelX32.Size = New System.Drawing.Size(148, 23)
        Me.LabelX32.TabIndex = 116
        Me.LabelX32.Text = "F1 = Popup Customer"
        '
        'LabelX33
        '
        Me.LabelX33.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX33.ForeColor = System.Drawing.Color.Black
        Me.LabelX33.Location = New System.Drawing.Point(18, 531)
        Me.LabelX33.Name = "LabelX33"
        Me.LabelX33.Size = New System.Drawing.Size(148, 23)
        Me.LabelX33.TabIndex = 117
        Me.LabelX33.Text = "F2 = Popup Barang"
        '
        'txtRemark
        '
        Me.txtRemark.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtRemark.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtRemark.BackColor = System.Drawing.Color.White
        Me.txtRemark.ForeColor = System.Drawing.Color.Black
        Me.txtRemark.Location = New System.Drawing.Point(70, 154)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(251, 40)
        Me.txtRemark.TabIndex = 118
        '
        'DataGridViewX1
        '
        Me.DataGridViewX1.AllowUserToOrderColumns = True
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.White
        Me.DataGridViewX1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewX1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridViewX1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridViewX1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewX1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridViewX1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewX1.DefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewX1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.DataGridViewX1.EnableHeadersVisualStyles = False
        Me.DataGridViewX1.GridColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.DataGridViewX1.Location = New System.Drawing.Point(18, 206)
        Me.DataGridViewX1.Name = "DataGridViewX1"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.ActiveCaption
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewX1.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridViewX1.RowHeadersWidth = 51
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DataGridViewX1.RowsDefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridViewX1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.DataGridViewX1.Size = New System.Drawing.Size(604, 201)
        Me.DataGridViewX1.TabIndex = 1
        Me.DataGridViewX1.Visible = False
        '
        'dgvTransaction
        '
        Me.dgvTransaction.AllowUserToOrderColumns = True
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.White
        Me.dgvTransaction.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvTransaction.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTransaction.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.HotTrack
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTransaction.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvTransaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(CType(CType(227, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(252, Byte), Integer))
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTransaction.DefaultCellStyle = DataGridViewCellStyle17
        Me.dgvTransaction.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvTransaction.EnableHeadersVisualStyles = False
        Me.dgvTransaction.Location = New System.Drawing.Point(18, 200)
        Me.dgvTransaction.Name = "dgvTransaction"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.HotTrack
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTransaction.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvTransaction.RowHeadersWidth = 51
        Me.dgvTransaction.RowTemplate.Height = 24
        Me.dgvTransaction.Size = New System.Drawing.Size(934, 207)
        Me.dgvTransaction.TabIndex = 120
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(162, 507)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(188, 23)
        Me.LabelX18.TabIndex = 121
        Me.LabelX18.Text = "F3 = Popup History Penjualan"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem3.Image = CType(resources.GetObject("ButtonItem3.Image"), System.Drawing.Image)
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.SubItemsExpandWidth = 24
        Me.ButtonItem3.Text = "&Open..."
        '
        'ButtonItem4
        '
        Me.ButtonItem4.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem4.Image = CType(resources.GetObject("ButtonItem4.Image"), System.Drawing.Image)
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.SubItemsExpandWidth = 24
        Me.ButtonItem4.Text = "&Save..."
        '
        'ButtonItem5
        '
        Me.ButtonItem5.BeginGroup = True
        Me.ButtonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem5.Image = CType(resources.GetObject("ButtonItem5.Image"), System.Drawing.Image)
        Me.ButtonItem5.Name = "ButtonItem5"
        Me.ButtonItem5.SubItemsExpandWidth = 24
        Me.ButtonItem5.Text = "S&hare..."
        '
        'ButtonItem7
        '
        Me.ButtonItem7.BeginGroup = True
        Me.ButtonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem7.Image = CType(resources.GetObject("ButtonItem7.Image"), System.Drawing.Image)
        Me.ButtonItem7.Name = "ButtonItem7"
        Me.ButtonItem7.SubItemsExpandWidth = 24
        Me.ButtonItem7.Text = "&Close"
        '
        'GalleryContainer1
        '
        '
        '
        '
        Me.GalleryContainer1.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer1.EnableGalleryPopup = False
        Me.GalleryContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer1.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer1.MultiLine = False
        Me.GalleryContainer1.Name = "GalleryContainer1"
        Me.GalleryContainer1.PopupUsesStandardScrollbars = False
        Me.GalleryContainer1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.labelItem8, Me.ButtonItem8, Me.ButtonItem9, Me.ButtonItem10, Me.ButtonItem11})
        '
        '
        '
        Me.GalleryContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'labelItem8
        '
        Me.labelItem8.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.labelItem8.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.labelItem8.CanCustomize = False
        Me.labelItem8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.labelItem8.Name = "labelItem8"
        Me.labelItem8.PaddingBottom = 2
        Me.labelItem8.PaddingTop = 2
        Me.labelItem8.Stretch = True
        Me.labelItem8.Text = "Recent Documents"
        '
        'ButtonItem8
        '
        Me.ButtonItem8.Name = "ButtonItem8"
        Me.ButtonItem8.Text = "&1. Short News 5-7.rtf"
        '
        'ButtonItem9
        '
        Me.ButtonItem9.Name = "ButtonItem9"
        Me.ButtonItem9.Text = "&2. Prospect Email.rtf"
        '
        'ButtonItem10
        '
        Me.ButtonItem10.Name = "ButtonItem10"
        Me.ButtonItem10.Text = "&3. Customer Email.rtf"
        '
        'ButtonItem11
        '
        Me.ButtonItem11.Name = "ButtonItem11"
        Me.ButtonItem11.Text = "&4. example.rtf"
        '
        'ButtonItem12
        '
        Me.ButtonItem12.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonItem12.Image = CType(resources.GetObject("ButtonItem12.Image"), System.Drawing.Image)
        Me.ButtonItem12.Name = "ButtonItem12"
        Me.ButtonItem12.SubItemsExpandWidth = 24
        Me.ButtonItem12.Text = "Opt&ions"
        '
        'GalleryContainer2
        '
        '
        '
        '
        Me.GalleryContainer2.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer2.EnableGalleryPopup = False
        Me.GalleryContainer2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer2.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer2.MultiLine = False
        Me.GalleryContainer2.Name = "GalleryContainer2"
        Me.GalleryContainer2.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem2
        '
        Me.LabelItem2.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem2.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem2.CanCustomize = False
        Me.LabelItem2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem2.Name = "LabelItem2"
        '
        'GalleryContainer3
        '
        '
        '
        '
        Me.GalleryContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer3.EnableGalleryPopup = False
        Me.GalleryContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer3.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer3.MultiLine = False
        Me.GalleryContainer3.Name = "GalleryContainer3"
        Me.GalleryContainer3.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem3
        '
        Me.LabelItem3.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem3.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem3.CanCustomize = False
        Me.LabelItem3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem3.Name = "LabelItem3"
        '
        'ButtonItem14
        '
        Me.ButtonItem14.BeginGroup = True
        Me.ButtonItem14.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem14.Image = CType(resources.GetObject("ButtonItem14.Image"), System.Drawing.Image)
        Me.ButtonItem14.Name = "ButtonItem14"
        Me.ButtonItem14.SubItemsExpandWidth = 24
        Me.ButtonItem14.Text = "S&hare..."
        '
        'ButtonItem15
        '
        Me.ButtonItem15.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem15.Image = CType(resources.GetObject("ButtonItem15.Image"), System.Drawing.Image)
        Me.ButtonItem15.Name = "ButtonItem15"
        Me.ButtonItem15.SubItemsExpandWidth = 24
        Me.ButtonItem15.Text = "&Print..."
        '
        'ButtonItem16
        '
        Me.ButtonItem16.BeginGroup = True
        Me.ButtonItem16.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem16.Image = CType(resources.GetObject("ButtonItem16.Image"), System.Drawing.Image)
        Me.ButtonItem16.Name = "ButtonItem16"
        Me.ButtonItem16.SubItemsExpandWidth = 24
        Me.ButtonItem16.Text = "&Close"
        '
        'GalleryContainer4
        '
        '
        '
        '
        Me.GalleryContainer4.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer4.EnableGalleryPopup = False
        Me.GalleryContainer4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer4.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer4.MultiLine = False
        Me.GalleryContainer4.Name = "GalleryContainer4"
        Me.GalleryContainer4.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem4
        '
        Me.LabelItem4.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem4.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem4.CanCustomize = False
        Me.LabelItem4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem4.Name = "LabelItem4"
        '
        'lblPPN
        '
        Me.lblPPN.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblPPN.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblPPN.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPPN.ForeColor = System.Drawing.Color.Black
        Me.lblPPN.Location = New System.Drawing.Point(647, 527)
        Me.lblPPN.Name = "lblPPN"
        Me.lblPPN.Size = New System.Drawing.Size(137, 23)
        Me.lblPPN.TabIndex = 123
        Me.lblPPN.Text = "PPN 10 % :"
        '
        'txtPPN
        '
        Me.txtPPN.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPPN.Border.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.txtPPN.Border.Class = "TextBoxBorder"
        Me.txtPPN.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPPN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPPN.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.749999!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPPN.ForeColor = System.Drawing.Color.Black
        Me.txtPPN.Location = New System.Drawing.Point(818, 527)
        Me.txtPPN.Multiline = True
        Me.txtPPN.Name = "txtPPN"
        Me.txtPPN.Size = New System.Drawing.Size(134, 25)
        Me.txtPPN.TabIndex = 124
        Me.txtPPN.Text = "0"
        Me.txtPPN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(162, 531)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(188, 23)
        Me.LabelX19.TabIndex = 125
        Me.LabelX19.Text = "F8 = Delete Row"
        '
        'lblPrintCount
        '
        Me.lblPrintCount.AutoSize = True
        Me.lblPrintCount.BackColor = System.Drawing.Color.White
        Me.lblPrintCount.ForeColor = System.Drawing.Color.Black
        Me.lblPrintCount.Location = New System.Drawing.Point(429, 526)
        Me.lblPrintCount.Name = "lblPrintCount"
        Me.lblPrintCount.Size = New System.Drawing.Size(96, 19)
        Me.lblPrintCount.TabIndex = 126
        Me.lblPrintCount.Text = "Total Cetak : 0"
        '
        'ButtonItem17
        '
        Me.ButtonItem17.BeginGroup = True
        Me.ButtonItem17.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem17.Image = CType(resources.GetObject("ButtonItem17.Image"), System.Drawing.Image)
        Me.ButtonItem17.Name = "ButtonItem17"
        Me.ButtonItem17.SubItemsExpandWidth = 24
        Me.ButtonItem17.Text = "S&hare..."
        '
        'ButtonItem18
        '
        Me.ButtonItem18.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem18.Image = CType(resources.GetObject("ButtonItem18.Image"), System.Drawing.Image)
        Me.ButtonItem18.Name = "ButtonItem18"
        Me.ButtonItem18.SubItemsExpandWidth = 24
        Me.ButtonItem18.Text = "&Print..."
        '
        'ButtonItem19
        '
        Me.ButtonItem19.BeginGroup = True
        Me.ButtonItem19.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem19.Image = CType(resources.GetObject("ButtonItem19.Image"), System.Drawing.Image)
        Me.ButtonItem19.Name = "ButtonItem19"
        Me.ButtonItem19.SubItemsExpandWidth = 24
        Me.ButtonItem19.Text = "&Close"
        '
        'GalleryContainer5
        '
        '
        '
        '
        Me.GalleryContainer5.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer5.EnableGalleryPopup = False
        Me.GalleryContainer5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer5.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer5.MultiLine = False
        Me.GalleryContainer5.Name = "GalleryContainer5"
        Me.GalleryContainer5.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem5
        '
        Me.LabelItem5.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem5.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem5.CanCustomize = False
        Me.LabelItem5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem5.Name = "LabelItem5"
        '
        'GalleryContainer6
        '
        '
        '
        '
        Me.GalleryContainer6.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer6.EnableGalleryPopup = False
        Me.GalleryContainer6.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer6.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer6.MultiLine = False
        Me.GalleryContainer6.Name = "GalleryContainer6"
        Me.GalleryContainer6.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem6
        '
        Me.LabelItem6.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem6.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem6.CanCustomize = False
        Me.LabelItem6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem6.Name = "LabelItem6"
        '
        'ApplicationButton2
        '
        Me.ApplicationButton2.AutoExpandOnClick = True
        Me.ApplicationButton2.CanCustomize = False
        Me.ApplicationButton2.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image
        Me.ApplicationButton2.Image = CType(resources.GetObject("ApplicationButton2.Image"), System.Drawing.Image)
        Me.ApplicationButton2.ImageFixedSize = New System.Drawing.Size(16, 16)
        Me.ApplicationButton2.ImagePaddingHorizontal = 0
        Me.ApplicationButton2.ImagePaddingVertical = 1
        Me.ApplicationButton2.Name = "ApplicationButton2"
        Me.ApplicationButton2.ShowSubItems = False
        Me.ApplicationButton2.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer5})
        Me.ApplicationButton2.Text = "&File"
        '
        'ItemContainer5
        '
        '
        '
        '
        Me.ItemContainer5.BackgroundStyle.Class = "RibbonFileMenuContainer"
        Me.ItemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer5.Name = "ItemContainer5"
        Me.ItemContainer5.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer6})
        '
        '
        '
        Me.ItemContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer6
        '
        '
        '
        '
        Me.ItemContainer6.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer6.ItemSpacing = 0
        Me.ItemContainer6.Name = "ItemContainer6"
        Me.ItemContainer6.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer7})
        '
        '
        '
        Me.ItemContainer6.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer7
        '
        '
        '
        '
        Me.ItemContainer7.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer7.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer7.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer7.Name = "ItemContainer7"
        Me.ItemContainer7.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem20, Me.ButtonItem21, Me.ButtonItem22, Me.ButtonItem23, Me.ButtonItem24})
        '
        '
        '
        Me.ItemContainer7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem20
        '
        Me.ButtonItem20.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem20.Image = CType(resources.GetObject("ButtonItem20.Image"), System.Drawing.Image)
        Me.ButtonItem20.Name = "ButtonItem20"
        Me.ButtonItem20.SubItemsExpandWidth = 24
        Me.ButtonItem20.Text = "&Open..."
        '
        'ButtonItem21
        '
        Me.ButtonItem21.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem21.Image = CType(resources.GetObject("ButtonItem21.Image"), System.Drawing.Image)
        Me.ButtonItem21.Name = "ButtonItem21"
        Me.ButtonItem21.SubItemsExpandWidth = 24
        Me.ButtonItem21.Text = "&Save..."
        '
        'ButtonItem22
        '
        Me.ButtonItem22.BeginGroup = True
        Me.ButtonItem22.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem22.Image = CType(resources.GetObject("ButtonItem22.Image"), System.Drawing.Image)
        Me.ButtonItem22.Name = "ButtonItem22"
        Me.ButtonItem22.SubItemsExpandWidth = 24
        Me.ButtonItem22.Text = "S&hare..."
        '
        'ButtonItem23
        '
        Me.ButtonItem23.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem23.Image = CType(resources.GetObject("ButtonItem23.Image"), System.Drawing.Image)
        Me.ButtonItem23.Name = "ButtonItem23"
        Me.ButtonItem23.SubItemsExpandWidth = 24
        Me.ButtonItem23.Text = "&Print..."
        '
        'ButtonItem24
        '
        Me.ButtonItem24.BeginGroup = True
        Me.ButtonItem24.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem24.Image = CType(resources.GetObject("ButtonItem24.Image"), System.Drawing.Image)
        Me.ButtonItem24.Name = "ButtonItem24"
        Me.ButtonItem24.SubItemsExpandWidth = 24
        Me.ButtonItem24.Text = "&Close"
        '
        'GalleryContainer7
        '
        '
        '
        '
        Me.GalleryContainer7.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer7.EnableGalleryPopup = False
        Me.GalleryContainer7.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer7.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer7.MultiLine = False
        Me.GalleryContainer7.Name = "GalleryContainer7"
        Me.GalleryContainer7.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer7.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem7
        '
        Me.LabelItem7.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem7.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem7.CanCustomize = False
        Me.LabelItem7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem7.Name = "LabelItem7"
        '
        'ButtonItem25
        '
        Me.ButtonItem25.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem25.Image = CType(resources.GetObject("ButtonItem25.Image"), System.Drawing.Image)
        Me.ButtonItem25.Name = "ButtonItem25"
        Me.ButtonItem25.SubItemsExpandWidth = 24
        Me.ButtonItem25.Text = "&Save..."
        '
        'ButtonItem26
        '
        Me.ButtonItem26.BeginGroup = True
        Me.ButtonItem26.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem26.Image = CType(resources.GetObject("ButtonItem26.Image"), System.Drawing.Image)
        Me.ButtonItem26.Name = "ButtonItem26"
        Me.ButtonItem26.SubItemsExpandWidth = 24
        Me.ButtonItem26.Text = "S&hare..."
        '
        'ButtonItem27
        '
        Me.ButtonItem27.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem27.Image = CType(resources.GetObject("ButtonItem27.Image"), System.Drawing.Image)
        Me.ButtonItem27.Name = "ButtonItem27"
        Me.ButtonItem27.SubItemsExpandWidth = 24
        Me.ButtonItem27.Text = "&Print..."
        '
        'ButtonItem28
        '
        Me.ButtonItem28.BeginGroup = True
        Me.ButtonItem28.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem28.Image = CType(resources.GetObject("ButtonItem28.Image"), System.Drawing.Image)
        Me.ButtonItem28.Name = "ButtonItem28"
        Me.ButtonItem28.SubItemsExpandWidth = 24
        Me.ButtonItem28.Text = "&Close"
        '
        'GalleryContainer8
        '
        '
        '
        '
        Me.GalleryContainer8.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer8.EnableGalleryPopup = False
        Me.GalleryContainer8.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer8.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer8.MultiLine = False
        Me.GalleryContainer8.Name = "GalleryContainer8"
        Me.GalleryContainer8.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer8.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem9
        '
        Me.LabelItem9.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem9.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem9.CanCustomize = False
        Me.LabelItem9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem9.Name = "LabelItem9"
        '
        'GalleryContainer9
        '
        '
        '
        '
        Me.GalleryContainer9.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer9.EnableGalleryPopup = False
        Me.GalleryContainer9.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer9.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer9.MultiLine = False
        Me.GalleryContainer9.Name = "GalleryContainer9"
        Me.GalleryContainer9.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer9.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem10
        '
        Me.LabelItem10.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem10.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem10.CanCustomize = False
        Me.LabelItem10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem10.Name = "LabelItem10"
        '
        'ApplicationButton1
        '
        Me.ApplicationButton1.AutoExpandOnClick = True
        Me.ApplicationButton1.CanCustomize = False
        Me.ApplicationButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image
        Me.ApplicationButton1.Image = CType(resources.GetObject("ApplicationButton1.Image"), System.Drawing.Image)
        Me.ApplicationButton1.ImageFixedSize = New System.Drawing.Size(16, 16)
        Me.ApplicationButton1.ImagePaddingHorizontal = 0
        Me.ApplicationButton1.ImagePaddingVertical = 1
        Me.ApplicationButton1.Name = "ApplicationButton1"
        Me.ApplicationButton1.ShowSubItems = False
        Me.ApplicationButton1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer1})
        Me.ApplicationButton1.Text = "&File"
        '
        'ItemContainer1
        '
        '
        '
        '
        Me.ItemContainer1.BackgroundStyle.BackColor = System.Drawing.SystemColors.Highlight
        Me.ItemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer"
        Me.ItemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer1.Name = "ItemContainer1"
        Me.ItemContainer1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer2, Me.ItemContainer4})
        '
        '
        '
        Me.ItemContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer4
        '
        '
        '
        '
        Me.ItemContainer4.BackgroundStyle.Class = "RibbonFileMenuBottomContainer"
        Me.ItemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right
        Me.ItemContainer4.Name = "ItemContainer4"
        Me.ItemContainer4.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem13})
        '
        '
        '
        Me.ItemContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem13
        '
        Me.ButtonItem13.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonItem13.Image = CType(resources.GetObject("ButtonItem13.Image"), System.Drawing.Image)
        Me.ButtonItem13.Name = "ButtonItem13"
        Me.ButtonItem13.SubItemsExpandWidth = 24
        Me.ButtonItem13.Text = "E&xit"
        '
        'ItemContainer2
        '
        '
        '
        '
        Me.ItemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer2.ItemSpacing = 0
        Me.ItemContainer2.Name = "ItemContainer2"
        Me.ItemContainer2.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer3})
        '
        '
        '
        Me.ItemContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer3
        '
        '
        '
        '
        Me.ItemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer3.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer3.Name = "ItemContainer3"
        Me.ItemContainer3.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem2, Me.ButtonItem6})
        '
        '
        '
        Me.ItemContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem6
        '
        Me.ButtonItem6.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem6.Image = CType(resources.GetObject("ButtonItem6.Image"), System.Drawing.Image)
        Me.ButtonItem6.Name = "ButtonItem6"
        Me.ButtonItem6.SubItemsExpandWidth = 24
        Me.ButtonItem6.Text = "&Print..."
        '
        'ButtonItem2
        '
        Me.ButtonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem2.Image = CType(resources.GetObject("ButtonItem2.Image"), System.Drawing.Image)
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.SubItemsExpandWidth = 24
        Me.ButtonItem2.Text = "&New"
        '
        'RibbonControl1
        '
        Me.RibbonControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.RibbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonControl1.CaptionVisible = True
        Me.RibbonControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RibbonControl1.ForeColor = System.Drawing.Color.Black
        Me.RibbonControl1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.RibbonControl1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ApplicationButton1})
        Me.RibbonControl1.KeyTipsFont = New System.Drawing.Font("Tahoma", 7.0!)
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.Margin = New System.Windows.Forms.Padding(1, 0, 0, 0)
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Padding = New System.Windows.Forms.Padding(0, 0, 0, 3)
        Me.RibbonControl1.Size = New System.Drawing.Size(969, 60)
        Me.RibbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon"
        Me.RibbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon"
        Me.RibbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar"
        Me.RibbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>"
        Me.RibbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar..."
        Me.RibbonControl1.SystemText.QatDialogAddButton = "&Add >>"
        Me.RibbonControl1.SystemText.QatDialogCancelButton = "Cancel"
        Me.RibbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar"
        Me.RibbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:"
        Me.RibbonControl1.SystemText.QatDialogOkButton = "OK"
        Me.RibbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon"
        Me.RibbonControl1.SystemText.QatDialogRemoveButton = "&Remove"
        Me.RibbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon"
        Me.RibbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon"
        Me.RibbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar"
        Me.RibbonControl1.TabGroupHeight = 14
        Me.RibbonControl1.TabIndex = 3
        Me.RibbonControl1.Text = "RibbonControl1"
        '
        'GalleryContainer10
        '
        '
        '
        '
        Me.GalleryContainer10.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer10.EnableGalleryPopup = False
        Me.GalleryContainer10.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer10.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer10.MultiLine = False
        Me.GalleryContainer10.Name = "GalleryContainer10"
        Me.GalleryContainer10.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer10.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem11
        '
        Me.LabelItem11.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.LabelItem11.BorderType = DevComponents.DotNetBar.eBorderType.Etched
        Me.LabelItem11.CanCustomize = False
        Me.LabelItem11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem11.Name = "LabelItem11"
        '
        'TransaksiPenjualan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.BorderColor = New DevComponents.DotNetBar.Metro.BorderColors(System.Drawing.Color.Empty, System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer)), System.Drawing.Color.Empty, System.Drawing.Color.Empty)
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(969, 635)
        Me.ControlBox = True
        Me.Controls.Add(Me.lblPrintCount)
        Me.Controls.Add(Me.LabelX19)
        Me.Controls.Add(Me.txtPPN)
        Me.Controls.Add(Me.lblPPN)
        Me.Controls.Add(Me.LabelX18)
        Me.Controls.Add(Me.dgvTransaction)
        Me.Controls.Add(Me.txtRemark)
        Me.Controls.Add(Me.LabelX33)
        Me.Controls.Add(Me.LabelX32)
        Me.Controls.Add(Me.lblSubTotal)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.LabelX31)
        Me.Controls.Add(Me.txtReffNo)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblIdTransaksiHidden)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ddlSalesOrder)
        Me.Controls.Add(Me.ddlWarehouse)
        Me.Controls.Add(Me.txtDP)
        Me.Controls.Add(Me.txtOngkir)
        Me.Controls.Add(Me.txtDiscBulat)
        Me.Controls.Add(Me.LabelX30)
        Me.Controls.Add(Me.LabelX29)
        Me.Controls.Add(Me.LabelX28)
        Me.Controls.Add(Me.lblSubTotalHidden)
        Me.Controls.Add(Me.txtDiscNilai2)
        Me.Controls.Add(Me.txtDiscNilai1)
        Me.Controls.Add(Me.LabelX27)
        Me.Controls.Add(Me.LabelX20)
        Me.Controls.Add(Me.txtDisc1)
        Me.Controls.Add(Me.txtDisc2)
        Me.Controls.Add(Me.LabelX17)
        Me.Controls.Add(Me.txtNamaCustomer)
        Me.Controls.Add(Me.cbxCustomer)
        Me.Controls.Add(Me.LabelX16)
        Me.Controls.Add(Me.TextBoxX4)
        Me.Controls.Add(Me.LabelX23)
        Me.Controls.Add(Me.LabelX24)
        Me.Controls.Add(Me.LabelX25)
        Me.Controls.Add(Me.LabelX22)
        Me.Controls.Add(Me.LabelX21)
        Me.Controls.Add(Me.ButtonX4)
        Me.Controls.Add(Me.ButtonX2)
        Me.Controls.Add(Me.DataGridViewX1)
        Me.Controls.Add(Me.LabelX15)
        Me.Controls.Add(Me.LabelX8)
        Me.Controls.Add(Me.LabelX7)
        Me.Controls.Add(Me.ComboBoxEx1)
        Me.Controls.Add(Me.LabelX4)
        Me.Controls.Add(Me.LabelX2)
        Me.Controls.Add(Me.LabelX3)
        Me.Controls.Add(Me.DateTimeInput1)
        Me.Controls.Add(Me.LabelX14)
        Me.Controls.Add(Me.LabelX12)
        Me.Controls.Add(Me.LabelX13)
        Me.Controls.Add(Me.LabelX9)
        Me.Controls.Add(Me.LabelX10)
        Me.Controls.Add(Me.LabelX6)
        Me.Controls.Add(Me.LabelX11)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Controls.Add(Me.MetroStatusBar1)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.Name = "TransaksiPenjualan"
        Me.Padding = New System.Windows.Forms.Padding(0)
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Transaksi Penjualan"
        CType(Me.DateTimeInput1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridViewX1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTransaction, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents txtNamaCustomer As System.Windows.Forms.TextBox
    Private WithEvents MetroStatusBar1 As DevComponents.DotNetBar.Metro.MetroStatusBar
    Private WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Private WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX14 As DevComponents.DotNetBar.LabelX
    Private WithEvents DateTimeInput1 As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Private WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Private WithEvents ButtonX2 As DevComponents.DotNetBar.ButtonX
    Private WithEvents ButtonX4 As DevComponents.DotNetBar.ButtonX
    Private WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextBoxX4 As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX16 As DevComponents.DotNetBar.LabelX
    Private WithEvents cbxCustomer As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboBoxEx1 As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteRecordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Private WithEvents txtDisc2 As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents txtDisc1 As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX27 As DevComponents.DotNetBar.LabelX
    Private WithEvents txtDiscNilai1 As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents txtDiscNilai2 As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents lblSubTotalHidden As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX28 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX29 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX30 As DevComponents.DotNetBar.LabelX
    Private WithEvents txtDiscBulat As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents txtOngkir As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents txtDP As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ddlWarehouse As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ddlSalesOrder As DevComponents.DotNetBar.Controls.ComboBoxEx
    Private WithEvents btnNext As DevComponents.DotNetBar.ButtonX
    Private WithEvents btnPrev As DevComponents.DotNetBar.ButtonX
    Private WithEvents btnLast As DevComponents.DotNetBar.ButtonX
    Private WithEvents btnBegin As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Private WithEvents lblIdTransaksiHidden As DevComponents.DotNetBar.LabelX
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Public WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtReffNo As System.Windows.Forms.TextBox
    Private WithEvents LabelX31 As DevComponents.DotNetBar.LabelX
    Private WithEvents btnEdit As DevComponents.DotNetBar.ButtonX
    Private WithEvents btnNew As DevComponents.DotNetBar.ButtonX
    Private WithEvents lblSubTotal As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX32 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX33 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewX1 As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents dgvTransaction As System.Windows.Forms.DataGridView
    Private WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem5 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem7 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer1 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents labelItem8 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem8 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem9 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem10 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem11 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem12 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer2 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents GalleryContainer3 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem14 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem15 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem16 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer4 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Private WithEvents lblPPN As DevComponents.DotNetBar.LabelX
    Private WithEvents txtPPN As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblPrintCount As System.Windows.Forms.Label
    Friend WithEvents ButtonItem17 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem18 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem19 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer5 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents GalleryContainer6 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem6 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ApplicationButton2 As DevComponents.DotNetBar.ApplicationButton
    Friend WithEvents ItemContainer5 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer6 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer7 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ButtonItem20 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem21 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem22 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem23 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem24 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer7 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem7 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem25 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem26 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem27 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem28 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer8 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem9 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents GalleryContainer9 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem10 As DevComponents.DotNetBar.LabelItem
    Private WithEvents ApplicationButton1 As DevComponents.DotNetBar.ApplicationButton
    Private WithEvents ItemContainer1 As DevComponents.DotNetBar.ItemContainer
    Private WithEvents ItemContainer2 As DevComponents.DotNetBar.ItemContainer
    Private WithEvents ItemContainer3 As DevComponents.DotNetBar.ItemContainer
    Private WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem6 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ItemContainer4 As DevComponents.DotNetBar.ItemContainer
    Private WithEvents ButtonItem13 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonControl1 As DevComponents.DotNetBar.RibbonControl
    Friend WithEvents GalleryContainer10 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents LabelItem11 As DevComponents.DotNetBar.LabelItem
End Class
