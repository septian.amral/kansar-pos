Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class PopupTransJual

    Private Sub PopupTransJual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(91, Byte), Integer))
        koneksiDB()
        Dim masterpenjualan As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        masterpenjualan = New SqlDataAdapter("SELECT distinct dp.id_penjualan, " +
            "tp.tanggal_penjualan, dp.kode_barang, dp.qty, dp.harga_satuan, dp.Diskon, dp.DiskonNilai FROM detail_penjualan dp " +
            "INNER JOIN transaksi_penjualan tp ON tp.id_penjualan = dp.id_penjualan " +
            "order by dp.id_penjualan ", conn)
        masterpenjualan.Fill(dtBr)
        gridPopUp.DataSource = dtBr
        gridPopUp.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        gridPopUp.Columns(0).ReadOnly = True
        gridPopUp.Columns(1).ReadOnly = True
        txtBarang.Focus()

        TransaksiPenjualan.colors()
    End Sub

    Private Sub txtBarang_TextChanged_1(ByVal sender As Object, ByVal e As EventArgs) Handles txtBarang.TextChanged
        koneksiDB()
        Dim masterpenjualan As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        Dim query As String = "SELECT distinct dp.id_penjualan, " +
            "tp.tanggal_penjualan, dp.kode_barang, dp.qty, dp.harga_satuan, dp.Diskon, dp.DiskonNilai FROM detail_penjualan dp " +
            "INNER JOIN transaksi_penjualan tp ON tp.id_penjualan = dp.id_penjualan "

        masterpenjualan = New SqlDataAdapter(query + "where dp.kode_barang like '%" + txtBarang.Text + "%' order by dp.id_penjualan ", conn)
        masterpenjualan.Fill(dtBr)
        gridPopUp.DataSource = dtBr
        gridPopUp.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub

    Private Sub ButtonX1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonX1.Click
        Me.Close()
    End Sub

    Private Sub gridPopUp_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles gridPopUp.KeyDown
        If e.KeyCode = Keys.Enter Then
            'e.Handled = True

            Try
                Dim Dgvr As DataGridViewRow = gridPopUp.SelectedRows(0)
                TransaksiPenjualan.dgvTransaction.CurrentCell.Value = Dgvr.Cells("kode_barang").Value.ToString
                TransaksiPenjualan.dgvTransaction.Rows.Add(1)
                'TransaksiPenjualan.dgvTransaction.Rows(TransaksiPenjualan.dgvTransaction.Rows.Count - 1).Cells(0).Value = TransaksiPenjualan.dgvTransaction.Rows.Count
                'TransaksiPenjualan.dgvTransaction.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2
                'TransaksiPenjualan.dgvTransaction.EndEdit()
                'TransaksiPenjualan.dgvTransaction.Rows.Add(1)
                Me.Close()
            Catch ex As Exception
                Me.Close()
            End Try
        ElseIf e.KeyCode >= Keys.D0 And e.KeyCode <= Keys.Z Then
            txtBarang.Select()
            txtBarang.Text = ChrW(e.KeyValue)
            txtBarang.SelectionStart = 1
        End If
    End Sub

    Private Sub txtBarang_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBarang.KeyDown
        If e.KeyCode = Keys.Enter Then
            gridPopUp.Select()
        End If
    End Sub
End Class
