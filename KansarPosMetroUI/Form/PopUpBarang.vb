Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class PopUpBarang

    Private Sub PopUpBarang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(91, Byte), Integer))

        koneksiDB()
        Dim mastercustomer1 As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        mastercustomer1 = New SqlDataAdapter("SELECT * FROM master_customer order by nama_customer ", conn)
        mastercustomer1.Fill(dtBr)
        gridPopUp.DataSource = dtBr
        gridPopUp.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        gridPopUp.Columns(0).ReadOnly = True
        gridPopUp.Columns(1).ReadOnly = True

        txtBarang.Focus()
        'aturDGV()
        BindBarang()
        TransaksiPenjualan.colors()
    End Sub

    Sub aturDGV()
        Try
            gridPopUp.Columns.Add("No", "No.") '0
            gridPopUp.Columns.Add("kode_barang", "Kode Barang") '1
            gridPopUp.Columns.Add("nama_barang", "Nama Barang") '2
            gridPopUp.Columns.Add("jenis_barang", "Jenis Barang") '3
            gridPopUp.Columns.Add("harga_beli", "Harga Beli") '4
            gridPopUp.Columns.Add("harga_jual", "Harga Jual") '5
            gridPopUp.Columns.Add("stock_barang", "Stock Barang") '6

            gridPopUp.Columns(0).Visible = False
            gridPopUp.Columns(1).ReadOnly = True
            gridPopUp.Columns(2).ReadOnly = True
            gridPopUp.Columns(3).ReadOnly = True
            gridPopUp.Columns(4).ReadOnly = True
            gridPopUp.Columns(5).ReadOnly = True
            gridPopUp.Columns(6).ReadOnly = True
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindBarang()
        ddlBarang.Items.Clear()
        ddlBarang.Items.Add("nama_barang")
        ddlBarang.Items.Add("kode_barang")
        ddlBarang.Items.Add("jenis_barang")
        ddlBarang.Items.Add("harga_beli")
        ddlBarang.Items.Add("harga_jual")
        ddlBarang.Items.Add("stock_barang")
        ddlBarang.Text = "nama_barang"
    End Sub

    Private Sub gridPopUp_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles gridPopUp.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True

            Try
                Dim CMD As SqlCommand
                Dim DREADER As SqlDataReader
                Dim Dgvr As DataGridViewRow = gridPopUp.SelectedRows(0)
                'TransaksiPenjualan.kodebarang = Dgvr.Cells("kode_barang").Value.ToString

                koneksiDB()
                CMD = New SqlCommand("select * from master_barang where kode_barang ='" & Dgvr.Cells("kode_barang").Value.ToString & "'", conn)
                DREADER = CMD.ExecuteReader()
                DREADER.Read()
                If DREADER.HasRows Then
                    TransaksiPenjualan.DataGridViewX1.Rows.Add(1)

                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(1).Value = Dgvr.Cells("kode_barang").Value.ToString
                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(2).Value = DREADER.Item("nama_barang")
                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(3).Value = DREADER.Item("stock_barang")
                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(4).Value = 0
                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(5).Value = DREADER.Item("satuan")
                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(6).Value = DREADER.Item("harga_jual")
                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(7).Value = 0
                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(8).Value = 0
                    TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(9).Value = DREADER.Item("harga_jual")

                    TransaksiPenjualan.QTY = DREADER.Item("stock_barang")
                    TransaksiPenjualan.DataGridViewX1.Columns(4).ReadOnly = False
                    TransaksiPenjualan.DataGridViewX1.Columns(7).ReadOnly = False
                    TransaksiPenjualan.DataGridViewX1.Columns(8).ReadOnly = False

                    'JUMLAH COUN
                    Dim rowcount As Integer
                    rowcount = TransaksiPenjualan.DataGridViewX1.Rows.Count()
                    TransaksiPenjualan.LabelX23.Text = rowcount - 1
                End If
                DREADER.Close()
                TransaksiPenjualan.Hitung_total()

                'TransaksiPenjualan.DataGridViewX1.Rows.Add(1)
                'TransaksiPenjualan.DataGridViewX1.Rows(TransaksiPenjualan.DataGridViewX1.RowCount - 2).Cells(1).Value = Dgvr.Cells("kode_barang").Value.ToString

                Me.Close()
            Catch ex As Exception
                Me.Close()
            End Try
        ElseIf e.KeyCode >= Keys.D0 And e.KeyCode <= Keys.Z Then
            txtBarang.Select()
            txtBarang.Text = ChrW(e.KeyValue)
            txtBarang.SelectionStart = 1
        End If
    End Sub

    'Private Sub gridPopUp_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles gridPopUp.CellFormatting
    '    gridPopUp.Rows(e.RowIndex).HeaderCell.Value = CStr(e.RowIndex + 1)
    'End Sub

    Private Sub txtBarang_TextChanged_1(sender As Object, e As EventArgs) Handles txtBarang.TextChanged
        koneksiDB()
        Dim mastercustomer1 As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
       

        Dim baxx As String = "SELECT * FROM master_barang where " & ddlBarang.SelectedItem & " like '" + txtBarang.Text + "%' order by nama_barang "
        mastercustomer1 = New SqlDataAdapter("SELECT * FROM master_barang where " & ddlBarang.SelectedItem & " like '" + txtBarang.Text + "%' order by nama_barang ", conn)
        mastercustomer1.Fill(dtBr)
        gridPopUp.DataSource = dtBr
        gridPopUp.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        gridPopUp.Columns(0).ReadOnly = True
        gridPopUp.Columns(1).ReadOnly = True
        gridPopUp.Columns(2).ReadOnly = True
        gridPopUp.Columns(3).ReadOnly = True
        gridPopUp.Columns(4).ReadOnly = True
        gridPopUp.Columns(5).ReadOnly = True
        gridPopUp.Columns(6).ReadOnly = True

        'Dim i As Integer = 1
        'gridPopUp.Rows.Clear()
        'gridPopUp.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        'For Each r In dtBr.Rows
        '    gridPopUp.Rows.Add(1)
        '    gridPopUp.Rows(gridPopUp.RowCount - 2).Cells(0).Value = i
        '    gridPopUp.Rows(gridPopUp.RowCount - 2).Cells(1).Value = r.Item("kode_barang").ToString
        '    gridPopUp.Rows(gridPopUp.RowCount - 2).Cells(2).Value = r.Item("nama_barang").ToString
        '    gridPopUp.Rows(gridPopUp.RowCount - 2).Cells(3).Value = r.Item("jenis_barang").ToString
        '    gridPopUp.Rows(gridPopUp.RowCount - 2).Cells(4).Value = r.Item("harga_beli").ToString
        '    gridPopUp.Rows(gridPopUp.RowCount - 2).Cells(5).Value = r.Item("harga_jual").ToString
        '    gridPopUp.Rows(gridPopUp.RowCount - 2).Cells(6).Value = r.Item("stock_barang").ToString

        '    i = i + 1
        'Next
       
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        Me.Close()
    End Sub

   
    Private Sub txtBarang_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBarang.KeyDown
        If e.KeyCode = Keys.Enter Then
            gridPopUp.Select()
        End If
    End Sub
End Class
