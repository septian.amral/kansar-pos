Imports System.Data
Imports DevComponents.DotNetBar
Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports DevComponents.WinForms.Drawing
Imports System.Windows.Forms.VisualStyles

Public Class TransaksiPenjualan

    Dim f As Double
    Dim ds As DataSet
    Dim myadp As SqlDataAdapter
    Dim dt As New DataTable
    Dim query As String
    Dim DREADER As SqlDataReader
    Dim CMD As SqlCommand
    Dim sql As String
    Public QTY As Integer
    Dim delete_id As Integer
    Dim count As Integer
    Dim dtDetail As DataTable

    Private complex As ComplexPopup
    Private complexPopup As ComplexPopup
    Public kodebarang As String
    Public isEdit As Boolean = False

    Dim last_txtcustomer As String = ""

    Sub Kodeotomatis()
        koneksiDB()
        CMD = New SqlCommand("Select id_penjualan From transaksi_penjualan where id_penjualan in(select max(id_penjualan) from transaksi_penjualan) ", conn)
        DREADER = CMD.ExecuteReader
        DREADER.Read()
        'lblIdTransaksiHidden
        If DREADER.HasRows Then
            LabelX14.Text = Val(Microsoft.VisualBasic.Right(DREADER.Item("id_penjualan").ToString, 4)) + 1
            lblIdTransaksiHidden.Text = Val(Microsoft.VisualBasic.Right(DREADER.Item("id_penjualan").ToString, 4)) + 1
            If Len(LabelX14.Text) = 1 Then
                LabelX14.Text = "TRANS-" + "0000" & LabelX14.Text & ""
                lblIdTransaksiHidden.Text = "TRANS-" + "0000" & lblIdTransaksiHidden.Text & ""
            ElseIf Len(LabelX14.Text) = 2 Then
                LabelX14.Text = "TRANS-" + "000" & LabelX14.Text & ""
                lblIdTransaksiHidden.Text = "TRANS-" + "000" & lblIdTransaksiHidden.Text & ""
            ElseIf Len(LabelX14.Text) = 3 Then
                LabelX14.Text = "TRANS-" + "00" & LabelX14.Text & ""
                lblIdTransaksiHidden.Text = "TRANS-" + "00" & lblIdTransaksiHidden.Text & ""
            ElseIf Len(LabelX14.Text) = 4 Then
                LabelX14.Text = "TRANS-" + "0" & LabelX14.Text & ""
                lblIdTransaksiHidden.Text = "TRANS-" + "0" & lblIdTransaksiHidden.Text & ""
            End If
        Else
            LabelX14.Text = "TRANS-" + "00001"
            lblIdTransaksiHidden.Text = "TRANS-" + "00001"
        End If

        count = Convert.ToInt16(Microsoft.VisualBasic.Right(lblIdTransaksiHidden.Text, 5))

        DREADER.Close()
    End Sub

    Sub Hide_Maximize()
        Dim sysItems As ArrayList = RibbonControl1.RibbonStrip.GetItems("", GetType(SystemCaptionItem))
        Dim item As SystemCaptionItem
        For Each item In sysItems
            If Not item.IsSystemIcon Then
                item.RestoreMaximizeVisible = False
            End If
        Next
    End Sub

    Sub aturDGV()
        Try
            dgvTransaction.Columns.Add("No.", "Nomor") '0
            dgvTransaction.Columns.Add("kode_barang", "Kode Barang") '1
            dgvTransaction.Columns.Add("nama_barang", "Nama Barang") '2
            dgvTransaction.Columns.Add("stock_barang", "Stok") '3
            dgvTransaction.Columns.Add("stock_barang", "Qty") '4
            dgvTransaction.Columns.Add("satuan", "Satuan") '5
            dgvTransaction.Columns.Add("harga_jual", "Harga") '6
            dgvTransaction.Columns.Add("Diskon", "Diskon %") '7
            dgvTransaction.Columns.Add("Nilai", "Diskon Nilai") '8
            dgvTransaction.Columns.Add("Total", "Total") '9

            dgvTransaction.Columns(0).Visible = False
            dgvTransaction.Columns(0).ReadOnly = True
            dgvTransaction.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvTransaction.Columns(2).ReadOnly = True
            dgvTransaction.Columns(3).ReadOnly = True
            dgvTransaction.Columns(4).ReadOnly = True
            dgvTransaction.Columns(5).ReadOnly = True
            dgvTransaction.Columns(7).ReadOnly = True
            dgvTransaction.Columns(8).ReadOnly = True
            dgvTransaction.Columns(9).ReadOnly = True

            'dgvTransaction.Columns.Add("No.", "Nomor") '0
            'dgvTransaction.Columns.Add("kode_barang", "Kode Barang") '1
            'dgvTransaction.Columns.Add("nama_barang", "Nama Barang") '2
            'dgvTransaction.Columns.Add("stock_barang", "Stok") '3
            'dgvTransaction.Columns.Add("stock_barang", "Qty") '4
            'dgvTransaction.Columns.Add("satuan", "Satuan") '5
            'dgvTransaction.Columns.Add("harga_jual", "Harga") '6
            'dgvTransaction.Columns.Add("Diskon", "Diskon %") '7
            'dgvTransaction.Columns.Add("Nilai", "Diskon Nilai") '8
            'dgvTransaction.Columns.Add("Total", "Total") '9

            'dgvTransaction.Columns(0).Visible = False
            'dgvTransaction.Columns(0).ReadOnly = True
            'dgvTransaction.Columns(2).ReadOnly = True
            'dgvTransaction.Columns(3).ReadOnly = True
            'dgvTransaction.Columns(4).ReadOnly = True
            'dgvTransaction.Columns(5).ReadOnly = True
            ''dgvTransaction.Columns(6).ReadOnly = True
            'dgvTransaction.Columns(7).ReadOnly = True
            'dgvTransaction.Columns(8).ReadOnly = True
            'dgvTransaction.Columns(9).ReadOnly = True

        Catch ex As Exception
        End Try
    End Sub

    Public Sub Hitung_total()
        Dim total As Double
        Dim max As Integer = dgvTransaction.Rows.Count - 1
        total = 0
        For t As Integer = 0 To dgvTransaction.Rows.Count - 1
            total = total + Val(dgvTransaction.Rows(t).Cells(9).Value)
            '-----------cell 2 disini menunjukan posisi field yang akan kita jumlahkan
        Next

        'LabelX20.Text = Format(Val(total), " #,#")


        'TextBoxX4.Text = total
        'LabelX16.Text = total
        lblSubTotal.Text = total
        lblSubTotalHidden.Text = total
        txtPPN.Text = total * 0.1
        LabelX16.Text = Val(txtPPN.Text) + Val(lblSubTotalHidden.Text) - Val(txtDiscNilai1.Text) - Val(txtDiscNilai2.Text) - Val(txtDiscBulat.Text) + Val(txtOngkir.Text) - Val(txtDP.Text)
        TextBoxX4.Text = LabelX16.Text
    End Sub

    Sub save_transaksi()
        Try

            If txtNamaCustomer.Text = "" Then 'ComboBoxEx2
                MsgBox(" Silahkan Cek Kembali Data Anda ")
            Else
                'save gride view detail
                koneksiDB()
                conn.Close()
                'Inser Data To Transaksi Penjualan
                Dim id_penjualan1, tanggal_penjualan, customer, total_harga, jumlah_barang As String
                Dim Diskon1, DiskonNilai1, Diskon2, DiskonNilai2, DiskonBulat, Ongkir, DP, Bayar, Remark, PPN10 As String

                id_penjualan1 = "'" & Me.LabelX14.Text & "'"
                tanggal_penjualan = "'" & Format(DateTimeInput1.Value, "yyyy-MM-dd") & "'"
                customer = "'" & Me.txtNamaCustomer.Text & "'"
                total_harga = "'" & Me.LabelX16.Text & "'"
                jumlah_barang = "'" & Me.LabelX23.Text & "'"

                Remark = "'" & Me.txtRemark.Text & "'"
                Diskon1 = "'" & Me.txtDisc1.Text & "'"
                DiskonNilai1 = "'" & Me.txtDiscNilai1.Text & "'"
                Diskon2 = "'" & Me.txtDisc2.Text & "'"
                DiskonNilai2 = "'" & Me.txtDiscNilai2.Text & "'"
                DiskonBulat = "'" & Me.txtDiscBulat.Text & "'"
                Ongkir = "'" & Me.txtOngkir.Text & "'"
                DP = "'" & Me.txtDP.Text & "'"
                PPN10 = "'" & Me.txtPPN.Text & "'"
                Bayar = "'" & Kembalian.tbxNilaiBayar.Text.Replace(".", "").Replace(",", ".") & "'"

                Dim transaksi As New SqlCommand("insert into transaksi_penjualan (id_penjualan,tanggal_penjualan, customer, total_harga, jumlah_barang, " &
                                                "Diskon1,Diskon2,DiskonBulat,Ongkir,DP,Bayar,PPN10,Remark)values (" &
                                                id_penjualan1 & "," & tanggal_penjualan & "," & customer & "," & total_harga & "," & jumlah_barang &
                                                "," & Diskon1 & "," & Diskon2 &
                                                "," & DiskonBulat & "," & Ongkir & "," & DP & "," & Bayar & "," &
                                                PPN10 & "," & Remark & ")", conn)
                conn.Open()
                transaksi.ExecuteNonQuery()
                conn.Close()

                For baris As Integer = 0 To dgvTransaction.Rows.Count - 2
                    'Insert Data To Detail Penjualan
                    Dim id_penjualan, kode_barang, qty, harga_satuan, Diskon, DiskonNilai, Total As String
                    Dim no_urut As Integer = baris + 1

                    id_penjualan = "'" & Me.LabelX14.Text & "'"
                    kode_barang = "'" & Me.dgvTransaction.Rows(baris).Cells(1).Value & "'"
                    qty = "'" & Me.dgvTransaction.Rows(baris).Cells(4).Value & "'"
                    harga_satuan = "'" & Me.dgvTransaction.Rows(baris).Cells(6).Value & "'"
                    Diskon = "'" & Me.dgvTransaction.Rows(baris).Cells(7).Value & "'"
                    DiskonNilai = "'" & Me.dgvTransaction.Rows(baris).Cells(8).Value & "'"
                    'Diskon = Convert.ToInt32(Diskon1.Replace("'","") + Convert.ToInt32(Diskon2.Replace)
                    'DiskonNilai = Convert.ToInt32(DiskonNilai1) + Convert.ToInt32(DiskonNilai2)
                    Total = "'" & Me.dgvTransaction.Rows(baris).Cells(9).Value & "'"

                    Dim detail_penjualan As New SqlCommand("insert into detail_penjualan (id_penjualan, no_urut, kode_barang, qty, harga_satuan,Diskon,DiskonNilai,Total)values (" &
                                                           id_penjualan & "," & no_urut & "," & kode_barang & "," & qty & "," & harga_satuan & "," & Diskon & "," & DiskonNilai & "," & Total & ")", conn)
                    conn.Open()
                    detail_penjualan.ExecuteNonQuery()
                    conn.Close()
                Next

                MsgBox(" Data Berhasil Disimpan ")
                ButtonX4.Visible = False

                tampilDGV_stock()
            End If
        Catch ex As Exception
            MsgBox(" Kemungkinan ID Sama ")
        End Try
    End Sub

    'Private Class CSharpImpl
    '    <Obsolete("Please refactor calling code to use normal Visual Basic assignment")>
    '    Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
    '        target = value
    '        Return value
    '    End Function
    'End Class

    'Public Sub New()

    '    ' This call is required by the designer.
    '    InitializeComponent()
    '    complex = New Popup(CSharpImpl.__Assign(complexPopup, New ComplexPopup))
    '    complex.Resizable = True
    '    ' Add any initialization after the InitializeComponent() call.

    'End Sub

    Private Sub TransaksiPenjualan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.KeyPreview = True
        DateTimeInput1.Text = Today
        Kodeotomatis()
        tampil_comboboxBarang()
        'tampil_comboboxmastercustomer2()
        tampil_comboboxmastercustomer()
        tampil_comboboxWarehouse()
        tampil_comboboxSalesOrder()
        aturDGV()
        ComboBoxEx1.Text = ""
        cbxCustomer.Text = ""
        txtNamaCustomer.Text = ""
        LabelX8.Text = ""
        LabelX15.Text = ""
        LabelX21.Text = ""
        'Hide_Maximize()

        colors()

        txtNamaCustomer.Select()
    End Sub


    Public Sub colors()

        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(91, Byte), Integer))
        RibbonControl1.BackgroundStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(91, Byte), Integer))
        RibbonControl1.BackgroundStyle.BorderBottom = eStyleBorderType.Solid
        RibbonControl1.BackgroundStyle.BorderBottomColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(91, Byte), Integer))
        RibbonControl1.BackgroundStyle.BorderBottomWidth = 100

        RibbonControl1.BackgroundStyle.BorderTop = eStyleBorderType.Solid
        RibbonControl1.BackgroundStyle.BorderTopColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(91, Byte), Integer))
        'RibbonControl1.BackgroundStyle.BorderTopWidth = 100
        GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(91, Byte), Integer))

        TextBoxX4.BackColor = Color.Black
        TextBoxX4.ForeColor = Color.Yellow

        txtSearch.BackColor = Color.Black
        txtSearch.ForeColor = Color.Yellow

        txtDisc1.BackColor = Color.Black
        txtDisc1.ForeColor = Color.Beige

        txtDiscNilai1.BackColor = Color.Black
        txtDiscNilai1.ForeColor = Color.Beige

        txtDisc2.BackColor = Color.Black
        txtDisc2.ForeColor = Color.Beige

        txtDiscNilai2.BackColor = Color.Black
        txtDiscNilai2.ForeColor = Color.Beige

        txtDiscBulat.BackColor = Color.Black
        txtDiscBulat.ForeColor = Color.Orange

        txtPPN.BackColor = Color.Black
        txtPPN.ForeColor = Color.LightBlue

        txtOngkir.BackColor = Color.Black
        txtOngkir.ForeColor = Color.Purple

        txtDP.BackColor = Color.Black
        txtDP.ForeColor = Color.DarkBlue

        lblSubTotal.BackColor = Color.Black
        lblSubTotal.ForeColor = Color.Aqua

        'dgvTransaction.ColumnHeadersDefaultCellStyle.BackColor = Color.Green
        'dgvTransaction.ColumnHeadersDefaultCellStyle.ForeColor = Color.Green
        'dgvTransaction.RowHeadersDefaultCellStyle.BackColor = Color.Green
        'dgvTransaction.ColumnHeadersDefaultCellStyle.BackColor = Color.Blue
        'dgvTransaction.EnableHeadersVisualStyles = False
        'dgvTransaction.Columns(1).DefaultCellStyle.BackColor = Color.Blue
        'dgvTransaction.C = Color.Green

        'Dim columnHeaderStyle As DataGridViewCellStyle = New DataGridViewCellStyle
        'columnHeaderStyle.BackColor = Color.Blue
        'columnHeaderStyle.SelectionBackColor = Color.Green
        'dgvTransaction.ColumnHeadersDefaultCellStyle = columnHeaderStyle
        'dgvTransaction.BackgroundColor = Color.Green
        'dgvTransaction.ColumnHeadersDefaultCellStyle = columnHeaderStyle
    End Sub

    Private Sub TransaksiPenjualan_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.Up
                If dgvTransaction.CurrentCell IsNot Nothing Then
                    If dgvTransaction.CurrentCell.RowIndex = 0 Then
                        txtNamaCustomer.Select()
                        txtNamaCustomer.Focus
                    End If
                End If
            Case Keys.F1
                PopUpCustomer.Show()
                PopUpCustomer.gridPopUp.Select()
                colors()
            Case Keys.F2
                PopUpBarang.Show()
                PopUpBarang.gridPopUp.Select()
                colors()
            Case Keys.F3
                If dgvTransaction.CurrentCell IsNot Nothing Then
                    If dgvTransaction.CurrentCell.ColumnIndex = 1 Then
                        PopupTransJual.Show()
                        PopupTransJual.gridPopUp.Select()
                    End If
                End If
            Case Keys.F8
                If dgvTransaction.CurrentCell IsNot Nothing Then
                    dgvTransaction.Rows.RemoveAt(dgvTransaction.CurrentRow.Index)
                End If
            Case Keys.F9
                txtNamaCustomer.Select()
            Case Keys.F6 'cancel
                '...sintak anda
            Case Keys.F7 'close
                '...sintak anda
        End Select
    End Sub

    Private Sub ComboBoxEx1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxEx1.SelectedIndexChanged
        koneksiDB()
        conn.Close()
        'Dim Kode_barang As New MySqlCommand("SELECT  kode_barang FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        Dim nama_barang As New SqlCommand("SELECT  nama_barang FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        Dim satuan As New SqlCommand("SELECT  satuan FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        Dim harga_jual As New SqlCommand("SELECT  harga_jual FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        conn.Open()
        ' LabelX20.Text = Kode_barang.ExecuteScalar
        LabelX8.Text = nama_barang.ExecuteScalar
        LabelX15.Text = satuan.ExecuteScalar
        LabelX21.Text = harga_jual.ExecuteScalar
        conn.Close()
    End Sub

    Private Sub ButtonX3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'ADD Line
        dgvTransaction.Rows.Add(1)

        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(1).Value = ComboBoxEx1.Text 'Kode Barang
        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(2).Value = LabelX8.Text 'Nama Barang
        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(5).Value = LabelX15.Text 'Satuan
        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(6).Value = LabelX21.Text  'Harga
        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(7).Value = LabelX22.Text 'Diskon %
        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(8).Value = LabelX22.Text 'Diskon Nilai
        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(9).Value = LabelX22.Text 'Total

        'JUMLAH COUN
        Dim rowcount As Integer
        rowcount = dgvTransaction.Rows.Count()
        LabelX23.Text = rowcount - 1


        'RETURN


        ComboBoxEx1.Text = ""
        LabelX8.Text = ""
        LabelX15.Text = ""
        LabelX22.Text = ""
        LabelX21.Text = ""
        dgvTransaction.Update()
        ComboBoxEx1.Focus()
        Hitung_total()
    End Sub

    Private Sub TextBoxX2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            Dim harga, qty, hasil As Double
            harga = LabelX21.Text
            hasil = harga * qty
            LabelX22.Text = hasil

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ButtonX4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX4.Click

        Dim xx As Integer = dgvTransaction.Rows.Count
        If xx < 2 Then
            MessageBox.Show("Data Penjualan belum di isi !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Dim bayar, harga, kembali As Integer

            harga = TextBoxX4.Text
            kembali = bayar - harga
            Kembalian.tbxNilaiBayar.Text = bayar
            Kembalian.TextBoxX4.Text = kembali

            Kembalian.Show()
            tampil_comboboxJenisBayar()
            colors()
        End If
    End Sub

    Private Sub TextBoxX4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxX4.TextChanged
        If TextBoxX4.Text = "" Or Not IsNumeric(TextBoxX4.Text) Then
            Exit Sub
        End If
        f = TextBoxX4.Text
        TextBoxX4.Text = Format(f, "#,###")
        TextBoxX4.SelectionStart = Len(TextBoxX4.Text)
    End Sub

    Private Sub TextBoxX3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'If TextBoxX3.Text = "" Or Not IsNumeric(TextBoxX3.Text) Then
        '    Exit Sub
        'End If
        'f = TextBoxX3.Text
        'TextBoxX3.Text = Format(f, "#,###")
        'TextBoxX3.SelectionStart = Len(TextBoxX3.Text)
    End Sub

    Private Sub ButtonItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem2.Click
        'For Each iRow As DataGridViewRow In dgvTransaction.SelectedRows
        '    dgvTransaction.Rows.Clear()
        'Next
        dgvTransaction.Rows.Clear()
        ComboBoxEx1.Text = ""
        LabelX8.Text = ""
        LabelX15.Text = ""
        LabelX21.Text = ""
        cbxCustomer.Text = ""
        txtNamaCustomer.Text = ""
        TextBoxX4.Text = ""
        LabelX16.Text = ""
        LabelX23.Text = ""
        Kodeotomatis()

        lblSubTotalHidden.Text = 0
        lblSubTotal.Text = 0
        txtSearch.Text = ""
        txtDisc1.Text = 0
        txtDiscNilai1.Text = 0
        txtDisc2.Text = 0
        txtDiscNilai2.Text = 0
        txtDiscBulat.Text = 0
        txtOngkir.Text = 0
        txtDP.Text = 0
        btnBegin.Enabled = True
        btnPrev.Enabled = True
        btnNext.Enabled = True
        btnLast.Enabled = True

    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        Me.Close()
    End Sub

    Private Sub ButtonItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem6.Click
        'laporan.Load(Application.StartupPath & "/Laporan/Faktur.rpt")
        'Call SetingLaporan()
        'CetakFaktur.CrystalReportViewer1.ReportSource = laporan
        Try
            'laporan.SetParameterValue("nofaktur", LabelX14.Text)
            CetakFaktur.Show()

            koneksiDB()
            CMD = New SqlCommand("UPDATE transaksi_penjualan SET PrintCount = PrintCount + 1 WHERE id_penjualan = '" & LabelX14.Text & "'", conn)
            DREADER = CMD.ExecuteScalar()
            DREADER.Close()
            CMD.Dispose()

            Dim lblprint = lblPrintCount.Text.Split(":")
            Dim printCount As Integer = lblprint(1).Trim
            lblPrintCount.Text = "Total Cetak : " & (printCount + 1)
        Catch ex As Exception
            MsgBox("cek kembali parameter cetak anda")
        End Try

    End Sub


    Private Sub DataGridViewX1_CellEndEdit(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles dgvTransaction.CellEndEdit
        If dgvTransaction.CurrentCell IsNot Nothing Then
            If dgvTransaction.CurrentCell.ColumnIndex = 1 Then 'e.ColumnIndex = 1
                'Dim str() As String
                'Dim y As Boolean
                'Dim s As String = dgvTransaction.Rows(e.RowIndex).Cells(1).Value
                'If s = Nothing Then
                'Else
                '    y = s.Contains("-")
                'End If
                'If y Then
                '    Str = s.Split(CType("-", Char()))

                '    If cbKodeBarang.Checked Then
                '        dgvTransaction.Rows(e.RowIndex).Cells(1).Value = Str(0)
                '    Else
                '        dgvTransaction.Rows(e.RowIndex).Cells(1).Value = Str(1)
                '    End If
                'End If
                'dgvTransaction.Rows(e.RowIndex).Cells(1).Value = kodebarang

                koneksiDB()
                CMD = New SqlCommand("select * from master_barang where kode_barang ='" & dgvTransaction.Rows(e.RowIndex).Cells(1).Value & "'", conn)
                DREADER = CMD.ExecuteReader()
                DREADER.Read()
                If DREADER.HasRows Then
                    dgvTransaction.Rows(e.RowIndex).Cells(2).Value = DREADER.Item("nama_barang")
                    dgvTransaction.Rows(e.RowIndex).Cells(3).Value = DREADER.Item("stock_barang")
                    dgvTransaction.Rows(e.RowIndex).Cells(4).Value = 0
                    dgvTransaction.Rows(e.RowIndex).Cells(5).Value = DREADER.Item("satuan")
                    dgvTransaction.Rows(e.RowIndex).Cells(6).Value = DREADER.Item("harga_jual")
                    dgvTransaction.Rows(e.RowIndex).Cells(7).Value = 0
                    dgvTransaction.Rows(e.RowIndex).Cells(8).Value = 0
                    dgvTransaction.Rows(e.RowIndex).Cells(9).Value = dgvTransaction.Rows(e.RowIndex).Cells(4).Value * dgvTransaction.Rows(e.RowIndex).Cells(6).Value

                    QTY = DREADER.Item("stock_barang")
                    dgvTransaction.Columns(4).ReadOnly = False
                    dgvTransaction.Columns(7).ReadOnly = False
                    dgvTransaction.Columns(8).ReadOnly = False

                    'JUMLAH COUN
                    Dim rowcount As Integer
                    rowcount = dgvTransaction.Rows.Count()
                    LabelX23.Text = rowcount - 1

                    Hitung_total()
                Else
                    'MessageBox.Show("Kode Barang tidak valid !!")
                    dgvTransaction.Rows(e.RowIndex).Cells(1).Value = ""
                    dgvTransaction.Rows(e.RowIndex).Cells(2).Value = ""
                    dgvTransaction.Rows(e.RowIndex).Cells(3).Value = ""
                    dgvTransaction.Rows(e.RowIndex).Cells(4).Value = ""
                    dgvTransaction.Rows(e.RowIndex).Cells(5).Value = ""
                    dgvTransaction.Rows(e.RowIndex).Cells(6).Value = ""
                    dgvTransaction.Rows(e.RowIndex).Cells(7).Value = ""
                    dgvTransaction.Rows(e.RowIndex).Cells(8).Value = ""
                    dgvTransaction.Rows(e.RowIndex).Cells(9).Value = ""
                    'If Not String.IsNullOrEmpty(dgvTransaction.CurrentCell.Value) Then
                    'PopUpBarang.Show()
                    'End If
                End If
                DREADER.Close()
            End If
        End If

        If e.ColumnIndex = 4 Then
            If (dgvTransaction.Rows(e.RowIndex).Cells(4).Value > QTY) Then
                MessageBox.Show("QTY melebihi stok barang yang ada !!")
                dgvTransaction.Rows(e.RowIndex).Cells(3).Value = 0
                dgvTransaction.Rows(e.RowIndex).Cells(7).Value = 0
                dgvTransaction.Rows(e.RowIndex).Cells(8).Value = 0
            Else
                Dim stock_barang, harga_jual, disc, discNilai, total As Decimal

                stock_barang = dgvTransaction.Rows(e.RowIndex).Cells(4).Value
                harga_jual = dgvTransaction.Rows(e.RowIndex).Cells(6).Value
                disc = dgvTransaction.Rows(e.RowIndex).Cells(7).Value
                discNilai = dgvTransaction.Rows(e.RowIndex).Cells(8).Value

                total = (disc / 100) * harga_jual
                total = harga_jual - total

                dgvTransaction.Rows(e.RowIndex).Cells(9).Value = Val(total * stock_barang) - discNilai
            End If
        End If

        If e.ColumnIndex = 6 Then
            Dim stock_barang, harga_jual, disc, discNilai, total As Decimal

            stock_barang = dgvTransaction.Rows(e.RowIndex).Cells(4).Value
            harga_jual = dgvTransaction.Rows(e.RowIndex).Cells(6).Value
            disc = dgvTransaction.Rows(e.RowIndex).Cells(7).Value
            discNilai = dgvTransaction.Rows(e.RowIndex).Cells(8).Value

            total = (disc / 100) * harga_jual
            total = harga_jual - total

            dgvTransaction.Rows(e.RowIndex).Cells(9).Value = Val(total * stock_barang) - discNilai
        End If

        If e.ColumnIndex = 7 Then
            Dim stock_barang, harga_jual, disc, discNilai, total As Decimal

            stock_barang = dgvTransaction.Rows(e.RowIndex).Cells(4).Value
            harga_jual = dgvTransaction.Rows(e.RowIndex).Cells(6).Value
            disc = dgvTransaction.Rows(e.RowIndex).Cells(7).Value
            discNilai = dgvTransaction.Rows(e.RowIndex).Cells(8).Value

            total = (disc / 100) * harga_jual
            total = harga_jual - total

            dgvTransaction.Rows(e.RowIndex).Cells(9).Value = Val(total * stock_barang) - discNilai
        End If

        If e.ColumnIndex = 8 Then
            Dim stock_barang, harga_jual, disc, discNilai, total As Decimal

            stock_barang = dgvTransaction.Rows(e.RowIndex).Cells(4).Value
            harga_jual = dgvTransaction.Rows(e.RowIndex).Cells(6).Value
            disc = dgvTransaction.Rows(e.RowIndex).Cells(7).Value
            discNilai = dgvTransaction.Rows(e.RowIndex).Cells(8).Value

            total = (disc / 100) * harga_jual
            total = harga_jual - total

            dgvTransaction.Rows(e.RowIndex).Cells(9).Value = Val(total * stock_barang) - discNilai
        End If

        Hitung_total()
    End Sub

    Dim HasAddKeyDown As Boolean = False

    Private Sub DataGridViewX1_EditingControlShowing(ByVal sender As Object, ByVal e As DataGridViewEditingControlShowingEventArgs) Handles dgvTransaction.EditingControlShowing

        If Not HasAddKeyDown Then
            AddHandler e.Control.KeyDown, AddressOf DataGridViewX1_KeyDown
            HasAddKeyDown = True
        End If

        koneksiDB()
        Dim namaBr As New SqlDataAdapter
        Dim dtBr As New DataTable
        Dim AutoComp As New AutoCompleteStringCollection()

        conn.Close()
        conn.Open()

        namaBr = New SqlDataAdapter("SELECT kode_barang FROM master_barang order by kode_barang ", conn)

        Try
            Dim tb As New TextBox
            namaBr.Fill(dtBr)
            Dim r As DataRow
            For Each r In dtBr.Rows
                If (dgvTransaction.CurrentCell.ColumnIndex = 1) Then
                    'tb = e.Control
                    'tb.AutoCompleteCustomSource.Add(r.Item("kode_barang").ToString)
                    'tb.AutoCompleteMode = AutoCompleteMode.Suggest
                    'tb.AutoCompleteSource = AutoCompleteSource.CustomSource

                    AutoComp.Add(r.Item("kode_barang").ToString)
                Else
                    tb = e.Control
                    tb.AutoCompleteMode = AutoCompleteMode.None
                End If
            Next
            If (dgvTransaction.CurrentCell.ColumnIndex = 1) Then
                tb = e.Control
                tb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                tb.AutoCompleteSource = AutoCompleteSource.CustomSource
                tb.AutoCompleteCustomSource = AutoComp

                'AddHandler tb.KeyDown, AddressOf DataGridViewX1_KeyDown
            End If

            'kolom QTY format angka
            If dgvTransaction.CurrentCell.ColumnIndex = 4 Then
                AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBoxQTY_keyPress
            End If

            'kolom Harga format angka
            If dgvTransaction.CurrentCell.ColumnIndex = 6 Then
                AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBoxHarga_keyPress
            End If

            'kolom diskon format decimal
            If dgvTransaction.CurrentCell.ColumnIndex = 7 Then
                AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBoxDiskon_keyPress
            End If

            'kolom diskon nIlai format angka
            If dgvTransaction.CurrentCell.ColumnIndex = 8 Then
                AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBoxDiskonNilai_keyPress
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'conn.Close()
    End Sub

    Private Sub TextBoxQTY_keyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        'only numeric
        'If Char.IsDigit(CChar(CStr(e.KeyChar))) = False Then e.Handled = True

        'numerin and titik
        'If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True

        'numeric and backspace
        If dgvTransaction.CurrentCell.ColumnIndex = 4 Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxHarga_keyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        'only numeric
        'If Char.IsDigit(CChar(CStr(e.KeyChar))) = False Then e.Handled = True

        'numerin and titik
        'If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True

        'numeric and backspace
        If dgvTransaction.CurrentCell.ColumnIndex = 6 Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxDiskon_keyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        If dgvTransaction.CurrentCell.ColumnIndex = 7 Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = "." Or Asc((e.KeyChar)) = "8") Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TextBoxDiskonNilai_keyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        'only numeric
        'If Char.IsDigit(CChar(CStr(e.KeyChar))) = False Then e.Handled = True

        'numerin and titik
        'If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True

        'numeric and backspace
        If dgvTransaction.CurrentCell.ColumnIndex = 8 Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub DataGridViewX1_CellMouseUp(ByVal sender As Object, ByVal e As DataGridViewCellMouseEventArgs) Handles dgvTransaction.CellMouseUp
        If e.Button = MouseButtons.Right Then
            'delete_id = Convert.ToInt32(dgvTransaction.Rows(e.RowIndex).Cells("id").Value.ToString())
            delete_id = e.RowIndex.ToString()
            Me.ContextMenuStrip1.Show(Me.dgvTransaction, e.Location)
            ContextMenuStrip1.Show(Cursor.Position)
        End If
    End Sub

    Private Sub DeleteRecordToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles DeleteRecordToolStripMenuItem.Click
        Try
            dgvTransaction.Rows.RemoveAt(delete_id)
            Hitung_total()
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub DataGridViewX1_CellFormatting(ByVal sender As Object, ByVal e As DataGridViewCellFormattingEventArgs) Handles dgvTransaction.CellFormatting
        dgvTransaction.Rows(e.RowIndex).HeaderCell.Value = CStr(e.RowIndex + 1)
        'Dim row = dgvTransaction.Rows(e.RowIndex)
        'If Not row.IsNewRow AndAlso e.ColumnIndex = 0 Then
        '    Dim cell = row.Cells(0)
        '    Dim value = e.RowIndex + 1

        '    If cell.Value Is DBNull.Value OrElse CInt(cell.Value) <> value Then
        '        cell.Value = e.RowIndex + 1
        '    End If
        'End If
    End Sub

    Private Sub lblSubTotal_TextChanged_1(ByVal sender As Object, ByVal e As EventArgs) Handles lblSubTotal.TextChanged
        If lblSubTotal.Text = "" Or Not IsNumeric(lblSubTotal.Text) Then
            Exit Sub
        End If
        f = lblSubTotal.Text
        lblSubTotal.Text = Format(f, "#,###")
    End Sub

    Private Sub txtDisc1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txtDisc1.KeyPress
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = "." Or Asc((e.KeyChar)) = "8") Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtDisc1_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtDisc1.KeyUp
        Dim subtotal, disc, total As Double
        subtotal = Val(lblSubTotalHidden.Text)
        disc = Val(txtDisc1.Text)
        total = (disc / 100) * subtotal
        total = subtotal - total

        'TextBoxX4.Text = total
        'LabelX16.Text = total
        txtDiscNilai1.Text = (disc * subtotal) / 100
        LabelX16.Text = Val(lblSubTotalHidden.Text) - Val(txtDiscNilai1.Text) - Val(txtDiscNilai2.Text) - Val(txtDiscBulat.Text) + Val(txtOngkir.Text) - Val(txtDP.Text)
        TextBoxX4.Text = LabelX16.Text
    End Sub

    Private Sub txtDiscNilai1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txtDiscNilai1.KeyPress
        'numeric and backspace
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtDiscNilai1_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtDiscNilai1.KeyUp
        LabelX16.Text = Val(lblSubTotalHidden.Text) - Val(txtDiscNilai1.Text) - Val(txtDiscNilai2.Text) - Val(txtDiscBulat.Text) + Val(txtOngkir.Text) - Val(txtDP.Text)
        TextBoxX4.Text = LabelX16.Text
        txtDisc1.Text = (Val(txtDiscNilai1.Text) / Val(lblSubTotalHidden.Text)) * 100 'LabelX16    TextBoxX4
    End Sub

    Private Sub txtDisc2_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txtDisc2.KeyPress
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = "." Or Asc((e.KeyChar)) = "8") Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtDisc2_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtDisc2.KeyUp
        Dim subtotal, disc, total As Decimal
        subtotal = Val(lblSubTotalHidden.Text)
        disc = Val(txtDisc2.Text)
        total = (disc / 100) * subtotal
        total = subtotal - total

        'TextBoxX4.Text = total
        'LabelX16.Text = total
        txtDiscNilai2.Text = (disc * subtotal) / 100
        LabelX16.Text = Val(lblSubTotalHidden.Text) - Val(txtDiscNilai1.Text) - Val(txtDiscNilai2.Text) - Val(txtDiscBulat.Text) + Val(txtOngkir.Text) - Val(txtDP.Text)
        TextBoxX4.Text = LabelX16.Text
    End Sub

    Private Sub txtDiscNilai2_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txtDiscNilai2.KeyPress
        'numeric and backspace
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
            e.Handled = True
        End If
    End Sub
    Private Sub txtDiscNilai2_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtDiscNilai2.KeyUp
        LabelX16.Text = Val(lblSubTotalHidden.Text) - Val(txtDiscNilai1.Text) - Val(txtDiscNilai2.Text) - Val(txtDiscBulat.Text) + Val(txtOngkir.Text) - Val(txtDP.Text)
        TextBoxX4.Text = LabelX16.Text
        txtDisc2.Text = (Val(txtDiscNilai2.Text) / Val(lblSubTotalHidden.Text)) * 100 'LabelX16    TextBoxX4
    End Sub

    Private Sub txtDiscBulat_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtDiscBulat.KeyUp
        LabelX16.Text = Val(lblSubTotalHidden.Text) - Val(txtDiscNilai1.Text) - Val(txtDiscNilai2.Text) - Val(txtDiscBulat.Text) + Val(txtOngkir.Text) - Val(txtDP.Text)
        TextBoxX4.Text = LabelX16.Text
    End Sub

    Private Sub txtOngkir_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtOngkir.KeyUp
        LabelX16.Text = Val(lblSubTotalHidden.Text) - Val(txtDiscNilai1.Text) - Val(txtDiscNilai2.Text) - Val(txtDiscBulat.Text) + Val(txtOngkir.Text) - Val(txtDP.Text)
        TextBoxX4.Text = LabelX16.Text
    End Sub

    Private Sub txtDP_KeyUp(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtDP.KeyUp
        LabelX16.Text = Val(lblSubTotalHidden.Text) - Val(txtDiscNilai1.Text) - Val(txtDiscNilai2.Text) - Val(txtDiscBulat.Text) + Val(txtOngkir.Text) - Val(txtDP.Text)
        TextBoxX4.Text = LabelX16.Text
    End Sub

    Private Sub TextBoxX3_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        'numeric and backspace
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
            e.Handled = True
        End If
    End Sub

    Private Sub getDataHeader()
        koneksiDB()
        CMD = New SqlCommand("select * from transaksi_penjualan WHERE cast(right(id_penjualan,5) as int) = " & count, conn)
        DREADER = CMD.ExecuteReader()
        DREADER.Read()
        If DREADER.HasRows Then
            LabelX14.Text = DREADER.Item("id_penjualan")
            DateTimeInput1.Value = DREADER.Item("tanggal_penjualan")
            DateTimeInput1.Format = DevComponents.Editors.eDateTimePickerFormat.Long
            txtNamaCustomer.Text = DREADER.Item("customer")
            LabelX16.Text = DREADER.Item("total_harga")

            If DREADER.Item("Bayar") >= DREADER.Item("total_harga") Then
                TextBoxX4.Text = "PAID - " & LabelX16.Text
            Else
                TextBoxX4.Text = LabelX16.Text
            End If

            LabelX23.Text = DREADER.Item("jumlah_barang")

            txtRemark.Text = DREADER.Item("Remark")
            txtDisc1.Text = IIf(IsDBNull(DREADER.Item("Diskon1")), 0, DREADER.Item("Diskon1"))
            'txtDiscNilai1.Text = IIf(IsDBNull(DREADER.Item("DiskonNilai1")), 0, DREADER.Item("DiskonNilai1"))
            txtDisc2.Text = IIf(IsDBNull(DREADER.Item("Diskon2")), 0, DREADER.Item("Diskon2"))
            'txtDiscNilai2.Text = IIf(IsDBNull(DREADER.Item("DiskonNilai2")), 0, DREADER.Item("DiskonNilai2"))
            txtDiscBulat.Text = IIf(IsDBNull(DREADER.Item("DiskonBulat")), 0, DREADER.Item("DiskonBulat"))
            txtOngkir.Text = IIf(IsDBNull(DREADER.Item("Ongkir")), 0, DREADER.Item("Ongkir"))
            txtDP.Text = IIf(IsDBNull(DREADER.Item("DP")), 0, DREADER.Item("DP"))
            lblPrintCount.Text = IIf(IsDBNull(DREADER.Item("PrintCount")), "Total Cetak : 0", "Total Cetak : " & DREADER.Item("PrintCount"))
        Else
            'LabelX14.Text = lblIdTransaksiHidden.Text
            txtNamaCustomer.Text = ""
            LabelX16.Text = 0
            TextBoxX4.Text = 0
            LabelX23.Text = ""
            txtDisc1.Text = 0
            txtDiscNilai1.Text = 0
            txtDisc2.Text = 0
            txtDiscNilai2.Text = 0
            txtDiscBulat.Text = 0
            txtOngkir.Text = 0
            txtDP.Text = 0
            lblSubTotal.Text = ""
            lblPrintCount.Text = "Total Cetak : 0"
        End If
        DREADER.Close()
    End Sub
    Private Sub getDataHeader2()
        koneksiDB()
        CMD = New SqlCommand("select * from transaksi_penjualan WHERE id_penjualan = '" & txtSearch.Text & "'", conn)
        DREADER = CMD.ExecuteReader()
        DREADER.Read()
        If DREADER.HasRows Then
            count = Convert.ToInt16(Microsoft.VisualBasic.Right(txtSearch.Text, 5))

            LabelX14.Text = DREADER.Item("id_penjualan")
            DateTimeInput1.Value = DREADER.Item("tanggal_penjualan")
            DateTimeInput1.Format = DevComponents.Editors.eDateTimePickerFormat.Long
            txtNamaCustomer.Text = DREADER.Item("customer")
            LabelX16.Text = DREADER.Item("total_harga")
            TextBoxX4.Text = LabelX16.Text
            LabelX23.Text = DREADER.Item("jumlah_barang")

            txtRemark.Text = DREADER.Item("Remark")
            txtDisc1.Text = IIf(IsDBNull(DREADER.Item("Diskon1")), 0, DREADER.Item("Diskon1"))
            txtDiscNilai1.Text = IIf(IsDBNull(DREADER.Item("DiskonNilai1")), 0, DREADER.Item("DiskonNilai1"))
            txtDisc2.Text = IIf(IsDBNull(DREADER.Item("Diskon2")), 0, DREADER.Item("Diskon2"))
            txtDiscNilai2.Text = IIf(IsDBNull(DREADER.Item("DiskonNilai2")), 0, DREADER.Item("DiskonNilai2"))
            txtDiscBulat.Text = IIf(IsDBNull(DREADER.Item("DiskonBulat")), 0, DREADER.Item("DiskonBulat"))
            txtOngkir.Text = IIf(IsDBNull(DREADER.Item("Ongkir")), 0, DREADER.Item("Ongkir"))
            txtDP.Text = IIf(IsDBNull(DREADER.Item("DP")), 0, DREADER.Item("DP"))
        Else
            LabelX14.Text = lblIdTransaksiHidden.Text
            txtNamaCustomer.Text = ""
            LabelX16.Text = 0
            TextBoxX4.Text = 0
            LabelX23.Text = ""
            txtDisc1.Text = 0
            txtDiscNilai1.Text = 0
            txtDisc2.Text = 0
            txtDiscNilai2.Text = 0
            txtDiscBulat.Text = 0
            txtOngkir.Text = 0
            txtDP.Text = 0
            lblSubTotal.Text = ""
        End If
        DREADER.Close()
    End Sub
    Private Sub getDataDetail2()
        Dim penjualan As New SqlDataAdapter
        dtDetail = New Data.DataTable
        conn.Close()
        conn.Open()
        penjualan = New SqlDataAdapter("SELECT a.kode_barang,b.nama_barang,b.stock_barang as stock_barang2, a.qty as stock_barang, a.harga_satuan as harga_jual,b.satuan,a.Diskon,a.DiskonNilai,a.Total FROM detail_penjualan a " +
                                       "inner join master_barang b on a.kode_barang = b.kode_barang " +
                                       "WHERE id_penjualan = '" & txtSearch.Text & "'", conn)
        penjualan.Fill(dtDetail)

        Dim i As Integer = 1
        dgvTransaction.Rows.Clear()
        For Each r In dtDetail.Rows
            dgvTransaction.Rows.Add(1)
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(0).Value = i
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(1).Value = r.Item("kode_barang").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(2).Value = r.Item("nama_barang").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(3).Value = r.Item("stock_barang2").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(4).Value = r.Item("stock_barang").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(5).Value = r.Item("satuan").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(6).Value = r.Item("harga_jual").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(7).Value = r.Item("Diskon").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(8).Value = r.Item("DiskonNilai").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(9).Value = r.Item("Total").ToString

            i = i + 1
        Next
        conn.Close()
    End Sub
    Private Sub getDataDetail3(ByVal id As String)
        Dim penjualan As New SqlDataAdapter
        dtDetail = New Data.DataTable
        conn.Close()
        conn.Open()
        penjualan = New SqlDataAdapter("SELECT a.kode_barang,b.nama_barang,b.stock_barang as stock_barang2, a.qty as stock_barang, a.harga_satuan as harga_jual,b.satuan,a.Diskon,a.DiskonNilai,a.Total FROM detail_penjualan a " +
                                       "inner join master_barang b on a.kode_barang = b.kode_barang " +
                                       "WHERE id_penjualan = '" & id & "'", conn)
        penjualan.Fill(dtDetail)

        If dtDetail.Rows.Count > 0 Then
            Dim i As Integer = 1
            dgvTransaction.Rows.Clear()
            For Each r In dtDetail.Rows
                dgvTransaction.Rows.Add(1)
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(0).Value = i
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(1).Value = r.Item("kode_barang").ToString
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(2).Value = r.Item("nama_barang").ToString
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(3).Value = r.Item("stock_barang2").ToString
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(4).Value = r.Item("stock_barang").ToString
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(5).Value = r.Item("satuan").ToString
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(6).Value = r.Item("harga_jual").ToString
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(7).Value = r.Item("Diskon").ToString
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(8).Value = r.Item("DiskonNilai").ToString
                dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(9).Value = r.Item("Total").ToString

                i = i + 1
            Next
            conn.Close()
            Hitung_total()
        Else
            MessageBox.Show("Kode Barang tidak valid!!")
        End If

    End Sub
    Private Sub getDataDetail()
        Dim penjualan As New SqlDataAdapter
        dtDetail = New Data.DataTable
        conn.Close()
        conn.Open()
        penjualan = New SqlDataAdapter("SELECT a.kode_barang,b.nama_barang,b.stock_barang as stock_barang2, a.qty as stock_barang, a.harga_satuan as harga_jual,b.satuan,a.Diskon,a.DiskonNilai,a.Total FROM detail_penjualan a " +
                                       "inner join master_barang b on a.kode_barang = b.kode_barang " +
                                       "WHERE cast(right(id_penjualan,5) as int) = " & count, conn)
        penjualan.Fill(dtDetail)

        Dim i As Integer = 1
        dgvTransaction.Rows.Clear()
        For Each r In dtDetail.Rows
            dgvTransaction.Rows.Add(1)
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(0).Value = i
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(1).Value = r.Item("kode_barang").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(2).Value = r.Item("nama_barang").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(3).Value = r.Item("stock_barang2").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(4).Value = r.Item("stock_barang").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(5).Value = r.Item("satuan").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(6).Value = r.Item("harga_jual").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(7).Value = r.Item("Diskon").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(8).Value = r.Item("DiskonNilai").ToString
            dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(9).Value = r.Item("Total").ToString
            'dgvTransaction.Rows(i - 1).Cells(0).Value = i
            'dgvTransaction.Rows(i - 1).Cells(1).Value = r.Item("kode_barang").ToString
            'dgvTransaction.Rows(i - 1).Cells(2).Value = r.Item("nama_barang").ToString
            'dgvTransaction.Rows(i - 1).Cells(3).Value = r.Item("stock_barang").ToString
            'dgvTransaction.Rows(i - 1).Cells(4).Value = r.Item("satuan").ToString
            'dgvTransaction.Rows(i - 1).Cells(5).Value = r.Item("harga_jual").ToString
            'dgvTransaction.Rows(i - 1).Cells(6).Value = r.Item("Diskon").ToString
            'dgvTransaction.Rows(i - 1).Cells(7).Value = r.Item("DiskonNilai").ToString
            'dgvTransaction.Rows(i - 1).Cells(8).Value = r.Item("Total").ToString

            i = i + 1
        Next
        conn.Close()
    End Sub
    Private Sub getDataDetailIsLast()
        Dim penjualan As New SqlDataAdapter
        dtDetail = New Data.DataTable
        conn.Close()
        conn.Open()
        penjualan = New SqlDataAdapter("SELECT a.kode_barang,b.nama_barang,b.stock_barang as stock_barang2, a.qty as stock_barang, a.harga_satuan as harga_jual,b.satuan,a.Diskon,a.DiskonNilai,a.Total FROM detail_penjualan a " +
                                       "inner join master_barang b on a.kode_barang = b.kode_barang " +
                                       "WHERE cast(right(id_penjualan,5) as int) = " & count + 1, conn)
        penjualan.Fill(dtDetail)
        conn.Close()
    End Sub
    Private Sub getDataDetailIsBegin()
        Dim penjualan As New SqlDataAdapter
        dtDetail = New Data.DataTable
        conn.Close()
        conn.Open()
        penjualan = New SqlDataAdapter("SELECT a.kode_barang,b.nama_barang,b.stock_barang as stock_barang2, a.qty as stock_barang, a.harga_satuan as harga_jual,b.satuan,a.Diskon,a.DiskonNilai,a.Total FROM detail_penjualan a " +
                                       "inner join master_barang b on a.kode_barang = b.kode_barang " +
                                       "WHERE cast(right(id_penjualan,5) as int) = " & count - 1, conn)
        penjualan.Fill(dtDetail)
        conn.Close()
    End Sub
    Private Sub getDataDetailMin()
        Dim penjualan As New SqlDataAdapter
        dtDetail = New Data.DataTable
        conn.Close()
        conn.Open()
        penjualan = New SqlDataAdapter("SELECT a.kode_barang,b.nama_barang,b.stock_barang as stock_barang2, a.qty as stock_barang, a.harga_satuan as harga_jual,b.satuan,a.Diskon,a.DiskonNilai,a.Total FROM detail_penjualan a " +
                                       "inner join master_barang b on a.kode_barang = b.kode_barang " +
                                       "WHERE cast(right(id_penjualan,5) as int) = cast(right((select MIN(id_penjualan) from detail_penjualan),5) as int)", conn)
        penjualan.Fill(dtDetail)

        Dim i As Integer = 0
        dgvTransaction.Rows.Clear()
        For Each r In dtDetail.Rows
            dgvTransaction.Rows.Add(1)
            dgvTransaction.Rows(i).Cells(0).Value = i + 1
            dgvTransaction.Rows(i).Cells(1).Value = r.Item("kode_barang").ToString
            dgvTransaction.Rows(i).Cells(2).Value = r.Item("nama_barang").ToString
            dgvTransaction.Rows(i).Cells(3).Value = r.Item("stock_barang2").ToString
            dgvTransaction.Rows(i).Cells(4).Value = r.Item("stock_barang").ToString
            dgvTransaction.Rows(i).Cells(5).Value = r.Item("satuan").ToString
            dgvTransaction.Rows(i).Cells(6).Value = r.Item("harga_jual").ToString
            dgvTransaction.Rows(i).Cells(7).Value = r.Item("Diskon").ToString
            dgvTransaction.Rows(i).Cells(8).Value = r.Item("DiskonNilai").ToString
            dgvTransaction.Rows(i).Cells(9).Value = r.Item("Total").ToString

            i = i + 1
        Next
        conn.Close()
    End Sub
    Private Sub getDataDetailMax()
        Dim penjualan As New SqlDataAdapter
        dtDetail = New Data.DataTable
        conn.Close()
        conn.Open()
        penjualan = New SqlDataAdapter("SELECT a.kode_barang,b.nama_barang,b.stock_barang as stock_barang2, a.qty as stock_barang, a.harga_satuan as harga_jual,b.satuan,a.Diskon,a.DiskonNilai,a.Total FROM detail_penjualan a " +
                                       "inner join master_barang b on a.kode_barang = b.kode_barang " +
                                       "WHERE cast(right(id_penjualan,5) as int) = cast(right((select MAX(id_penjualan) from detail_penjualan),5) as int)", conn)
        penjualan.Fill(dtDetail)

        Dim i As Integer = 0
        dgvTransaction.Rows.Clear()
        For Each r In dtDetail.Rows
            dgvTransaction.Rows.Add(1)
            dgvTransaction.Rows(i).Cells(0).Value = i + 1
            dgvTransaction.Rows(i).Cells(1).Value = r.Item("kode_barang").ToString
            dgvTransaction.Rows(i).Cells(2).Value = r.Item("nama_barang").ToString
            dgvTransaction.Rows(i).Cells(3).Value = r.Item("stock_barang2").ToString
            dgvTransaction.Rows(i).Cells(4).Value = r.Item("stock_barang").ToString
            dgvTransaction.Rows(i).Cells(5).Value = r.Item("satuan").ToString
            dgvTransaction.Rows(i).Cells(6).Value = r.Item("harga_jual").ToString
            dgvTransaction.Rows(i).Cells(7).Value = r.Item("Diskon").ToString
            dgvTransaction.Rows(i).Cells(8).Value = r.Item("DiskonNilai").ToString
            dgvTransaction.Rows(i).Cells(9).Value = r.Item("Total").ToString

            i = i + 1
        Next
        conn.Close()
    End Sub

    'Private Sub getDataBarcode()
    '    koneksiDB()
    '    CMD = New SqlCommand("select * from master_barang where kode_barang ='" & txtBarcode.Text & "'", conn)
    '    DREADER = CMD.ExecuteReader()
    '    DREADER.Read()
    '    If DREADER.HasRows Then
    '        dgvTransaction.Rows.Add(1)
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(1).Value = DREADER.Item("kode_barang")
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(2).Value = DREADER.Item("nama_barang")
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(3).Value = DREADER.Item("stock_barang")
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(4).Value = 0
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(5).Value = DREADER.Item("satuan")
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(6).Value = DREADER.Item("harga_jual")
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(7).Value = 0
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(8).Value = 0
    '        dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(9).Value = dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(4).Value * dgvTransaction.Rows(dgvTransaction.RowCount - 2).Cells(6).Value

    '        QTY = DREADER.Item("stock_barang")
    '        dgvTransaction.Columns(4).ReadOnly = False
    '        dgvTransaction.Columns(7).ReadOnly = False
    '        dgvTransaction.Columns(8).ReadOnly = False

    '        'JUMLAH COUN
    '        Dim rowcount As Integer
    '        rowcount = dgvTransaction.Rows.Count()
    '        LabelX23.Text = rowcount - 1

    '        Hitung_total()
    '    End If
    '    DREADER.Close()
    'End Sub

    Private Sub btnBegin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBegin.Click
        getDataDetailMin()
        btnPrev.Enabled = False
        btnBegin.Enabled = False
        btnNext.Enabled = True
        btnLast.Enabled = True
        ButtonX4.Visible = False
        count = 1
        getDataHeader()
        Hitung_total()
    End Sub

    Private Sub btnPrev_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrev.Click
        ButtonX4.Visible = False
        count = count - 1
        If (count = 0) Then
            btnPrev.Enabled = False
            btnBegin.Enabled = False
            count = 1
        Else
            btnNext.Enabled = True
            btnLast.Enabled = True
        End If

        getDataHeader()
        getDataDetail()


        getDataDetailIsBegin()
        If dtDetail.Rows.Count < 1 Then
            count = Convert.ToInt16(Microsoft.VisualBasic.Right(LabelX14.Text, 5)) 'lblIdTransaksiHidden
            btnPrev.Enabled = False
            btnBegin.Enabled = False
        End If

        Hitung_total()
        getDataHeader()
    End Sub

    Private Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        ButtonX4.Visible = False
        count = count + 1
        If (count > 0) Then
            btnPrev.Enabled = True
            btnBegin.Enabled = True
        End If

        getDataDetail()
        getDataHeader()

        getDataDetailIsLast()
        If dtDetail.Rows.Count < 1 Then
            count = Convert.ToInt16(Microsoft.VisualBasic.Right(LabelX14.Text, 5)) 'lblIdTransaksiHidden
            btnNext.Enabled = False
            btnLast.Enabled = False
        End If

        Hitung_total()
    End Sub

    Private Sub btnLast_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLast.Click
        ButtonX4.Visible = False
        getDataDetailMax()
        btnPrev.Enabled = True
        btnBegin.Enabled = True
        btnNext.Enabled = False
        btnLast.Enabled = False
        count = Convert.ToInt16(Microsoft.VisualBasic.Right(lblIdTransaksiHidden.Text, 5)) - 1
        getDataHeader()
        Hitung_total()
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True
            ButtonX4.Visible = False
            getDataHeader2()
            getDataDetail2()
            Hitung_total()
        ElseIf e.Control AndAlso e.KeyCode = Keys.Right Then
            btnLast_Click(sender, e)
        ElseIf e.Control AndAlso e.KeyCode = Keys.Left Then
            btnBegin_Click(sender, e)
        ElseIf e.KeyCode = Keys.Right Then
            btnNext_Click(sender, e)
        ElseIf e.KeyCode = Keys.Left Then
            btnPrev_Click(sender, e)
        End If
    End Sub

    'Private Sub txtNamaCustomer_Leave(sender As Object, e As EventArgs) Handles txtNamaCustomer.Leave
    '    Dim str() As String
    '    Dim y As Boolean
    '    Dim s As String = txtNamaCustomer.Text
    '    If s = Nothing Then
    '    Else
    '        y = s.Contains("-")
    '    End If
    '    If y Then
    '        str = s.Split(CType("-", Char()))

    '        If cbNamaCustomer.Checked Then
    '            txtNamaCustomer.Text = str(0)
    '        Else
    '            txtNamaCustomer.Text = str(1)
    '        End If
    '    End If
    'End Sub

    'Sub tampil_comboboxmastercustomer2()
    '    koneksiDB()
    '    Dim mastercustomer1 As New SqlDataAdapter
    '    Dim dtBr As New DataTable
    '    Dim AutoComp As New AutoCompleteStringCollection()

    '    conn.Close()
    '    conn.Open()

    '    If cbNamaCustomer.Checked Then
    '        mastercustomer1 = New SqlDataAdapter("SELECT  nama_customer,alamat_customer FROM master_customer order by nama_customer ", conn)
    '    Else
    '        mastercustomer1 = New SqlDataAdapter("SELECT  nama_customer,alamat_customer FROM master_customer order by alamat_customer ", conn)
    '    End If

    '    Try
    '        mastercustomer1.Fill(dtBr)
    '        txtNamaCustomer.AutoCompleteCustomSource.Clear()
    '        For Each r In dtBr.Rows
    '            If cbNamaCustomer.Checked Then
    '                AutoComp.Add(r.Item("nama_customer").ToString + "-" + r.Item("alamat_customer").ToString)
    '            Else
    '                AutoComp.Add(r.Item("alamat_customer").ToString + "-" + r.Item("nama_customer").ToString)
    '            End If
    '        Next

    '        txtNamaCustomer.AutoCompleteMode = AutoCompleteMode.SuggestAppend
    '        txtNamaCustomer.AutoCompleteSource = AutoCompleteSource.CustomSource
    '        txtNamaCustomer.AutoCompleteCustomSource = AutoComp

    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message)
    '    End Try
    '    conn.Close()
    'End Sub   


    Private Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        'complex.Show(TryCast(sender, Button))
    End Sub


    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        Me.KeyPreview = True

        DateTimeInput1.Text = Today
        Kodeotomatis()
        'tampil_comboboxBarang()
        'tampil_comboboxmastercustomer2()
        'tampil_comboboxWarehouse()
        'tampil_comboboxSalesOrder()
        'aturDGV()
        ComboBoxEx1.Text = ""
        cbxCustomer.Text = ""
        txtNamaCustomer.Text = ""
        LabelX8.Text = ""
        LabelX15.Text = ""
        LabelX21.Text = ""
        ButtonX4.Visible = True
        'Hide_Maximize()

        'colors()

        dgvTransaction.Rows.Clear()
        LabelX14.Text = lblIdTransaksiHidden.Text
        txtNamaCustomer.Text = ""
        LabelX16.Text = 0
        TextBoxX4.Text = 0
        LabelX23.Text = ""

        txtRemark.Text = ""
        txtDisc1.Text = 0
        txtDiscNilai1.Text = 0
        txtDisc2.Text = 0
        txtDiscNilai2.Text = 0
        txtDiscBulat.Text = 0
        txtOngkir.Text = 0
        txtDP.Text = 0
        lblSubTotal.Text = ""
    End Sub

    Sub edit_transaksi()
        Try

            'save gride view detail
            koneksiDB()
            conn.Close()
            'Inser Data To Transaksi Penjualan
            Dim id_penjualan1, tanggal_penjualan, customer, total_harga, jumlah_barang As String
            Dim Diskon1, DiskonNilai1, Diskon2, DiskonNilai2, DiskonBulat, Ongkir, DP, Bayar, PPN10, Remark As String

            id_penjualan1 = "'" & Me.LabelX14.Text & "'"
            tanggal_penjualan = "'" & Format(DateTimeInput1.Value, "yyyy-MM-dd") & "'"
            customer = "'" & Me.txtNamaCustomer.Text & "'"
            total_harga = "'" & Me.LabelX16.Text & "'"
            jumlah_barang = "'" & Me.LabelX23.Text & "'"

            Remark = "'" & Me.txtRemark.Text & "'"
            Diskon1 = "'" & Me.txtDisc1.Text & "'"
            DiskonNilai1 = "'" & Me.txtDiscNilai1.Text & "'"
            Diskon2 = "'" & Me.txtDisc2.Text & "'"
            DiskonNilai2 = "'" & Me.txtDiscNilai2.Text & "'"
            DiskonBulat = "'" & Me.txtDiscBulat.Text & "'"
            Ongkir = "'" & Me.txtOngkir.Text & "'"
            DP = "'" & Me.txtDP.Text & "'"
            PPN10 = "'" & Me.txtPPN.Text & "'"
            Bayar = "'" & Kembalian.tbxNilaiBayar.Text & "'"

            Dim transaksi As New SqlCommand("UPDATE transaksi_penjualan SET tanggal_penjualan = " & tanggal_penjualan & "," &
                                            "customer = " & customer & "," &
                                            "total_harga = " & total_harga & "," &
                                            "jumlah_barang = " & jumlah_barang & "," &
                                            "Diskon1 = " & Diskon1 & "," &
                                            "DiskonNilai1 = " & DiskonNilai1 & "," &
                                            "Diskon2 = " & Diskon2 & "," &
                                            "DiskonNilai2 = " & DiskonNilai2 & "," &
                                            "DiskonBulat = " & DiskonBulat & "," &
                                            "Ongkir = " & Ongkir & "," &
                                            "DP = " & DP & "," &
                                            "Bayar = " & Bayar & "," &
                                            "PPN10 = " & PPN10 & "," &
                                            "Remark = " & Remark &
                                            " where id_penjualan = " & id_penjualan1, conn)
            conn.Open()
            transaksi.ExecuteNonQuery()
            conn.Close()

            For baris As Integer = 0 To dgvTransaction.Rows.Count - 2
                'Insert Data To Detail Penjualan
                Dim id_penjualan, kode_barang, qty, harga_satuan, Diskon, DiskonNilai, Total As String

                id_penjualan = "'" & Me.LabelX14.Text & "'"
                kode_barang = "'" & Me.dgvTransaction.Rows(baris).Cells(1).Value & "'"
                qty = "'" & Me.dgvTransaction.Rows(baris).Cells(4).Value & "'"
                harga_satuan = "'" & Me.dgvTransaction.Rows(baris).Cells(6).Value & "'"
                'Diskon = "'" & Me.dgvTransaction.Rows(baris).Cells(7).Value & "'"
                'DiskonNilai = "'" & Me.dgvTransaction.Rows(baris).Cells(8).Value & "'"
                Diskon = Convert.ToInt32(Diskon1) + Convert.ToInt32(Diskon2)
                DiskonNilai = Convert.ToInt32(DiskonNilai1) + Convert.ToInt32(DiskonNilai2)
                Total = "'" & Me.dgvTransaction.Rows(baris).Cells(9).Value & "'"

                Dim detail_penjualan As New SqlCommand("UPDATE detail_penjualan SET kode_barang = " & kode_barang & "," &
                                                       "qty = " & qty & "," &
                                                       "harga_satuan = " & harga_satuan & "," &
                                                       "Diskon = " & Diskon & "," &
                                                       "DiskonNilai = " & DiskonNilai & "," &
                                                       "Total = " & Total &
                                                       " where id_penjualan = " & id_penjualan & " and kode_barang = " & kode_barang, conn)

                conn.Open()
                detail_penjualan.ExecuteNonQuery()
                conn.Close()
            Next

            MsgBox(" Data Berhasil Disimpan ")
            ButtonX4.Visible = False

            tampilDGV_stock()
        Catch ex As Exception
            MsgBox(" Kemungkinan ID Sama ")
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        isEdit = True

        Dim xx As Integer = dgvTransaction.Rows.Count
        If xx < 2 Then
            MessageBox.Show("Data Penjualan belum di isi !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Dim bayar, harga, kembali As Integer

            harga = TextBoxX4.Text
            kembali = bayar - harga
            Kembalian.TextBoxX4.Text = kembali

            Kembalian.Show()
            tampil_comboboxJenisBayar()
            colors()
        End If
    End Sub

    Private Sub txtNamaCustomer_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNamaCustomer.KeyUp
        If e.KeyValue = 13 Then
            koneksiDB()
            CMD = New SqlCommand("Select * from master_customer where nama_customer = '" + txtNamaCustomer.Text + "' ", conn)
            DREADER = CMD.ExecuteReader
            DREADER.Read()
            'lblIdTransaksiHidden
            If Not DREADER.HasRows Then
                PopUpCustomer.Show()
            Else
                dgvTransaction.Select()
            End If
            Return
        End If

        If (txtNamaCustomer.SelectionLength = 0) Then
            If e.KeyValue = Keys.Up Then
                dgvTransaction.Select()
                Return
            End If


            If e.KeyValue = Keys.Tab Then
                dgvTransaction.Select()
                Return
            End If
        End If
    End Sub

    Private Sub DataGridViewX1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If dgvTransaction.CurrentCell.RowIndex = 0 Then

            If e.Alt AndAlso e.KeyValue = Keys.A Then
                dgvTransaction.EndEdit()
                dgvTransaction.CurrentCell = dgvTransaction.Rows(dgvTransaction.CurrentCell.RowIndex).Cells(2)

                txtNamaCustomer.Select()
            End If

            'If e.KeyValue = 13 Then
            '    koneksiDB()
            '    CMD = New SqlCommand("Select * from master_barang where kode_barang = '" + dgvTransaction.CurrentCell.Value + "' ", conn)
            '    DREADER = CMD.ExecuteReader
            '    DREADER.Read()
            '    'lblIdTransaksiHidden
            '    If Not DREADER.HasRows Then
            '        PopUpCustomer.Show()
            '    End If
            '    Return
            'End If

            'If e.KeyValue = Keys.B Then
            '    e.Handled = True
            'End If
        End If
    End Sub

    Private Sub txtNamaCustomer_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNamaCustomer.KeyDown
        'If e.KeyValue = Keys.E Then
        '    e.Handled = True
        'End If
        'last_txtcustomer = txtNamaCustomer.Text

        If e.KeyValue = Keys.Delete Or e.KeyValue = Keys.Back Then
            last_txtcustomer = txtNamaCustomer.Text
        End If
    End Sub

    Private Sub txtNamaCustomer_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNamaCustomer.KeyPress
        If e.KeyChar <> Convert.ToChar(Keys.Delete) And e.KeyChar <> Convert.ToChar(Keys.Back) Then
            koneksiDB()

            Dim customertext As String

            'If txtNamaCustomer.Text.Length >= (last_txtcustomer + e.KeyChar).Length Then
            '    customertext = txtNamaCustomer.Text
            'Else
            '    customertext = txtNamaCustomer.Text + e.KeyChar
            'End If

            Dim position As Integer = txtNamaCustomer.SelectionStart
            If (txtNamaCustomer.SelectionLength > 0) Then
                customertext = txtNamaCustomer.Text
                customertext = customertext.Remove(txtNamaCustomer.SelectionStart, txtNamaCustomer.SelectionLength)
                customertext = customertext.Insert(position, e.KeyChar.ToString)
            Else
                customertext = txtNamaCustomer.Text.Insert(position, e.KeyChar.ToString)
            End If


            CMD = New SqlCommand("Select * from master_customer where nama_customer LIKE '" + customertext + "%' ", conn)
            DREADER = CMD.ExecuteReader
            DREADER.Read()
            'lblIdTransaksiHidden
            If Not DREADER.HasRows Then
                e.Handled = True
                Return
            End If

            last_txtcustomer = customertext

            'last_txtcustomer = txtNamaCustomer.Text + e.KeyChar
        End If
    End Sub
End Class

