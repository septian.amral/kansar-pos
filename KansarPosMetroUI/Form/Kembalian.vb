Public Class Kembalian
    Dim f As Double
    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        If TransaksiPenjualan.isEdit Then
            TransaksiPenjualan.edit_transaksi()
        Else
            TransaksiPenjualan.save_transaksi()
        End If
        Me.Close()
    End Sub

    Private Sub Kembalian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TextBoxX4.BackColor = Color.Black
        TextBoxX4.ForeColor = Color.Red
        TransaksiPenjualan.colors()
    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        Me.Close()
    End Sub
    Private Sub TextBoxX4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxX4.TextChanged
        'If TextBoxX4.Text = "" Or Not IsNumeric(TextBoxX4.Text) Then
        '    Exit Sub
        'End If
        'f = TextBoxX4.Text
        'TextBoxX4.Text = Format(f, "#,###")
        'TextBoxX4.SelectionStart = Len(TextBoxX4.Text)
    End Sub

    Private Sub tbxNilaiBayar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbxNilaiBayar.TextChanged
        Dim bayar, harga, kembali As Integer

        If tbxNilaiBayar.Text = "" Or Not IsNumeric(tbxNilaiBayar.Text) Then
            Exit Sub
        End If
        f = tbxNilaiBayar.Text
        tbxNilaiBayar.Text = Format(f, "##,##0.00")

        If Len(TextBoxX4.Text) >= 3 Then
            tbxNilaiBayar.SelectionStart = Len(TextBoxX4.Text) - 2
        End If

        bayar = f
        harga = TransaksiPenjualan.TextBoxX4.Text
        kembali = bayar - harga
        TextBoxX4.Text = Format(kembali, "##,##0.00")
    End Sub


End Class
