Imports DevComponents.DotNetBar

Public Class Koneksi
    Sub Hide_Maximize()
        Dim sysItems As ArrayList = RibbonControl1.RibbonStrip.GetItems("", GetType(SystemCaptionItem))
        Dim item As SystemCaptionItem
        For Each item In sysItems
            If Not item.IsSystemIcon Then
                item.RestoreMaximizeVisible = False
            End If
        Next
    End Sub
    Private Sub Koneksi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDatabaseSetting()
        TextBoxX1.Text = dbServer
        TextBoxX2.Text = dbUser
        TextBoxX3.Text = dbPassword
        TextBoxX4.Text = dbName
        TextBoxX1.Enabled = False
        TextBoxX2.Enabled = False
        TextBoxX3.Enabled = False
        TextBoxX4.Enabled = False
        Hide_Maximize()
    End Sub


    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        Me.Close()
    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        If DatabaseConnected(TextBoxX1.Text, TextBoxX2.Text, _
                            TextBoxX3.Text, TextBoxX4.Text) = True Then
            With My.Settings
                .dbserver = TextBoxX1.Text
                .dbuser = TextBoxX2.Text
                .dbpassword = TextBoxX3.Text
                .dbname = TextBoxX4.Text
                .Save()
            End With
            GetDatabaseSetting()
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub ButtonItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem1.Click
        TextBoxX1.Enabled = True
        TextBoxX2.Enabled = True
        TextBoxX3.Enabled = True
        TextBoxX4.Enabled = True
    End Sub
End Class
