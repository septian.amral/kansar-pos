Imports DevComponents.DotNetBar
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class StockOpname
    Sub Hide_Maximize()
        Dim sysItems As ArrayList = RibbonControl1.RibbonStrip.GetItems("", GetType(SystemCaptionItem))
        Dim item As SystemCaptionItem
        For Each item In sysItems
            If Not item.IsSystemIcon Then
                item.RestoreMaximizeVisible = False
            End If
        Next
    End Sub
    Private Sub StockOpname_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Hide_Maximize()
        tampil_comboboxBarang()
        ComboBoxEx1.Text = ""
        TextBoxX1.Text = ""
        TextBoxX2.Text = ""
        TextBoxX3.Text = ""
        TextBoxX4.Text = ""
        TextBoxX5.Text = ""
        ComboBoxEx1.Focus()

    End Sub

    Private Sub ComboBoxEx1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxEx1.SelectedIndexChanged
        koneksiDB()
        conn.Close()
        Dim nama_barang As New SqlCommand("SELECT  nama_barang FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        Dim satuan As New SqlCommand("SELECT  satuan FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        Dim jumlah_stock As New SqlCommand("SELECT  stock_barang FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        conn.Open()

        TextBoxX1.Text = nama_barang.ExecuteScalar
        TextBoxX2.Text = satuan.ExecuteScalar
        TextBoxX3.Text = jumlah_stock.ExecuteScalar
        conn.Close()
        TextBoxX4.Focus()
    End Sub

    Private Sub TextBoxX4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxX4.TextChanged
        If TextBoxX4.Text = "" Then
            TextBoxX5.Text = ""
        Else
            Try

                Dim jumlahstock, jumlahaktual, selisih As String
                jumlahstock = TextBoxX3.Text
                jumlahaktual = TextBoxX4.Text
                selisih = jumlahaktual - jumlahstock
                TextBoxX5.Text = selisih

            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        'save
        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Save") = MsgBoxResult.Yes Then

            If TextBoxX4.Text = "" Or TextBoxX5.Text = "" Then
                MessageBox.Show("Data Tidak Boleh Kosong!", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)

            Else
                Try


                    koneksiDB()
                    conn.Close()

                    Dim kodebarang, namabarang, satuan, selisih_qty As String

                    kodebarang = "'" & Me.ComboBoxEx1.Text & "'"
                    namabarang = "'" & Me.TextBoxX1.Text & "'"
                    satuan = "'" & Me.TextBoxX2.Text & "'"
                    selisih_qty = "'" & Me.TextBoxX5.Text & "'"
                    Dim stockopname As New SqlCommand("insert into stock_opname (kode_barang, nama_barang, satuan, selisih_qty)values (" & kodebarang & "," & namabarang & "," & satuan & "," & selisih_qty & ")", conn)
                    conn.Open()
                    stockopname.ExecuteNonQuery()
                    conn.Close()
                    MessageBox.Show("Data Telah Di Simpan!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    
                    tampilDGV_stock()
                Catch ex As Exception
                    MessageBox.Show("error! Mungkin Kode Barang Sudah Ada, Silahkan Cek Kembali Data Anda", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                End Try
                TextBoxX1.Text = ""
                TextBoxX2.Text = ""
                TextBoxX3.Text = ""
                TextBoxX4.Text = ""
                TextBoxX5.Text = ""
                ComboBoxEx1.Text = ""
                ComboBoxEx1.Focus()

            End If
        ElseIf MsgBoxResult.No Then
        End If

    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        Me.Close()
    End Sub

    Private Sub ButtonItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem7.Click
        Me.Close()
    End Sub
End Class
