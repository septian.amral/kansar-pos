Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class PopUpCustomer

    Private Sub PopUpCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(91, Byte), Integer))
        koneksiDB()
        Dim mastercustomer1 As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        mastercustomer1 = New SqlDataAdapter("SELECT  nama_customer,alamat_customer FROM master_customer order by nama_customer ", conn)
        mastercustomer1.Fill(dtBr)
        gridPopUp.DataSource = dtBr
        gridPopUp.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        gridPopUp.Columns(0).ReadOnly = True
        gridPopUp.Columns(1).ReadOnly = True
        txtCustomer.Focus()

        TransaksiPenjualan.colors()
    End Sub

    Private Sub txtCustomer_TextChanged_1(sender As Object, e As EventArgs) Handles txtCustomer.TextChanged
        koneksiDB()
        Dim mastercustomer1 As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        mastercustomer1 = New SqlDataAdapter("SELECT  nama_customer,alamat_customer FROM master_customer where nama_customer like '" + txtCustomer.Text + "%' order by nama_customer ", conn)
        mastercustomer1.Fill(dtBr)
        gridPopUp.DataSource = dtBr
        gridPopUp.SelectionMode = DataGridViewSelectionMode.FullRowSelect
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        Me.Close()
    End Sub

    Private Sub gridPopUp_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs) Handles gridPopUp.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.Handled = True

            Try
                Dim Dgvr As DataGridViewRow = gridPopUp.SelectedRows(0)
                TransaksiPenjualan.txtNamaCustomer.Text = Dgvr.Cells("nama_customer").Value.ToString
                TransaksiPenjualan.txtNamaCustomer.Tag = Dgvr.Cells("alamat_customer").Value.ToString
                Me.Close()
            Catch ex As Exception
                Me.Close()
            End Try
        ElseIf e.KeyCode >= Keys.D0 And e.KeyCode <= Keys.Z Then
            txtCustomer.Select()
            txtCustomer.Text = ChrW(e.KeyValue)
            txtCustomer.SelectionStart = 1
        End If
    End Sub

    Private Sub txtCustomer_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCustomer.KeyDown
        If e.KeyCode = Keys.Enter Then
            gridPopUp.Select()
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        MasterCustomer.Show()
        Me.Close()
    End Sub
End Class
