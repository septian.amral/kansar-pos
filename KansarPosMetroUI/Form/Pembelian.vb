Imports System.Data
Imports System.Data.SqlClient
Imports DevComponents.DotNetBar
Imports System.Windows.Forms

Public Class Pembelian
    Dim DREADER As SqlDataReader
    Dim CMD As SqlCommand

    Sub Hide_Maximize()
        Dim sysItems As ArrayList = RibbonControl1.RibbonStrip.GetItems("", GetType(SystemCaptionItem))
        Dim item As SystemCaptionItem
        For Each item In sysItems
            If Not item.IsSystemIcon Then
                item.RestoreMaximizeVisible = False
            End If
        Next
    End Sub
    Sub aturDGV()
        Try

            DataGridViewX1.Columns.Add("kode_barang", "Kode Barang") '0
            DataGridViewX1.Columns.Add("nama_barang", "Nama Barang") '1
            DataGridViewX1.Columns.Add("qty", "Quantity") '2
            DataGridViewX1.Columns.Add("Satuan", "Satuan") '3

            DataGridViewX1.Columns(1).ReadOnly = True
            DataGridViewX1.Columns(2).ReadOnly = True
            DataGridViewX1.Columns(3).ReadOnly = True

        Catch ex As Exception
        End Try
    End Sub

    Sub save_pembelian()
        Try

            'save gride view detail
            If txtNamaSuplier.Text = "" Or TextBoxX3.Text = "" Then 'ComboBoxEx2
                MsgBox(" Silahkan Cek Kembali Data Anda ")
            Else
                koneksiDB()
                conn.Close()
                For baris As Integer = 0 To DataGridViewX1.Rows.Count - 2
                    'Insert Data To Detail Pembelian
                    Dim id_pembelian, kode_barang, qty As String

                    id_pembelian = "'" & Me.TextBoxX3.Text & "'"
                    kode_barang = "'" & Me.DataGridViewX1.Rows(baris).Cells(0).Value & "'"
                    qty = "'" & Me.DataGridViewX1.Rows(baris).Cells(2).Value & "'"



                    Dim detail_pembelian As New SqlCommand("insert into detail_pembelian (id_pembelian, kode_barang, qty)values (" & id_pembelian & "," & kode_barang & "," & qty & ")", conn)
                    conn.Open()
                    detail_pembelian.ExecuteNonQuery()
                    conn.Close()
                Next
                'Inser Data To Transaksi Penjualan
                Dim id_pembelian1, tanggal_pembelian, suplier, jumlah_barang As String

                id_pembelian1 = "'" & Me.TextBoxX3.Text & "'"
                tanggal_pembelian = "'" & Format(DateTimeInput1.Value, "yyyy-MM-dd") & "'"
                suplier = "'" & Me.txtNamaSuplier.Text & "'"
                jumlah_barang = "'" & Me.LabelX23.Text & "'"


                Dim transaksi As New SqlCommand("insert into transaksi_pembelian (id_pembelian, tanggal_pembelian, suplier, jumlah_barang)values (" & id_pembelian1 & "," & tanggal_pembelian & "," & suplier & "," & jumlah_barang & ")", conn)
                conn.Open()
                transaksi.ExecuteNonQuery()
                conn.Close()
                MsgBox(" Data Berhasil Disimpan ")
                tampilDGV_stock()
            End If
        Catch ex As Exception
            MsgBox(" Kemungkinan ID Sama ")
        End Try
    End Sub
    Private Sub Pembelian_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DateTimeInput1.Text = Today
        tampil_comboboxBarang()
        tampil_comboboxmastersuplier()
        ComboBoxEx1.Text = ""
        ComboBoxEx2.Text = ""
        txtNamaSuplier.Text = ""
        LabelX8.Text = ""
        LabelX15.Text = ""
        aturDGV()
        Hide_Maximize()
    End Sub

    Private Sub ComboBoxEx1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxEx1.SelectedIndexChanged
        koneksiDB()
        conn.Close()
        'Dim Kode_barang As New MySqlCommand("SELECT  kode_barang FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        Dim nama_barang As New SqlCommand("SELECT  nama_barang FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        Dim satuan As New SqlCommand("SELECT  satuan FROM master_barang where kode_barang = '" & ComboBoxEx1.Text & "'", conn)
        conn.Open()
        ' LabelX20.Text = Kode_barang.ExecuteScalar
        LabelX8.Text = nama_barang.ExecuteScalar
        LabelX15.Text = satuan.ExecuteScalar
        conn.Close()
    End Sub

    Private Sub ButtonX3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX3.Click
        'ADD Line
        If TextBoxX2.Text = "" Then
            MsgBox(" Quantity Kosong ")
        Else

            DataGridViewX1.Rows.Add(1)

            DataGridViewX1.Rows(DataGridViewX1.RowCount - 2).Cells(0).Value = ComboBoxEx1.Text 'Kode Barang
            DataGridViewX1.Rows(DataGridViewX1.RowCount - 2).Cells(1).Value = LabelX8.Text 'Nama Barang
            DataGridViewX1.Rows(DataGridViewX1.RowCount - 2).Cells(2).Value = TextBoxX2.Text 'Qty
            DataGridViewX1.Rows(DataGridViewX1.RowCount - 2).Cells(3).Value = LabelX15.Text 'Satuan

            'JUMLAH COUN
            Dim rowcount As Integer
            rowcount = DataGridViewX1.Rows.Count()
            LabelX23.Text = rowcount - 1


            'RETURN
            ComboBoxEx1.Text = ""
            LabelX8.Text = ""
            LabelX15.Text = ""
            TextBoxX2.Text = ""
            DataGridViewX1.Update()
            ComboBoxEx1.Focus()

        End If
    End Sub

    Private Sub ButtonX4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX4.Click
        save_pembelian()
        For Each iRow As DataGridViewRow In DataGridViewX1.SelectedRows
            DataGridViewX1.Rows.Clear()
        Next
        ComboBoxEx1.Text = ""
        LabelX8.Text = ""
        LabelX15.Text = ""
        ComboBoxEx2.Text = ""
        txtNamaSuplier.Text = ""
        TextBoxX3.Text = ""
        LabelX23.Text = ""

    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        Me.Close()
    End Sub

    Private Sub DataGridViewX1_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridViewX1.CellEndEdit
        If DataGridViewX1.CurrentCell.ColumnIndex = 0 Then 'e.ColumnIndex = 0
            koneksiDB()
            CMD = New SqlCommand("select * from master_barang where kode_barang ='" & DataGridViewX1.Rows(e.RowIndex).Cells(0).Value & "'", conn)
            DREADER = CMD.ExecuteReader()
            DREADER.Read()
            If DREADER.HasRows Then
                DataGridViewX1.Rows(e.RowIndex).Cells(1).Value = DREADER.Item("nama_barang")
                DataGridViewX1.Rows(e.RowIndex).Cells(2).Value = DREADER.Item("stock_barang")
                DataGridViewX1.Rows(e.RowIndex).Cells(3).Value = DREADER.Item("satuan")

                DataGridViewX1.Columns(2).ReadOnly = False

                'JUMLAH COUN
                Dim rowcount As Integer
                rowcount = DataGridViewX1.Rows.Count()
                LabelX23.Text = rowcount - 1
            End If
            DREADER.Close()
        End If
    End Sub

    Private Sub DataGridViewX1_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles DataGridViewX1.EditingControlShowing
        koneksiDB()
        Dim namaBr As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        namaBr = New SqlDataAdapter("SELECT kode_barang FROM master_barang ", conn)
        Try
            Dim tb As New TextBox
            namaBr.Fill(dtBr)
            Dim r As DataRow
            For Each r In dtBr.Rows
                If (DataGridViewX1.CurrentCell.ColumnIndex = 0) Then
                    tb = e.Control
                    tb.AutoCompleteCustomSource.Add(r.Item("kode_barang").ToString)
                    tb.AutoCompleteMode = AutoCompleteMode.Suggest
                    tb.AutoCompleteSource = AutoCompleteSource.CustomSource
                Else
                    tb = e.Control
                    tb.AutoCompleteMode = AutoCompleteMode.None
                End If
            Next

            'kolom QTY format angka
            If DataGridViewX1.CurrentCell.ColumnIndex = 2 Then
                AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBoxQTY_keyPress
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        conn.Close()
    End Sub

    Private Sub TextBoxQTY_keyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        'only numeric
        'If Char.IsDigit(CChar(CStr(e.KeyChar))) = False Then e.Handled = True

        'numerin and titik
        'If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or e.KeyChar = ".") Then e.Handled = True

        'numeric and backspace
        If DataGridViewX1.CurrentCell.ColumnIndex = 2 Then
            If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
                e.Handled = True
            End If
        End If
    End Sub

End Class
