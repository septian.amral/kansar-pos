Imports DevComponents.DotNetBar
Imports System.Data
Imports System.Data.SqlClient

Public Class ListPageStock
    Sub Hide_Maximize()
        Dim sysItems As ArrayList = RibbonControl1.RibbonStrip.GetItems("", GetType(SystemCaptionItem))
        Dim item As SystemCaptionItem
        For Each item In sysItems
            If Not item.IsSystemIcon Then
                item.RestoreMaximizeVisible = False
            End If
        Next
    End Sub
    Private Sub ListPageStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Hide_Maximize()
        tampilDGV_stock()
    End Sub

    Private Sub ButtonItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem7.Click
        Export_ListPageStock()
    End Sub

    Private Sub TextBoxX1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxX1.TextChanged
        If ComboBoxEx1.Text = "Kode Barang" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT kode_barang,Jenis_barang,nama_barang,satuan,harga_jual,stock_barang  from master_barang where kode_barang   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_barang")
                DataGridViewX1.DataSource = DS.Tables("master_barang")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Nama Barang" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT kode_barang,Jenis_barang,nama_barang,satuan,harga_jual,stock_barang  from master_barang where nama_barang   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_barang")
                DataGridViewX1.DataSource = DS.Tables("master_barang")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Satuan" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT kode_barang,Jenis_barang,nama_barang,satuan,harga_jual,stock_barang  from master_barang where satuan   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_barang")
                DataGridViewX1.DataSource = DS.Tables("master_barang")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Jenis Barang" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT kode_barang,Jenis_barang,nama_barang,satuan,harga_jual,stock_barang  from master_barang where jenis_barang   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_barang")
                DataGridViewX1.DataSource = DS.Tables("master_barang")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        End If
    End Sub
End Class
