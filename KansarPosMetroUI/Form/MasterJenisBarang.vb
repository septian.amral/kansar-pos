Imports DevComponents.DotNetBar

Public Class MasterJenisBarang
    Sub Hide_Maximize()
        Dim sysItems As ArrayList = RibbonControl1.RibbonStrip.GetItems("", GetType(SystemCaptionItem))
        Dim item As SystemCaptionItem
        For Each item In sysItems
            If Not item.IsSystemIcon Then
                item.RestoreMaximizeVisible = False
            End If
        Next
    End Sub
    Private Sub MasterJenisObat_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        remove_masterjenisbarang()
    End Sub
    Private Sub MasterJenisObat_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Hide_Maximize()
        tampil_ViewMasterJenisbarang()
    End Sub

    Private Sub ButtonItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem2.Click
        tampil_newMasterJenisbarang()
    End Sub

    Private Sub ButtonItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem3.Click
        delete_masterjenisbarang()
    End Sub

    Private Sub ButtonItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem7.Click
        Export_MasterJenisObat()
    End Sub
End Class
