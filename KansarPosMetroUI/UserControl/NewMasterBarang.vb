﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class NewMasterBarang

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        tampil_ViewMasterBarang()
    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        'save
        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Save") = MsgBoxResult.Yes Then

            If TextBoxX1.Text = "" Or TextBoxX2.Text = "" Or TextBoxX3.Text = "" Or TextBoxX4.Text = "" Or TextBoxX5.Text = "" Or ComboBoxEx1.Text = "" Then
                MessageBox.Show("Data Tidak Boleh Kosong!", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                'TextBoxX1.Text = ""
                'TextBoxX2.Text = ""
                'TextBoxX3.Text = ""
                'TextBoxX1.Focus()
            Else
                Try


                    koneksiDB()
                    conn.Close()

                    Dim kodebarang, namabarang, satuan, hargabeli, hargajual, jenis_barang, stock_barang As String

                    kodebarang = "'" & Me.TextBoxX1.Text & "'"
                    jenis_barang = "'" & Me.ComboBoxEx1.Text & "'"
                    namabarang = "'" & Me.TextBoxX2.Text & "'"
                    satuan = "'" & Me.TextBoxX3.Text & "'"
                    hargabeli = "'" & Me.TextBoxX4.Text & "'"
                    hargajual = "'" & Me.TextBoxX5.Text & "'"
                    stock_barang = "'" & Me.txtStock.Text & "'"

                    Dim masterbarang As New SqlCommand("insert into master_barang (kode_barang,jenis_barang, nama_barang, satuan,harga_beli,harga_jual,stock_barang)values (" & kodebarang & "," & jenis_barang & "," & namabarang & "," & satuan & "," & hargabeli & "," & hargajual & "," & stock_barang & ")", conn)
                    conn.Open()
                    masterbarang.ExecuteNonQuery()
                    conn.Close()
                    MessageBox.Show("Data Telah Di Simpan!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    tampilDGV_masterbarang()
                    tampil_comboboxBarang()
                    tampilDGV_stock()
                Catch ex As Exception
                    MessageBox.Show("error! Mungkin Kode Barang Sudah Ada, Silahkan Cek Kembali Data Anda", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                End Try
                TextBoxX1.Text = ""
                TextBoxX2.Text = ""
                TextBoxX3.Text = ""
                TextBoxX4.Text = ""
                TextBoxX5.Text = ""
                ComboBoxEx1.Text = ""
                TextBoxX1.Focus()

            End If
        ElseIf MsgBoxResult.No Then
        End If

    End Sub

    Private Sub NewMasterBarang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampil_comboboxJenisbarang()
        TextBoxX1.Text = ""
        TextBoxX2.Text = ""
        TextBoxX3.Text = ""
        TextBoxX4.Text = ""
        TextBoxX5.Text = ""
        ComboBoxEx1.Text = ""
        TextBoxX1.Focus()
    End Sub

    Private Sub TextBoxX4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxX4.KeyPress
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
            e.Handled = True
        End If
    End Sub

    Private Sub TextBoxX5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBoxX5.KeyPress
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtStock_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtStock.KeyPress
        If Not (Char.IsDigit(CChar(CStr(e.KeyChar))) Or Asc((e.KeyChar)) = "8") Then
            e.Handled = True
        End If
    End Sub
End Class
