﻿Imports System.Drawing
Imports System.Windows.Forms
Imports PopupControl
Imports System.Data.SqlClient

Partial Public Class ComplexPopup
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
        MinimumSize = Size
        MaximumSize = New Size(Size.Width * 2, Size.Height * 2)
        DoubleBuffered = True
        ResizeRedraw = True
    End Sub

    Protected Overrides Sub WndProc(ByRef m As Message)
        
        'If TryCast(Parent, Popup).ProcessResizing(m) Then
        '    Return
        'End If

        'MyBase.WndProc(m)
    End Sub

    Private Sub ComplexPopup_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tampil_masterbarang()
        tampil_masterbarang2()
    End Sub

    Sub tampil_masterbarang()
        koneksiDB()
        Dim masterbarang As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        masterbarang = New SqlDataAdapter("SELECT kode_barang,nama_barang FROM master_barang order by kode_barang,nama_barang", conn)
        Try
            masterbarang.Fill(dtBr)
            Dim r As DataRow
            txtKodeBarang.AutoCompleteCustomSource.Clear()
            For Each r In dtBr.Rows
                txtKodeBarang.AutoCompleteCustomSource.Add(r.Item(0).ToString + "-" + r.Item(1).ToString)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        conn.Close()
    End Sub

    Sub tampil_masterbarang2()
        koneksiDB()
        Dim masterbarang As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        masterbarang = New SqlDataAdapter("SELECT kode_barang,nama_barang FROM master_barang order by nama_barang,kode_barang", conn)
        Try
            masterbarang.Fill(dtBr)
            Dim r As DataRow
            txtNamaBarang.AutoCompleteCustomSource.Clear()
            For Each r In dtBr.Rows
                txtNamaBarang.AutoCompleteCustomSource.Add(r.Item(1).ToString + "-" + r.Item(0).ToString)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        conn.Close()
    End Sub

End Class

