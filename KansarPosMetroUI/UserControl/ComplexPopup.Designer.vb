﻿Imports System


Partial Public Class ComplexPopup
    Private components As ComponentModel.IContainer = Nothing

    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If

        MyBase.Dispose(disposing)
    End Sub

    Private Sub InitializeComponent()
        Me.label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtKodeBarang = New System.Windows.Forms.TextBox()
        Me.txtNamaBarang = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(9, 9)
        Me.label1.Margin = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(84, 14)
        Me.label1.TabIndex = 0
        Me.label1.Text = "Kode Barang :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 61)
        Me.Label2.Margin = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 14)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Nama Barang :"
        '
        'txtKodeBarang
        '
        Me.txtKodeBarang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtKodeBarang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtKodeBarang.BackColor = System.Drawing.Color.White
        Me.txtKodeBarang.ForeColor = System.Drawing.Color.Black
        Me.txtKodeBarang.Location = New System.Drawing.Point(12, 26)
        Me.txtKodeBarang.Name = "txtKodeBarang"
        Me.txtKodeBarang.Size = New System.Drawing.Size(305, 22)
        Me.txtKodeBarang.TabIndex = 87
        '
        'txtNamaBarang
        '
        Me.txtNamaBarang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtNamaBarang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtNamaBarang.BackColor = System.Drawing.Color.White
        Me.txtNamaBarang.ForeColor = System.Drawing.Color.Black
        Me.txtNamaBarang.Location = New System.Drawing.Point(12, 81)
        Me.txtNamaBarang.Name = "txtNamaBarang"
        Me.txtNamaBarang.Size = New System.Drawing.Size(305, 22)
        Me.txtNamaBarang.TabIndex = 88
        '
        'ComplexPopup
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me.txtNamaBarang)
        Me.Controls.Add(Me.txtKodeBarang)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.label1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "ComplexPopup"
        Me.Padding = New System.Windows.Forms.Padding(9)
        Me.Size = New System.Drawing.Size(329, 115)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private label1 As Windows.Forms.Label
    Private WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtKodeBarang As System.Windows.Forms.TextBox
    Friend WithEvents txtNamaBarang As System.Windows.Forms.TextBox
End Class

