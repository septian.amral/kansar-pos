﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ViewMasterUser

    Private Sub ViewMasterUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampilDGV_masteruser()
    End Sub

    Private Sub TextBoxX1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxX1.TextChanged
        If ComboBoxEx1.Text = "User Name" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from user where username   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "user")
                DataGridViewX1.DataSource = DS.Tables("user")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Alamat User" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from user where alamat   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "user")
                DataGridViewX1.DataSource = DS.Tables("user")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "No. Telephone" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from user where notlpn   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "user")
                DataGridViewX1.DataSource = DS.Tables("user")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        End If
    End Sub
End Class
