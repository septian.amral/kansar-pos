﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ViewMasterBarang

    Private Sub ViewMasterBarang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampilDGV_masterbarang()
    End Sub

    Private Sub TextBoxX1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxX1.TextChanged
        If ComboBoxEx1.Text = "Kode Barang" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT kode_barang,jenis_barang,nama_barang,satuan,harga_beli,harga_jual  from master_barang where kode_barang   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_barang")
                DataGridViewX1.DataSource = DS.Tables("master_barang")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Nama Barang" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT kode_barang,jenis_barang,nama_barang,satuan,harga_beli,harga_jual  from master_barang where nama_barang   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_barang")
                DataGridViewX1.DataSource = DS.Tables("master_barang")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Satuan" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT kode_barang,jenis_barang,nama_barang,satuan,harga_beli,harga_jual  from master_barang where satuan   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_barang")
                DataGridViewX1.DataSource = DS.Tables("master_barang")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Jenis Barang" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT kode_barang,jenis_barang,nama_barang,satuan,harga_beli,harga_jual  from master_barang where jenis_barang   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_barang")
                DataGridViewX1.DataSource = DS.Tables("master_barang")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        End If
    End Sub
End Class
