﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ViewMasterCustomer

    Private Sub ViewMasterPasien_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampilDGV_Customer()
    End Sub

    Private Sub TextBoxX1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxX1.TextChanged
        If ComboBoxEx1.Text = "Nama Customer" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from master_customer where nama_customer   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_customer")
                DataGridViewX1.DataSource = DS.Tables("master_customer")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Alamat Customer" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from master_customer where alamat_customer   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_customer")
                DataGridViewX1.DataSource = DS.Tables("master_customer")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "No. Telephone" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from master_customer where notlpn   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_customer")
                DataGridViewX1.DataSource = DS.Tables("master_customer")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try

        End If
    End Sub
End Class
