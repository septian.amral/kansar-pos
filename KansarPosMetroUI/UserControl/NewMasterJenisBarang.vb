﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data

Public Class NewMasterJenisBarang

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        tampil_ViewMasterJenisbarang()
    End Sub

    Private Sub NewMasterJenisObat_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        TextBoxX1.Focus()
    End Sub

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        'save
        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Save") = MsgBoxResult.Yes Then

            If TextBoxX1.Text = "" Or TextBoxX2.Text = "" Then
                MessageBox.Show("Data Tidak Boleh Kosong!", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                'TextBoxX1.Text = ""
                'TextBoxX2.Text = ""
                'TextBoxX3.Text = ""
                'TextBoxX1.Focus()
            Else
                Try


                    koneksiDB()
                    conn.Close()

                    Dim kodejenis, jenisbarang As String

                    kodejenis = "'" & Me.TextBoxX1.Text & "'"
                    jenisbarang = "'" & Me.TextBoxX2.Text & "'"

                    Dim masterjenisbarang As New SqlCommand("insert into master_jenis_barang (kode_jenis, jenis_barang)values (" & kodejenis & "," & jenisbarang & ")", conn)
                    conn.Open()
                    masterjenisbarang.ExecuteNonQuery()
                    conn.Close()
                    MessageBox.Show("Data Telah Di Simpan!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

                    tampilDGV_stock()
                Catch ex As Exception
                    MessageBox.Show("error! Mungkin Kode Barang Sudah Ada, Silahkan Cek Kembali Data Anda", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                End Try
                TextBoxX1.Text = ""
                TextBoxX2.Text = ""
                TextBoxX1.Focus()
                tampilDGV_JenisBarang()
                tampil_comboboxJenisbarang()
            End If
        ElseIf MsgBoxResult.No Then
        End If

    End Sub
End Class
