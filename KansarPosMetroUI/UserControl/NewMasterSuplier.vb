﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Data
Public Class NewMasterSuplier

    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX2.Click
        'save
        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Save") = MsgBoxResult.Yes Then

            If TextBoxX1.Text = "" Or TextBoxX2.Text = "" Or TextBoxX3.Text = "" Then
                MessageBox.Show("Data Tidak Boleh Kosong!", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                'TextBoxX1.Text = ""
                'TextBoxX2.Text = ""
                'TextBoxX3.Text = ""
                'TextBoxX1.Focus()
            Else
                Try


                    koneksiDB()
                    conn.Close()

                    Dim namasuplier, alamatsuplier, notlpn As String

                    namasuplier = "'" & Me.TextBoxX1.Text & "'"
                    alamatsuplier = "'" & Me.TextBoxX2.Text & "'"
                    notlpn = "'" & Me.TextBoxX3.Text & "'"

                    Dim mastersuplier As New SqlCommand("insert into master_suplier (nama_suplier, alamat_suplier,notlpn)values (" & namasuplier & "," & alamatsuplier & "," & notlpn & ")", conn)
                    conn.Open()
                    mastersuplier.ExecuteNonQuery()
                    conn.Close()
                    MessageBox.Show("Data Telah Di Simpan!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

                    tampilDGV_Suplier()
                Catch ex As Exception
                    MessageBox.Show("error! Mungkin Kode Barang Sudah Ada, Silahkan Cek Kembali Data Anda", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                End Try
                TextBoxX1.Text = ""
                TextBoxX2.Text = ""
                TextBoxX3.Text = ""
                TextBoxX1.Focus()
                tampil_comboboxmastersuplier()
            End If
        ElseIf MsgBoxResult.No Then
        End If
    End Sub

    Private Sub ButtonX1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonX1.Click
        tampil_ViewMasterSuplier()
    End Sub
End Class
