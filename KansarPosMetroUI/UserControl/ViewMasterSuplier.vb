﻿Imports System.Data
Imports System.Data.SqlClient

Public Class ViewMasterSuplier

    Private Sub ViewMasterSuplier_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tampilDGV_Suplier()
    End Sub

    Private Sub TextBoxX1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxX1.TextChanged
        If ComboBoxEx1.Text = "Nama Suplier" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from master_suplier where nama_suplier   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_suplier")
                DataGridViewX1.DataSource = DS.Tables("master_suplier")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "Alamat Suplier" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from master_suplier where alamat_suplier   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_suplier")
                DataGridViewX1.DataSource = DS.Tables("master_suplier")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try
        ElseIf ComboBoxEx1.Text = "No. Telephone" Then
            Try

                Dim DA As New SqlDataAdapter("SELECT * from master_suplier where notlpn   like ""%" + TextBoxX1.Text + "%""", conn)
                Dim DS As New DataSet
                DA.Fill(DS, "master_suplier")
                DataGridViewX1.DataSource = DS.Tables("master_suplier")
            Catch ex As Exception
                MsgBox("Silahkan cek kembali pencarian anda !!", MsgBoxStyle.Information, "Pemberitahuan!")
            End Try

        End If
    End Sub
End Class
