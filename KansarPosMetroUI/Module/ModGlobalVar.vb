﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Module ModGlobalVar
    Public dbServer As String
    Public dbUser As String
    Public dbPassword As String
    Public dbName As String
    Public sLocalConn As String
    Public Sub GetDatabaseSetting()
        dbServer = My.Settings.dbserver
        dbUser = My.Settings.dbuser
        dbPassword = My.Settings.dbpassword
        dbName = My.Settings.dbname
        sLocalConn = "server=" & dbServer & ";user id=" & dbUser & ";" & _
                     "password=" & dbPassword & ";database=" & dbName
    End Sub
    Public Function DatabaseConnected(Optional ByVal Server As String = "", _
            Optional ByVal User As String = "", _
            Optional ByVal Password As String = "", _
            Optional ByVal DatabaseName As String = "") As Boolean
        Dim conn As SqlConnection
        conn = New SqlConnection()
        If Server = "" And User = "" And Password = "" And DatabaseName = "" Then
            conn.ConnectionString = sLocalConn
        Else
            conn.ConnectionString = "Data Source=" & Server & ";User Id=" & User & ";" & "password=" & Password & ";Initial Catalog=" & DatabaseName
        End If
        Try
            conn.Open()

            Dim cmd As SqlCommand = New SqlCommand("use " + DatabaseName, conn)
            cmd.ExecuteNonQuery()

            conn.Close()
            Return True
        Catch myerror As SqlException
            Return False
            MessageBox.Show("Error: " & myerror.Message)
        Finally
            conn.Dispose()
        End Try
        Return False
    End Function
End Module
