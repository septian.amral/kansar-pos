﻿Imports System.Data
Imports System.Windows.Forms
Imports Microsoft.Office.Interop
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Data.SqlClient

Module ModuleTampil
    Dim viewmasterbarang As New ViewMasterBarang
    Dim newmasterbarang As New NewMasterBarang
    Dim viewmasteruser As New ViewMasterUser
    Dim newmasteruser As New NewMasterUser
    Dim viewmasterjenisbarang As New ViewMasterJenisBarang
    Dim newmasterjenisbarang As New NewMasterJenisBarang
    Dim viewmastersuplier As New ViewMasterSuplier
    Dim newmastersuplier As New NewMasterSuplier
    Dim viewmastercustomer As New ViewMasterCustomer
    Dim newmastercustomer As New NewMasterCustomer

    '====================================================
    'Remove Master Barang
    '====================================================
    Sub remove_masterbarang()
        MasterBarang.Controls.Remove(viewmasterbarang)
        MasterBarang.Controls.Remove(newmasterbarang)
    End Sub
    '====================================================
    'Remove Master User
    '====================================================
    Sub remove_masteruser()
        User.Controls.Remove(viewmasteruser)
        User.Controls.Remove(newmasteruser)
    End Sub
    '====================================================
    'Remove Master Jenis barang
    '====================================================
    Sub remove_masterjenisbarang()
        MasterJenisBarang.Controls.Remove(viewmasterjenisbarang)
        MasterJenisBarang.Controls.Remove(newmasterjenisbarang)
    End Sub
    '====================================================
    'Remove Master Suplier
    '====================================================
    Sub remove_mastersuplier()
        MasterSuplier.Controls.Remove(viewmastersuplier)
        MasterSuplier.Controls.Remove(newmastersuplier)
    End Sub
    '====================================================
    'Remove Master customer
    '====================================================
    Sub remove_mastercustomer()
        MasterCustomer.Controls.Remove(viewmastercustomer)
        MasterCustomer.Controls.Remove(newmastercustomer)
    End Sub
    '====================================================
    'View Master Barang
    '====================================================
    Sub tampil_ViewMasterBarang()
        remove_masterbarang()
        MasterBarang.Controls.Add(viewmasterbarang)
        viewmasterbarang.Location = New Point(2, 70)
    End Sub
    '====================================================
    'View Master Barang
    '====================================================
    Sub tampil_newMasterBarang()
        remove_masterbarang()
        MasterBarang.Controls.Add(newmasterbarang)
        newmasterbarang.Location = New Point(2, 70)
    End Sub
    '====================================================
    'View Master User
    '====================================================
    Sub tampil_ViewMasterUser()
        remove_masteruser()
        User.Controls.Add(viewmasteruser)
        viewmasteruser.Location = New Point(2, 70)
    End Sub
    '====================================================
    'new Master User
    '====================================================
    Sub tampil_newMasterUser()
        remove_masteruser()
        User.Controls.Add(newmasteruser)
        newmasteruser.Location = New Point(2, 70)
    End Sub
    '====================================================
    'View Master Suplier
    '====================================================
    Sub tampil_ViewMasterSuplier()
        remove_mastersuplier()
        MasterSuplier.Controls.Add(viewmastersuplier)
        viewmastersuplier.Location = New Point(2, 70)
    End Sub
    '====================================================
    'new Master Suplier
    '====================================================
    Sub tampil_newMasterSuplier()
        remove_mastersuplier()
        MasterSuplier.Controls.Add(newmastersuplier)
        newmastersuplier.Location = New Point(2, 70)
    End Sub
    '====================================================
    'View Master Customer
    '====================================================
    Sub tampil_ViewMasterCustomer()
        remove_mastercustomer()
        MasterCustomer.Controls.Add(viewmastercustomer)
        viewmastercustomer.Location = New Point(2, 70)
    End Sub
    '====================================================
    'new Master Cstomer
    '====================================================
    Sub tampil_newMasterCustomer()
        remove_mastercustomer()
        MasterCustomer.Controls.Add(newmastercustomer)
        newmastercustomer.Location = New Point(2, 70)
    End Sub
    '====================================================
    'View Master Jenis barang
    '====================================================
    Sub tampil_ViewMasterJenisbarang()
        remove_masterjenisbarang()
        MasterJenisBarang.Controls.Add(viewmasterjenisbarang)
        viewmasterjenisbarang.Location = New Point(2, 70)
    End Sub
    '====================================================
    'new Master Jenis barang
    '====================================================
    Sub tampil_newMasterJenisbarang()
        remove_masterjenisbarang()
        MasterJenisBarang.Controls.Add(newmasterjenisbarang)
        newmasterjenisbarang.Location = New Point(2, 70)
    End Sub
    '====================================================
    'Tampil DGV Master Barang
    '====================================================
    Sub tampilDGV_masterbarang()
        Try
            koneksiDB()
            Dim masterbarang As New SqlDataAdapter
            Dim dtData As New Data.DataTable
            conn.Close()
            'tampil tabel barang
            conn.Open()
            masterbarang = New SqlDataAdapter("SELECT kode_barang,jenis_barang,nama_barang,satuan,harga_beli,harga_jual FROM master_barang ORDER BY nama_barang ASC ", conn)
            masterbarang.Fill(dtData)
            viewmasterbarang.DataGridViewX1.DataSource = dtData
            conn.Close()
            aturDGV_viewmasterbarang()
        Catch ex As Exception
            MsgBox("Erorr == tampilDGV_MainAccount ==")
        End Try
    End Sub

    '====================================================
    'Tampil DGV Master user
    '====================================================
    Sub tampilDGV_masteruser()
        Try
            koneksiDB()
            Dim masteruser As New SqlDataAdapter
            Dim dtData As New Data.DataTable
            conn.Close()
            'tampil tabel barang
            conn.Open()
            masteruser = New SqlDataAdapter("SELECT * FROM user ORDER BY username ASC ", conn)
            masteruser.Fill(dtData)
            viewmasteruser.DataGridViewX1.DataSource = dtData
            conn.Close()
            aturDGV_viewmasteruser()
        Catch ex As Exception
            MsgBox("Erorr == tampilDGV_MainAccount ==")
        End Try
    End Sub
    '====================================================
    'Tampil DGV Stock
    '====================================================
    Sub tampilDGV_stock()
        Try
            koneksiDB()
            Dim stock As New SqlDataAdapter
            Dim dtData As New Data.DataTable
            conn.Close()
            'tampil tabel barang
            conn.Open()
            stock = New SqlDataAdapter("SELECT kode_barang,Jenis_barang,nama_barang,satuan,harga_jual,stock_barang FROM master_barang ORDER BY nama_barang ASC ", conn)
            stock.Fill(dtData)
            ListPageStock.DataGridViewX1.DataSource = dtData
            conn.Close()
            aturDGV_stock()
        Catch ex As Exception
            MsgBox("Erorr == tampilDGV_MainAccount ==")
        End Try
    End Sub
    '====================================================
    'Tampil DGV Pasien
    '====================================================
    Sub tampilDGV_Customer()
        Try
            koneksiDB()
            Dim pasien As New SqlDataAdapter
            Dim dtData As New Data.DataTable
            conn.Close()
            'tampil tabel barang
            conn.Open()
            pasien = New SqlDataAdapter("SELECT * FROM master_customer ORDER BY nama_customer ASC ", conn)
            pasien.Fill(dtData)
            viewmastercustomer.DataGridViewX1.DataSource = dtData
            conn.Close()
            aturDGV_viewmastercustomer()
        Catch ex As Exception
            MsgBox("Erorr == tampilDGV_MainAccount ==")
        End Try
    End Sub
    '====================================================
    'Tampil DGV Suplier
    '====================================================
    Sub tampilDGV_Suplier()
        Try
            koneksiDB()
            Dim suplier As New SqlDataAdapter
            Dim dtData As New Data.DataTable
            conn.Close()
            'tampil tabel barang
            conn.Open()
            suplier = New SqlDataAdapter("SELECT * FROM master_suplier ORDER BY nama_suplier ASC ", conn)
            suplier.Fill(dtData)
            viewmastersuplier.DataGridViewX1.DataSource = dtData
            conn.Close()
            aturDGV_viewmasterSuplier()
        Catch ex As Exception
            MsgBox("Erorr == tampilDGV_MainAccount ==")
        End Try
    End Sub
    '====================================================
    'Tampil DGV Jenis Obat
    '====================================================
    Sub tampilDGV_JenisBarang()
        Try
            koneksiDB()
            Dim jenisbarang As New SqlDataAdapter
            Dim dtData As New Data.DataTable
            conn.Close()
            'tampil tabel barang
            conn.Open()
            jenisbarang = New SqlDataAdapter("SELECT * FROM master_jenis_barang ORDER BY jenis_barang ASC ", conn)
            jenisbarang.Fill(dtData)
            viewmasterjenisbarang.DataGridViewX1.DataSource = dtData
            conn.Close()
            aturDGV_viewmasterjenisbarang()
        Catch ex As Exception
            MsgBox("Erorr == tampilDGV_MainAccount ==")
        End Try
    End Sub
    '====================================================
    'atur DGV View Master Barang
    '====================================================
    Sub aturDGV_viewmasterbarang()
        Try

            viewmasterbarang.DataGridViewX1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter
            viewmasterbarang.DataGridViewX1.Columns(0).HeaderText = "Kode Barang"
            viewmasterbarang.DataGridViewX1.Columns(1).HeaderText = "Jenis Barang"
            viewmasterbarang.DataGridViewX1.Columns(2).HeaderText = "Nama Barang"
            viewmasterbarang.DataGridViewX1.Columns(3).HeaderText = "Satuan"
            viewmasterbarang.DataGridViewX1.Columns(4).HeaderText = "Harga Beli"
            viewmasterbarang.DataGridViewX1.Columns(5).HeaderText = "Harga Jual"
        Catch ex As Exception

        End Try
    End Sub
    '====================================================
    'atur DGV View Master User
    '====================================================
    Sub aturDGV_viewmasteruser()
        Try

            viewmasteruser.DataGridViewX1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter
            viewmasteruser.DataGridViewX1.Columns(0).HeaderText = "User Name"
            viewmasteruser.DataGridViewX1.Columns(1).HeaderText = "Password"
            viewmasteruser.DataGridViewX1.Columns(2).HeaderText = "Alamat"
            viewmasteruser.DataGridViewX1.Columns(3).HeaderText = "No. Telephone"
        Catch ex As Exception

        End Try
    End Sub
    '====================================================
    'atur DGV View Master Jenis Obat
    '====================================================
    Sub aturDGV_viewmasterjenisbarang()
        Try

            viewmasterjenisbarang.DataGridViewX1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter
            viewmasterjenisbarang.DataGridViewX1.Columns(0).HeaderText = "Kode Jenis"
            viewmasterjenisbarang.DataGridViewX1.Columns(1).HeaderText = "Jenis Barang"

            viewmasterjenisbarang.DataGridViewX1.Columns(0).Width = 100

        Catch ex As Exception

        End Try
    End Sub
    '====================================================
    'atur DGV View Master Pasien
    '====================================================
    Sub aturDGV_viewmastercustomer()
        Try

            viewmastercustomer.DataGridViewX1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter
            viewmastercustomer.DataGridViewX1.Columns(0).HeaderText = "ID"
            viewmastercustomer.DataGridViewX1.Columns(1).HeaderText = "Nama Customer"
            viewmastercustomer.DataGridViewX1.Columns(2).HeaderText = "Alamat Customer"
            viewmastercustomer.DataGridViewX1.Columns(3).HeaderText = "No. Telephone"

            viewmastercustomer.DataGridViewX1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            viewmastercustomer.DataGridViewX1.Columns(0).Width = 75

        Catch ex As Exception

        End Try
    End Sub
    '====================================================
    'atur DGV View Master Suplier
    '====================================================
    Sub aturDGV_viewmasterSuplier()
        Try

            viewmastersuplier.DataGridViewX1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter
            viewmastersuplier.DataGridViewX1.Columns(0).HeaderText = "ID"
            viewmastersuplier.DataGridViewX1.Columns(1).HeaderText = "Nama Suplier"
            viewmastersuplier.DataGridViewX1.Columns(2).HeaderText = "Alamat Suplier"
            viewmastersuplier.DataGridViewX1.Columns(3).HeaderText = "No. Telephone"

            viewmastersuplier.DataGridViewX1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            viewmastersuplier.DataGridViewX1.Columns(0).Width = 75
        Catch ex As Exception

        End Try
    End Sub
    '====================================================
    'atur DGV stock
    '====================================================
    Sub aturDGV_stock()
        Try
            ListPageStock.DataGridViewX1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter
            ListPageStock.DataGridViewX1.Columns(0).HeaderText = "Kode Barang"
            ListPageStock.DataGridViewX1.Columns(1).HeaderText = "Jenis Barang"
            ListPageStock.DataGridViewX1.Columns(2).HeaderText = "Nama Barang"
            ListPageStock.DataGridViewX1.Columns(3).HeaderText = "Satuan"
            ListPageStock.DataGridViewX1.Columns(4).HeaderText = "Harga Jual"
            ListPageStock.DataGridViewX1.Columns(5).HeaderText = "Jumlah Stock"

            ListPageStock.DataGridViewX1.Columns(0).Width = 100
            ListPageStock.DataGridViewX1.Columns(1).Width = 75
            ListPageStock.DataGridViewX1.Columns(2).Width = 300
            'ListPageStock.DataGridViewX1.Columns(3).Width = 150

        Catch ex As Exception

        End Try
    End Sub
    '====================================================
    'Delete Master Barang
    '====================================================
    Sub delete_masterbarang()

        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Delete") = MsgBoxResult.Yes Then
            Try
                koneksiDB()
                conn.Close()
                Dim delete As New SqlCommand("DELETE FROM master_barang where kode_barang = '" & viewmasterbarang.DataGridViewX1.SelectedCells(0).Value & "'", conn)
                conn.Open()
                delete.ExecuteNonQuery()
                conn.Close()
                MessageBox.Show("Data Berhasil Di Hapus!", "warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                tampilDGV_masterbarang()
            Catch ex As Exception
                MessageBox.Show("Error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        ElseIf MsgBoxResult.No Then
        End If

    End Sub
    '====================================================
    'Delete Master Customer
    '====================================================
    Sub delete_mastercustomer()

        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Delete") = MsgBoxResult.Yes Then
            Try
                koneksiDB()
                conn.Close()
                Dim delete As New SqlCommand("DELETE FROM master_customer where id = '" & viewmastercustomer.DataGridViewX1.SelectedCells(0).Value & "'", conn)
                conn.Open()
                delete.ExecuteNonQuery()
                conn.Close()
                MessageBox.Show("Data Berhasil Di Hapus!", "warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                tampilDGV_Customer()
            Catch ex As Exception
                MessageBox.Show("Error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        ElseIf MsgBoxResult.No Then
        End If

    End Sub
    '====================================================
    'Delete Master Suplier
    '====================================================
    Sub delete_mastersuplier()

        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Delete") = MsgBoxResult.Yes Then
            Try
                koneksiDB()
                conn.Close()
                Dim delete As New SqlCommand("DELETE FROM master_suplier where id = '" & viewmastersuplier.DataGridViewX1.SelectedCells(0).Value & "'", conn)
                conn.Open()
                delete.ExecuteNonQuery()
                conn.Close()
                MessageBox.Show("Data Berhasil Di Hapus!", "warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                tampilDGV_Suplier()
            Catch ex As Exception
                MessageBox.Show("Error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        ElseIf MsgBoxResult.No Then
        End If

    End Sub
    '====================================================
    'Delete Master Jenis Obat
    '====================================================
    Sub delete_masterjenisbarang()

        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Delete") = MsgBoxResult.Yes Then
            Try
                koneksiDB()
                conn.Close()
                Dim delete As New SqlCommand("DELETE FROM master_jenis_barang where kode_jenis = '" & viewmasterjenisbarang.DataGridViewX1.SelectedCells(0).Value & "'", conn)
                conn.Open()
                delete.ExecuteNonQuery()
                conn.Close()
                MessageBox.Show("Data Berhasil Di Hapus!", "warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                tampilDGV_JenisBarang()
            Catch ex As Exception
                MessageBox.Show("Error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        ElseIf MsgBoxResult.No Then
        End If

    End Sub
    '====================================================
    'Delete Master user
    '====================================================
    Sub delete_masteruser()

        If MsgBox("Are you sure ?", MsgBoxStyle.YesNo, "Delete") = MsgBoxResult.Yes Then
            Try
                koneksiDB()
                conn.Close()
                Dim delete As New SqlCommand("DELETE FROM user where username = '" & viewmasteruser.DataGridViewX1.SelectedCells(0).Value & "'", conn)
                conn.Open()
                delete.ExecuteNonQuery()
                conn.Close()
                MessageBox.Show("Data Berhasil Di Hapus!", "warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                tampilDGV_masteruser()
            Catch ex As Exception
                MessageBox.Show("Error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        ElseIf MsgBoxResult.No Then
        End If

    End Sub
    '====================================================
    'TAmpil Combobox Barang
    '====================================================
    Sub tampil_comboboxBarang()
        koneksiDB()
        Dim namaBr As New SqlDataAdapter
        Dim dtBr As New DataTable

        conn.Close()
        'tampil combo box konsumen
        conn.Open()
        namaBr = New SqlDataAdapter("SELECT  kode_barang FROM master_barang ", conn)

        Try
            namaBr.Fill(dtBr)
            TransaksiPenjualan.ComboBoxEx1.DataSource = dtBr
            TransaksiPenjualan.ComboBoxEx1.DisplayMember = "kode_barang"

            Pembelian.ComboBoxEx1.DataSource = dtBr
            Pembelian.ComboBoxEx1.DisplayMember = "kode_barang"

            StockOpname.ComboBoxEx1.DataSource = dtBr
            StockOpname.ComboBoxEx1.DisplayMember = "kode_barang"

            TransaksiPenjualan.ComboBoxEx1.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            TransaksiPenjualan.ComboBoxEx1.AutoCompleteSource = AutoCompleteSource.ListItems

            Pembelian.ComboBoxEx1.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Pembelian.ComboBoxEx1.AutoCompleteSource = AutoCompleteSource.ListItems

            StockOpname.ComboBoxEx1.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            StockOpname.ComboBoxEx1.AutoCompleteSource = AutoCompleteSource.ListItems
          
        Catch ex As Exception
            ' tampilkan pesan error
            MessageBox.Show(ex.Message)
        End Try

        conn.Close()
    End Sub
    '====================================================
    'TAmpil Combobox Jenis barang
    '====================================================
    Sub tampil_comboboxJenisbarang()
        koneksiDB()
        Dim jenisBRG As New SqlDataAdapter
        Dim dtBr As New DataTable

        conn.Close()
        'tampil combo box konsumen
        conn.Open()
        jenisBRG = New SqlDataAdapter("SELECT  jenis_barang FROM master_jenis_barang ", conn)

        Try
            ' ambil data dari database
            jenisBRG.Fill(dtBr)
            ' bind data ke combobox
            newmasterbarang.ComboBoxEx1.DataSource = dtBr
            newmasterbarang.ComboBoxEx1.DisplayMember = "jenis_barang"
            ' AUTO TEXT 
            newmasterbarang.ComboBoxEx1.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            newmasterbarang.ComboBoxEx1.AutoCompleteSource = AutoCompleteSource.ListItems

            ' DONE!!!
        Catch ex As Exception
            ' tampilkan pesan error
            MessageBox.Show(ex.Message)
        End Try

        conn.Close()
    End Sub

    Sub tampil_comboboxWarehouse()
        koneksiDB()
        Dim mastercustomer1 As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        mastercustomer1 = New SqlDataAdapter("SELECT 'WAREHOUSE' as WH ", conn)
        Try
            mastercustomer1.Fill(dtBr)
            TransaksiPenjualan.ddlWarehouse.DataSource = dtBr
            TransaksiPenjualan.ddlWarehouse.DisplayMember = "WH"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        conn.Close()
    End Sub

    Sub tampil_comboboxJenisBayar()
        koneksiDB()
        Dim masterJenisBayar As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        masterJenisBayar = New SqlDataAdapter("SELECT * FROM jenis_bayar ", conn)
        Try
            masterJenisBayar.Fill(dtBr)
            Kembalian.cbxJenisBayar.DataSource = dtBr
            Kembalian.cbxJenisBayar.DisplayMember = "Name"
            Kembalian.cbxJenisBayar.ValueMember = "ID"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        conn.Close()
    End Sub

    Sub tampil_comboboxSalesOrder()
        koneksiDB()
        Dim mastercustomer1 As New SqlDataAdapter
        Dim dtBr As New DataTable
        conn.Close()
        conn.Open()
        mastercustomer1 = New SqlDataAdapter("SELECT 'SALES ORDER' as Sales ", conn)
        Try
            mastercustomer1.Fill(dtBr)
            TransaksiPenjualan.ddlSalesOrder.DataSource = dtBr
            TransaksiPenjualan.ddlSalesOrder.DisplayMember = "Sales"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        conn.Close()
    End Sub

    '====================================================
    'TAmpil Combobox Master Pasien
    '====================================================
    Sub tampil_comboboxmastercustomer()
        koneksiDB()
        Dim mastercustomer1 As New SqlDataAdapter
        Dim dtBr As New DataTable
        Dim AutoComp As New AutoCompleteStringCollection()

        conn.Close()
        conn.Open()

   mastercustomer1 = New SqlDataAdapter("SELECT  nama_customer,alamat_customer FROM master_customer order by nama_customer ", conn)

        Try
            mastercustomer1.Fill(dtBr)
            'ComboBoxEx2.DataSource = dtBr
            'ComboBoxEx2.DisplayMember = "nama_customer"
            'ComboBoxEx2.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            'ComboBoxEx2.AutoCompleteSource = AutoCompleteSource.ListItems
            Dim r As DataRow
            TransaksiPenjualan.txtNamaCustomer.AutoCompleteCustomSource.Clear()
            For Each r In dtBr.Rows
                 'AutoComp.Add(r.Item("nama_customer").ToString + "-" + r.Item("alamat_customer").ToString)
                AutoComp.Add(r.Item("nama_customer").ToString)
            Next

            TransaksiPenjualan.txtNamaCustomer.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            TransaksiPenjualan.txtNamaCustomer.AutoCompleteSource = AutoCompleteSource.CustomSource
            TransaksiPenjualan.txtNamaCustomer.AutoCompleteCustomSource = AutoComp

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        conn.Close()
    End Sub

    '====================================================
    'Tampil Combobox Master splier
    '====================================================
    Sub tampil_comboboxmastersuplier()
        koneksiDB()
        Dim mastersuplier As New SqlDataAdapter
        Dim dtBr As New DataTable

        conn.Close()
        conn.Open()
        mastersuplier = New SqlDataAdapter("SELECT  nama_suplier FROM master_suplier ", conn)

        Try
            mastersuplier.Fill(dtBr)
            Pembelian.ComboBoxEx2.DataSource = dtBr
            Pembelian.ComboBoxEx2.DisplayMember = "nama_suplier"
            Pembelian.ComboBoxEx2.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            Pembelian.ComboBoxEx2.AutoCompleteSource = AutoCompleteSource.ListItems

            Dim r As DataRow
            Pembelian.txtNamaSuplier.AutoCompleteCustomSource.Clear()
            For Each r In dtBr.Rows
                Pembelian.txtNamaSuplier.AutoCompleteCustomSource.Add(r.Item(0).ToString)
            Next
        Catch ex As Exception
            ' tampilkan pesan error
            MessageBox.Show(ex.Message)
        End Try

        conn.Close()
    End Sub
    '##################################################################################################################
    '==================================================================================================================
    '                                              EXPORT TO EXCEL
    '==================================================================================================================
    '##################################################################################################################

    '=================================================
    'Export To Excel Master Barang
    '=================================================
    Sub Export_MasterBarang()
        Dim rowsTotal, colsTotal As Short
        Dim I, j, iC As Short
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Dim xlApp As New Excel.Application

        Try
            Dim excelBook As Excel.Workbook = xlApp.Workbooks.Add
            Dim excelWorksheet As Excel.Worksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)
            xlApp.Visible = True
            rowsTotal = viewmasterbarang.DataGridViewX1.RowCount - 1
            colsTotal = viewmasterbarang.DataGridViewX1.Columns.Count - 1

            With (excelWorksheet)
                .Cells.Select()
                .Cells.Delete()
                For iC = 0 To colsTotal
                    .Cells(1, iC + 1).Value = viewmasterbarang.DataGridViewX1.Columns(iC).HeaderText
                Next

                For I = 0 To rowsTotal
                    For j = 0 To colsTotal
                        .Cells(I + 2, j + 1).value = viewmasterbarang.DataGridViewX1.Rows(I).Cells(j).Value
                    Next (j)
                Next (I)

                .Rows("1:1").Font.FontStyle = "Bold"
                .Rows("1:1").Font.Size = 10
                .Cells.Columns.AutoFit()
                .Cells.Select()
                .Cells.EntireColumn.AutoFit()
                .Cells(1, 1).Select()
            End With

        Catch ex As Exception
            MsgBox("Export Excel Error " & ex.Message)
        Finally
            'RELEASE ALLOACTED RESOURCES 
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            xlApp = Nothing

        End Try
    End Sub


    '=================================================
    'Export To Excel Master Suplier
    '=================================================
    Sub Export_MasterSuplier()
        Dim rowsTotal, colsTotal As Short
        Dim I, j, iC As Short
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Dim xlApp As New Excel.Application

        Try
            Dim excelBook As Excel.Workbook = xlApp.Workbooks.Add
            Dim excelWorksheet As Excel.Worksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)
            xlApp.Visible = True
            rowsTotal = viewmastersuplier.DataGridViewX1.RowCount - 1
            colsTotal = viewmastersuplier.DataGridViewX1.Columns.Count - 1

            With (excelWorksheet)
                .Cells.Select()
                .Cells.Delete()
                For iC = 0 To colsTotal
                    .Cells(1, iC + 1).Value = viewmastersuplier.DataGridViewX1.Columns(iC).HeaderText
                Next

                For I = 0 To rowsTotal
                    For j = 0 To colsTotal
                        .Cells(I + 2, j + 1).value = viewmastersuplier.DataGridViewX1.Rows(I).Cells(j).Value
                    Next (j)
                Next (I)

                .Rows("1:1").Font.FontStyle = "Bold"
                .Rows("1:1").Font.Size = 10
                .Cells.Columns.AutoFit()
                .Cells.Select()
                .Cells.EntireColumn.AutoFit()
                .Cells(1, 1).Select()
            End With

        Catch ex As Exception
            MsgBox("Export Excel Error " & ex.Message)
        Finally
            'RELEASE ALLOACTED RESOURCES 
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            xlApp = Nothing

        End Try
    End Sub



    '=================================================
    'Export To Excel Master Pasien
    '=================================================
    Sub Export_MasterPasien()
        Dim rowsTotal, colsTotal As Short
        Dim I, j, iC As Short
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Dim xlApp As New Excel.Application

        Try
            Dim excelBook As Excel.Workbook = xlApp.Workbooks.Add
            Dim excelWorksheet As Excel.Worksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)
            xlApp.Visible = True
            rowsTotal = viewmastercustomer.DataGridViewX1.RowCount - 1
            colsTotal = viewmastercustomer.DataGridViewX1.Columns.Count - 1

            With (excelWorksheet)
                .Cells.Select()
                .Cells.Delete()
                For iC = 0 To colsTotal
                    .Cells(1, iC + 1).Value = viewmastercustomer.DataGridViewX1.Columns(iC).HeaderText
                Next

                For I = 0 To rowsTotal
                    For j = 0 To colsTotal
                        .Cells(I + 2, j + 1).value = viewmastercustomer.DataGridViewX1.Rows(I).Cells(j).Value
                    Next (j)
                Next (I)

                .Rows("1:1").Font.FontStyle = "Bold"
                .Rows("1:1").Font.Size = 10
                .Cells.Columns.AutoFit()
                .Cells.Select()
                .Cells.EntireColumn.AutoFit()
                .Cells(1, 1).Select()
            End With

        Catch ex As Exception
            MsgBox("Export Excel Error " & ex.Message)
        Finally
            'RELEASE ALLOACTED RESOURCES 
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            xlApp = Nothing

        End Try
    End Sub


    '=================================================
    'Export To Excel Master Jenis Obat
    '=================================================
    Sub Export_MasterJenisObat()
        Dim rowsTotal, colsTotal As Short
        Dim I, j, iC As Short
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Dim xlApp As New Excel.Application

        Try
            Dim excelBook As Excel.Workbook = xlApp.Workbooks.Add
            Dim excelWorksheet As Excel.Worksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)
            xlApp.Visible = True
            rowsTotal = viewmasterjenisbarang.DataGridViewX1.RowCount - 1
            colsTotal = viewmasterjenisbarang.DataGridViewX1.Columns.Count - 1

            With (excelWorksheet)
                .Cells.Select()
                .Cells.Delete()
                For iC = 0 To colsTotal
                    .Cells(1, iC + 1).Value = viewmasterjenisbarang.DataGridViewX1.Columns(iC).HeaderText
                Next

                For I = 0 To rowsTotal
                    For j = 0 To colsTotal
                        .Cells(I + 2, j + 1).value = viewmasterjenisbarang.DataGridViewX1.Rows(I).Cells(j).Value
                    Next (j)
                Next (I)

                .Rows("1:1").Font.FontStyle = "Bold"
                .Rows("1:1").Font.Size = 10
                .Cells.Columns.AutoFit()
                .Cells.Select()
                .Cells.EntireColumn.AutoFit()
                .Cells(1, 1).Select()
            End With

        Catch ex As Exception
            MsgBox("Export Excel Error " & ex.Message)
        Finally
            'RELEASE ALLOACTED RESOURCES 
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            xlApp = Nothing

        End Try
    End Sub


    '=================================================
    'Export To Excel List Page Stock
    '=================================================
    Sub Export_ListPageStock()
        Dim rowsTotal, colsTotal As Short
        Dim I, j, iC As Short
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Dim xlApp As New Excel.Application

        Try
            Dim excelBook As Excel.Workbook = xlApp.Workbooks.Add
            Dim excelWorksheet As Excel.Worksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)
            xlApp.Visible = True
            rowsTotal = ListPageStock.DataGridViewX1.RowCount - 1
            colsTotal = ListPageStock.DataGridViewX1.Columns.Count - 1

            With (excelWorksheet)
                .Cells.Select()
                .Cells.Delete()
                For iC = 0 To colsTotal
                    .Cells(1, iC + 1).Value = ListPageStock.DataGridViewX1.Columns(iC).HeaderText
                Next

                For I = 0 To rowsTotal
                    For j = 0 To colsTotal
                        .Cells(I + 2, j + 1).value = ListPageStock.DataGridViewX1.Rows(I).Cells(j).Value
                    Next (j)
                Next (I)

                .Rows("1:1").Font.FontStyle = "Bold"
                .Rows("1:1").Font.Size = 10
                .Cells.Columns.AutoFit()
                .Cells.Select()
                .Cells.EntireColumn.AutoFit()
                .Cells(1, 1).Select()
            End With

        Catch ex As Exception
            MsgBox("Export Excel Error " & ex.Message)
        Finally
            'RELEASE ALLOACTED RESOURCES 
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
            xlApp = Nothing

        End Try
    End Sub
End Module
