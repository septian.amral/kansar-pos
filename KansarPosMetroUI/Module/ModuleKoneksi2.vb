﻿Imports System.Data
Imports MySql.Data.MySqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Module ModuleKoneksi2
    Public laporan As New ReportDocument
    Dim TabelLogon As CrystalDecisions.Shared.TableLogOnInfo

    '========================================================================
    'KONEKSI DATABASE REPORT ( ODBC )
    '========================================================================
    Sub SetingLaporan()
        GetDatabaseSetting()
        Dim BacaTabel As CrystalDecisions.CrystalReports.Engine.Table
        For Each BacaTabel In laporan.Database.Tables
            TabelLogon = BacaTabel.LogOnInfo
            With TabelLogon.ConnectionInfo
                '.ServerName = "PenjualanMetroUI"
                .IntegratedSecurity = False
                .ServerName = dbServer
                .UserID = dbUser
                .Password = dbPassword
                .DatabaseName = dbName
            End With
            BacaTabel.ApplyLogOnInfo(TabelLogon)
        Next BacaTabel

    End Sub



End Module
