﻿Imports System.Data
'Imports CrystalDecisions.CrystalReports.Engine
'Imports CrystalDecisions.Shared
Imports System.Data.SqlClient

Module ModuleKoneksi

    Public conn As SqlConnection
    'Public laporan As New ReportDocument
    'Dim TabelLogon As CrystalDecisions.Shared.TableLogOnInfo

    '========================================================================
    'KONEKSI KE DATABASE MYSQL
    '========================================================================
    Sub koneksiDB()
        GetDatabaseSetting()
        conn = New SqlConnection
        conn.ConnectionString = "Data Source=" & dbServer & ";User Id=" & dbUser & ";" & "password=" & dbPassword & ";Initial Catalog=" & dbName
        Try
            conn.Open()
        Catch myerror As SqlException
            MsgBox("Ada kesalahan dalam koneksi database")
        End Try
    End Sub

    ''========================================================================
    ''KONEKSI DATABASE REPORT ( ODBC )
    ''========================================================================
    'Sub SetingLaporan()
    '    GetDatabaseSetting()
    '    Dim BacaTabel As CrystalDecisions.CrystalReports.Engine.Table
    '    For Each BacaTabel In laporan.Database.Tables
    '        TabelLogon = BacaTabel.LogOnInfo
    '        With TabelLogon.ConnectionInfo
    '            .ServerName = "PenjualanMetroUI"
    '            .UserID = dbUser
    '            .Password = dbPassword
    '            .DatabaseName = dbName
    '        End With
    '        BacaTabel.ApplyLogOnInfo(TabelLogon)
    '    Next BacaTabel

    'End Sub



End Module
