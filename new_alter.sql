/*
 Navicat Premium Data Transfer

 Source Server         : sqlserverlocal
 Source Server Type    : SQL Server
 Source Server Version : 14001000
 Source Host           : 127.0.0.1:1433
 Source Catalog        : POS
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 14001000
 File Encoding         : 65001

 Date: 08/04/2020 19:38:29
*/


-- ----------------------------
-- Table structure for detail_pembelian
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[detail_pembelian]') AND type IN ('U'))
	DROP TABLE [dbo].[detail_pembelian]
GO

CREATE TABLE [dbo].[detail_pembelian] (
  [id_pembelian] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [kode_barang] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [qty] int  NOT NULL
)
GO

ALTER TABLE [dbo].[detail_pembelian] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for detail_penjualan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[detail_penjualan]') AND type IN ('U'))
	DROP TABLE [dbo].[detail_penjualan]
GO

CREATE TABLE [dbo].[detail_penjualan] (
  [id_penjualan] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [kode_barang] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [qty] int  NOT NULL,
  [harga_satuan] int  NOT NULL,
  [Diskon] float(53)  NULL,
  [DiskonNilai] float(53)  NULL,
  [Total] float(53)  NULL,
  [no_urut] int  NULL
)
GO

ALTER TABLE [dbo].[detail_penjualan] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of detail_penjualan
-- ----------------------------
INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00001', N'1', N'10', N'60000', N'10', N'100', N'539900', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00001', N'3', N'10', N'60000', N'10', N'1000', N'539000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00001', N'1', N'10', N'60000', N'10', N'100', N'539900', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00002', N'1', N'10', N'60000', N'0', N'0', N'600000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00002', N'2', N'10', N'60000', N'0', N'0', N'600000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00002', N'3', N'10', N'60000', N'0', N'0', N'600000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00003', N'1', N'10', N'60000', N'5', N'0', N'570000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00003', N'2', N'5', N'60000', N'5', N'0', N'285000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00004', N'3', N'10', N'60000', N'0', N'0', N'600000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00004', N'2', N'5', N'60000', N'0', N'0', N'300000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00005', N'2', N'5', N'60000', N'0', N'0', N'300000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00005', N'3', N'10', N'60000', N'0', N'0', N'600000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00006', N'1', N'10', N'60000', N'0', N'0', N'600000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00006', N'2', N'5', N'60000', N'0', N'0', N'300000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00007', N'1', N'5', N'60000', N'2', N'4000', N'290000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00007', N'2', N'10', N'60000', N'2', N'0', N'588000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00007', N'3', N'3', N'60000', N'2', N'0', N'176400', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00008', N'4', N'5', N'70000', N'5', N'0', N'332500', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00008', N'2', N'5', N'60000', N'5', N'0', N'285000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00009', N'1', N'5', N'60000', N'5', N'0', N'285000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00009', N'4', N'5', N'70000', N'5', N'0', N'332500', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00010', N'4', N'7', N'70000', N'7', N'0', N'455700', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00010', N'1', N'5', N'60000', N'5', N'0', N'285000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00012', N'2', N'1', N'60000', N'0', N'0', N'60000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00012', N'2', N'1', N'60000', N'0', N'0', N'60000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00013', N'4', N'0', N'70000', N'10', N'1500', N'-1500', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00013', N'2', N'10', N'60000', N'10', N'1500', N'538500', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00014', N'1', N'5', N'60000', N'0', N'0', N'300000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00014', N'2', N'5', N'60000', N'0', N'0', N'300000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00014', N'3', N'5', N'60000', N'0', N'0', N'300000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00015', N'2', N'0', N'60000', N'0', N'0', N'0', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00015', N'3', N'1', N'60000', N'0', N'0', N'60000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00016', N'2', N'10', N'60000', N'5', N'0', N'570000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00016', N'3', N'5', N'60000', N'5', N'0', N'285000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00017', N'4', N'5', N'70000', N'10', N'0', N'315000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00017', N'2', N'5', N'60000', N'10', N'0', N'270000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00018', N'3', N'10', N'60000', N'10', N'0', N'540000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00018', N'1', N'5', N'60000', N'10', N'0', N'270000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00018', N'4', N'4', N'70000', N'10', N'0', N'252000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00019', N'1', N'10', N'60000', N'0', N'0', N'600000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00019', N'2', N'10', N'60000', N'0', N'0', N'600000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00021', N'2', N'1', N'60000', N'5', N'0', N'57000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00021', N'2', N'1', N'60000', N'5', N'0', N'57000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00021', N'3', N'1', N'60000', N'5', N'0', N'57000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00021', N'4', N'1', N'70000', N'5', N'0', N'66500', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00022', N'2', N'2', N'60000', N'10', N'0', N'108000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00022', N'3', N'3', N'60000', N'10', N'0', N'162000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00011', N'1', N'5', N'60000', N'5', N'0', N'285000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00011', N'2', N'3', N'60000', N'5', N'0', N'171000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00011', N'3', N'2', N'60000', N'5', N'0', N'114000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00015', N'4', N'3', N'70000', N'0', N'0', N'210000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00020', N'3', N'1', N'60000', N'10', N'0', N'54000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00020', N'2', N'2', N'60000', N'10', N'0', N'108000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00023', N'1', N'1', N'60000', N'0', N'0', N'60000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00024', N'1', N'1', N'60000', N'0', N'0', N'60000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00024', N'2', N'0', N'60000', N'0', N'0', N'0', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00025', N'1', N'1', N'60000', N'0', N'0', N'60000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00026', N'1', N'1', N'60000', N'0', N'0', N'60000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00027', N'1', N'1', N'60000', N'0', N'0', N'60000', NULL)
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00028', N'1', N'1', N'60000', N'0', N'0', N'60000', N'1')
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00028', N'3', N'3', N'60000', N'0', N'0', N'180000', N'2')
GO

INSERT INTO [dbo].[detail_penjualan]  VALUES (N'TRANS-00028', N'8819912023831704', N'1', N'60000', N'0', N'0', N'60000', N'3')
GO


-- ----------------------------
-- Table structure for jenis_bayar
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jenis_bayar]') AND type IN ('U'))
	DROP TABLE [dbo].[jenis_bayar]
GO

CREATE TABLE [dbo].[jenis_bayar] (
  [ID] smallint  IDENTITY(1,1) NOT NULL,
  [Name] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jenis_bayar] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of jenis_bayar
-- ----------------------------
SET IDENTITY_INSERT [dbo].[jenis_bayar] ON
GO

INSERT INTO [dbo].[jenis_bayar] ([ID], [Name]) VALUES (N'1', N'Mesin EDC')
GO

INSERT INTO [dbo].[jenis_bayar] ([ID], [Name]) VALUES (N'2', N'Uang Asing')
GO

INSERT INTO [dbo].[jenis_bayar] ([ID], [Name]) VALUES (N'3', N'Rupiah')
GO

INSERT INTO [dbo].[jenis_bayar] ([ID], [Name]) VALUES (N'4', N'Giro')
GO

INSERT INTO [dbo].[jenis_bayar] ([ID], [Name]) VALUES (N'5', N'Cek')
GO

SET IDENTITY_INSERT [dbo].[jenis_bayar] OFF
GO


-- ----------------------------
-- Table structure for laporan_pembelian
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[laporan_pembelian]') AND type IN ('U'))
	DROP TABLE [dbo].[laporan_pembelian]
GO

CREATE TABLE [dbo].[laporan_pembelian] (
  [id_pembelian] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [tanggal_pembelian] date  NULL,
  [suplier] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [kode_barang] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [nama_barang] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [satuan] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [qty] int  NULL
)
GO

ALTER TABLE [dbo].[laporan_pembelian] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for laporan_penjualan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[laporan_penjualan]') AND type IN ('U'))
	DROP TABLE [dbo].[laporan_penjualan]
GO

CREATE TABLE [dbo].[laporan_penjualan] (
  [id_penjualan] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [tanggal_penjualan] date  NULL,
  [customer] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [kode_barang] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [nama_barang] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [satuan] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [qty] int  NULL,
  [harga_satuan] int  NULL
)
GO

ALTER TABLE [dbo].[laporan_penjualan] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for master_barang
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[master_barang]') AND type IN ('U'))
	DROP TABLE [dbo].[master_barang]
GO

CREATE TABLE [dbo].[master_barang] (
  [kode_barang] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [jenis_barang] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [nama_barang] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [satuan] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [harga_beli] int  NOT NULL,
  [harga_jual] int  NOT NULL,
  [stock_barang] int  NOT NULL
)
GO

ALTER TABLE [dbo].[master_barang] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of master_barang
-- ----------------------------
INSERT INTO [dbo].[master_barang]  VALUES (N'1', N'TAS', N'TAS KANA 001', N'PCS', N'50000', N'60000', N'30')
GO

INSERT INTO [dbo].[master_barang]  VALUES (N'2', N'TAS', N'TAS KANA 002', N'PCS', N'50000', N'60000', N'-1')
GO

INSERT INTO [dbo].[master_barang]  VALUES (N'3', N'TAS', N'TAS KANA 003', N'PCS', N'50000', N'60000', N'25')
GO

INSERT INTO [dbo].[master_barang]  VALUES (N'4', N'TAS', N'RUDI TAS', N'PCS', N'50000', N'70000', N'17')
GO

INSERT INTO [dbo].[master_barang]  VALUES (N'8819912023831704', N'TAS', N'BARANG BARCODE 01', N'PCS', N'50000', N'60000', N'50')
GO


-- ----------------------------
-- Table structure for master_customer
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[master_customer]') AND type IN ('U'))
	DROP TABLE [dbo].[master_customer]
GO

CREATE TABLE [dbo].[master_customer] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [nama_customer] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [alamat_customer] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [notlpn] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[master_customer] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of master_customer
-- ----------------------------
SET IDENTITY_INSERT [dbo].[master_customer] ON
GO

INSERT INTO [dbo].[master_customer] ([id], [nama_customer], [alamat_customer], [notlpn]) VALUES (N'1', N'KANA', N'BANDUNG', N'123')
GO

INSERT INTO [dbo].[master_customer] ([id], [nama_customer], [alamat_customer], [notlpn]) VALUES (N'2', N'RUDI', N'BANDUNG', N'123')
GO

INSERT INTO [dbo].[master_customer] ([id], [nama_customer], [alamat_customer], [notlpn]) VALUES (N'3', N'KANA 01', N'BANDUNG', N'123')
GO

INSERT INTO [dbo].[master_customer] ([id], [nama_customer], [alamat_customer], [notlpn]) VALUES (N'4', N'KANA02', N'BANDUNG', N'123')
GO

INSERT INTO [dbo].[master_customer] ([id], [nama_customer], [alamat_customer], [notlpn]) VALUES (N'5', N'RUDI', N'GEMPOL SARI,CARINGIN,BANDUNG KULON,KOTA BANDUNG', N'123456')
GO

SET IDENTITY_INSERT [dbo].[master_customer] OFF
GO


-- ----------------------------
-- Table structure for master_jenis_barang
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[master_jenis_barang]') AND type IN ('U'))
	DROP TABLE [dbo].[master_jenis_barang]
GO

CREATE TABLE [dbo].[master_jenis_barang] (
  [kode_jenis] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [jenis_barang] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[master_jenis_barang] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for master_suplier
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[master_suplier]') AND type IN ('U'))
	DROP TABLE [dbo].[master_suplier]
GO

CREATE TABLE [dbo].[master_suplier] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [nama_suplier] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [alamat_suplier] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [notlpn] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[master_suplier] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for stock_opname
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[stock_opname]') AND type IN ('U'))
	DROP TABLE [dbo].[stock_opname]
GO

CREATE TABLE [dbo].[stock_opname] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [kode_barang] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [nama_barang] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [satuan] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [selisih_qty] int  NOT NULL
)
GO

ALTER TABLE [dbo].[stock_opname] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of stock_opname
-- ----------------------------
SET IDENTITY_INSERT [dbo].[stock_opname] ON
GO

INSERT INTO [dbo].[stock_opname] ([id], [kode_barang], [nama_barang], [satuan], [selisih_qty]) VALUES (N'1', N'1', N'TAS KANA 001', N'PCS', N'60')
GO

INSERT INTO [dbo].[stock_opname] ([id], [kode_barang], [nama_barang], [satuan], [selisih_qty]) VALUES (N'2', N'2', N'TAS KANA 002', N'PCS', N'40')
GO

INSERT INTO [dbo].[stock_opname] ([id], [kode_barang], [nama_barang], [satuan], [selisih_qty]) VALUES (N'3', N'3', N'TAS KANA 003', N'PCS', N'50')
GO

INSERT INTO [dbo].[stock_opname] ([id], [kode_barang], [nama_barang], [satuan], [selisih_qty]) VALUES (N'4', N'1', N'TAS KANA 001', N'PCS', N'50')
GO

INSERT INTO [dbo].[stock_opname] ([id], [kode_barang], [nama_barang], [satuan], [selisih_qty]) VALUES (N'5', N'2', N'TAS KANA 002', N'PCS', N'50')
GO

INSERT INTO [dbo].[stock_opname] ([id], [kode_barang], [nama_barang], [satuan], [selisih_qty]) VALUES (N'6', N'3', N'TAS KANA 003', N'PCS', N'35')
GO

INSERT INTO [dbo].[stock_opname] ([id], [kode_barang], [nama_barang], [satuan], [selisih_qty]) VALUES (N'7', N'4', N'RUDI TAS', N'PCS', N'17')
GO

SET IDENTITY_INSERT [dbo].[stock_opname] OFF
GO


-- ----------------------------
-- Table structure for transaksi_pembelian
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[transaksi_pembelian]') AND type IN ('U'))
	DROP TABLE [dbo].[transaksi_pembelian]
GO

CREATE TABLE [dbo].[transaksi_pembelian] (
  [id_pembelian] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [tanggal_pembelian] date  NOT NULL,
  [suplier] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [jumlah_barang] int  NOT NULL
)
GO

ALTER TABLE [dbo].[transaksi_pembelian] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for transaksi_penjualan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[transaksi_penjualan]') AND type IN ('U'))
	DROP TABLE [dbo].[transaksi_penjualan]
GO

CREATE TABLE [dbo].[transaksi_penjualan] (
  [id_penjualan] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [tanggal_penjualan] date  NOT NULL,
  [customer] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [total_harga] int  NOT NULL,
  [jumlah_barang] int  NOT NULL,
  [Diskon1] float(53)  NULL,
  [DiskonNilai1] float(53)  NULL,
  [Diskon2] float(53)  NULL,
  [DiskonNilai2] float(53)  NULL,
  [DiskonBulat] float(53)  NULL,
  [Ongkir] float(53)  NULL,
  [DP] float(53)  NULL,
  [Bayar] float(53)  NULL,
  [Remark] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [PrintCount] int DEFAULT ((0)) NULL,
  [PPN10] float(53)  NULL
)
GO

ALTER TABLE [dbo].[transaksi_penjualan] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of transaksi_penjualan
-- ----------------------------
INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00001', N'2019-12-31', N'KANA', N'1077900', N'3', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00002', N'2020-01-05', N'KANA', N'1550000', N'3', N'10', N'180000', N'5', N'90000', N'30000', N'100000', N'50000', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00003', N'2020-01-09', N'KANA', N'855000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00004', N'2020-01-09', N'RUDI', N'900000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00005', N'2020-01-10', N'RUDI', N'900000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00006', N'2020-01-12', N'RUDI', N'900000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00007', N'2020-01-12', N'KANA02', N'401000', N'3', N'5', N'52720', N'10', N'105440', N'240', N'5000', N'500000', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00008', N'2020-01-14', N'RUDI', N'494000', N'2', N'20', N'123500', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00009', N'2020-01-14', N'RUDI', N'617500', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00010', N'2020-01-14', N'RUDI', N'740700', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00011', N'2020-01-14', N'KANA', N'570000', N'3', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00012', N'2020-01-15', N'RUDI', N'120000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00013', N'2020-01-15', N'KANA 01', N'537000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00014', N'2020-01-16', N'KANA 01', N'710000', N'3', N'10', N'90000', N'10', N'90000', N'10000', N'100000', N'100000', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00015', N'2020-01-16', N'KANA 01', N'127500', N'3', N'10', N'51000', N'10', N'51000', N'500', N'10000', N'50000', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00016', N'2020-01-17', N'KANA 01', N'246000', N'2', N'10', N'85500', N'5', N'42750', N'750', N'20000', N'500000', N'0', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00017', N'2020-01-29', N'RUDI', N'585000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'585000', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00018', N'2020-01-29', N'RUDI', N'1062000', N'3', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'1062000', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00019', N'2020-01-29', N'KANA02', N'1200000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'1200000', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00020', N'2020-01-29', N'KANA 01', N'162000', N'3', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'162000', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00021', N'2020-01-29', N'KANA 01', N'237500', N'4', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'200000', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00022', N'2020-02-26', N'KANA', N'270000', N'2', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'270000', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00023', N'2020-03-09', N'Keren Aja', N'60000', N'1', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'100', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00024', N'2020-03-21', N'KANA', N'57000', N'2', N'10', NULL, N'5', NULL, N'0', N'0', N'0', N'100', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00025', N'2020-03-21', N'KANA', N'56000', N'1', N'5', NULL, N'1', NULL, N'400', N'0', N'0', N'100', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00026', N'2020-03-21', N'KANA', N'58500', N'1', N'1', NULL, N'1', NULL, N'300', N'0', N'0', N'100', N'', NULL, NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00027', N'2020-03-21', N'kana 01', N'66000', N'1', N'0', NULL, N'0', NULL, N'0', N'0', N'0', N'100000', N'', N'12', NULL)
GO

INSERT INTO [dbo].[transaksi_penjualan]  VALUES (N'TRANS-00028', N'2020-04-06', N'KANA', N'330000', N'3', N'0', NULL, N'0', NULL, N'0', N'0', N'0', N'500000', N'', N'0', NULL)
GO


-- ----------------------------
-- Table structure for user
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[user]') AND type IN ('U'))
	DROP TABLE [dbo].[user]
GO

CREATE TABLE [dbo].[user] (
  [username] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [password] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [alamat] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [notlpn] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[user] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO [dbo].[user]  VALUES (N'admin', N'admin', N'bandung', N'123')
GO


-- ----------------------------
-- View structure for laporan_penjualan_view
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[laporan_penjualan_view]') AND type IN ('V'))
	DROP VIEW [dbo].[laporan_penjualan_view]
GO

CREATE VIEW [dbo].[laporan_penjualan_view] AS SELECT dbo.transaksi_penjualan.id_penjualan, dbo.transaksi_penjualan.tanggal_penjualan, dbo.transaksi_penjualan.customer, dbo.detail_penjualan.kode_barang, dbo.master_barang.nama_barang, dbo.detail_penjualan.harga_satuan, 
                  dbo.detail_penjualan.qty, dbo.master_barang.satuan, dbo.detail_penjualan.harga_satuan * dbo.detail_penjualan.qty as jumlah
FROM     dbo.detail_penjualan INNER JOIN
                  dbo.master_barang ON dbo.detail_penjualan.kode_barang = dbo.master_barang.kode_barang INNER JOIN
                  dbo.transaksi_penjualan ON dbo.detail_penjualan.id_penjualan = dbo.transaksi_penjualan.id_penjualan
GO


-- ----------------------------
-- Primary Key structure for table jenis_bayar
-- ----------------------------
ALTER TABLE [dbo].[jenis_bayar] ADD CONSTRAINT [PK__jenis_ba__3214EC276048119E] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table master_barang
-- ----------------------------
ALTER TABLE [dbo].[master_barang] ADD CONSTRAINT [PK_master_barang] PRIMARY KEY CLUSTERED ([kode_barang])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table master_customer
-- ----------------------------
ALTER TABLE [dbo].[master_customer] ADD CONSTRAINT [PK_master_customer] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table master_jenis_barang
-- ----------------------------
ALTER TABLE [dbo].[master_jenis_barang] ADD CONSTRAINT [PK_master_jenis_barang] PRIMARY KEY CLUSTERED ([kode_jenis])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table master_suplier
-- ----------------------------
ALTER TABLE [dbo].[master_suplier] ADD CONSTRAINT [PK_master_suplier] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table stock_opname
-- ----------------------------
ALTER TABLE [dbo].[stock_opname] ADD CONSTRAINT [PK_stock_opname] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table transaksi_pembelian
-- ----------------------------
ALTER TABLE [dbo].[transaksi_pembelian] ADD CONSTRAINT [PK_transaksi_pembelian] PRIMARY KEY CLUSTERED ([id_pembelian])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table transaksi_penjualan
-- ----------------------------
ALTER TABLE [dbo].[transaksi_penjualan] ADD CONSTRAINT [PK_transaksi_penjualan] PRIMARY KEY CLUSTERED ([id_penjualan])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE [dbo].[user] ADD CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED ([username])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Foreign Keys structure for table detail_pembelian
-- ----------------------------
ALTER TABLE [dbo].[detail_pembelian] ADD CONSTRAINT [FK_kode_barang] FOREIGN KEY ([kode_barang]) REFERENCES [dbo].[master_barang] ([kode_barang]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table detail_penjualan
-- ----------------------------
ALTER TABLE [dbo].[detail_penjualan] ADD CONSTRAINT [FK_kode_barang_jual] FOREIGN KEY ([kode_barang]) REFERENCES [dbo].[master_barang] ([kode_barang]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

